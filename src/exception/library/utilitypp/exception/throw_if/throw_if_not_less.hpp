#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"
#include "throw_if_not_finite.hpp"

#define upp_throw_if_not_less(except, a, b)     \
    upp_throw_if(except, !(a < b))              \
            << "Variable "                      \
            << upp_varout(a)                    \
            << " not less than "                \
            << upp_varout(b) << "!\n"

