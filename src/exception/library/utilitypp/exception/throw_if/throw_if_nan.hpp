#pragma once

#include <cmath>

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"

#define upp_throw_if_nan(except, v)         \
    upp_throw_if(except, ::std::isnan(v))   \
        << "Variable " << upp_varout(v)     \
        << " is nan!\n"
