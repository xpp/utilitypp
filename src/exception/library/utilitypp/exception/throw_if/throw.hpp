#pragma once

#include <stdexcept>
#include <sstream>

#include <utilitypp/io/streamout_file_line_function.hpp>

#include "../throw_stream.hpp"

#define upp_throw(except)                                   \
    ::upp::throw_stream<except>{}                           \
        << "EXCEPTION\n"                                    \
        << upp_streamout_multiline_file_line_func("    ")   \
        << "\n    What    :\n"
