#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"
#include "throw_if_nan.hpp"

#define upp_throw_if_greater(except, a, b)      \
    upp_throw_if_nan(except, a);                \
    upp_throw_if_nan(except, b);                \
    upp_throw_if(except, a > b)                 \
        << "Variable "                          \
        << upp_varout(a) << " greater than "    \
        << upp_varout(b) << "!\n"
