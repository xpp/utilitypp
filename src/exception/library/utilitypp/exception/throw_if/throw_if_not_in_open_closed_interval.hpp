#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"

#define throw_if_not_in_open_closed_interval(except, lo, hi, ...)   \
    upp_throw_if(except, __VA_ARGS__ <= lo || __VA_ARGS__ > hi)     \
        << "Variable " << upp_varout(__VA_ARGS__)                   \
        << " is not in the interval (" << lo << ", " << hi << "]!\n"

