#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"
#include "throw_if_not_finite.hpp"

#define upp_throw_if_not_greater(except, a, b)  \
    upp_throw_if_not_finite(except, a);         \
    upp_throw_if_not_finite(except, b);         \
    upp_throw_if(except, !(a > b))              \
        << "Variable " << upp_varout(a)         \
        << " not greater than "                 \
        << upp_varout(b) << "!\n"
