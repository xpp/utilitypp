#pragma once

#include "throw.hpp"

#define upp_throw_if_not(except, ...)       \
    if(!(__VA_ARGS__)) upp_throw(except)
