#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"

#define upp_throw_if_not_null(except, ...)          \
    upp_throw_if(except, __VA_ARGS__ == nullptr)    \
        << "Variable " << upp_varout(__VA_ARGS__)   \
        << " is not null!\n"
