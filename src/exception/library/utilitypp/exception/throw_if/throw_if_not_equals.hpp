#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"
#include "throw_if_not_finite.hpp"

#define upp_throw_if_not_equal(except, a, b)        \
    upp_throw_if(except, a != b)                    \
            << "Variables " << upp_varout(a)        \
            << " and " << upp_varout(b)             \
            << " are not equal!\n"

#define upp_throw_if_not_equal_0(except, ...)       \
    upp_throw_if_not_equal(except, __VA_ARGS__, 0)

#define upp_throw_if_not_equal_1(except, ...)       \
    upp_throw_if_not_equal(except, __VA_ARGS__, 1)


#define upp_throw_if_not_exactly_equal(except, a, b)    \
    upp_throw_if(except, std::abs(a - b) > 0)           \
            << "Variables " << upp_varout(a)            \
            << " and " << upp_varout(b)                 \
            << " are not equal!\n"

#define upp_throw_if_not_exactly_equal_0(except, ...)   \
    upp_throw_if_not_exactly_equal(except, __VA_ARGS__, 0)

#define upp_throw_if_not_exactly_equal_1(except, ...)   \
    upp_throw_if_not_exactly_equal(except, __VA_ARGS__, 1)
