#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"

#define upp_throw_if_less(except, a, b)         \
    upp_throw_if(except, a < b)                 \
            << "Variable "                      \
            << upp_varout(a) << " less than "   \
            << upp_varout(b) << "!\n"
