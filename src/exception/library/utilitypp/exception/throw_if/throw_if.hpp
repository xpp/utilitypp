#pragma once

#include "throw.hpp"

#define upp_throw_if(except, ...)                                           \
    if(__VA_ARGS__) upp_throw(except) << upp_stringify(__VA_ARGS__) << "\n"
