#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"

#define upp_throw_if_null(except, ...)              \
    upp_throw_if(except, __VA_ARGS__ == nullptr)    \
        << "Variable '" << #__VA_ARGS__             \
        << "' is null!\n"
