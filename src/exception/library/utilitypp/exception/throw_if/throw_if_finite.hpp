#pragma once

#include <cmath>

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"

#define upp_throw_if_finite(except, v)          \
    upp_throw_if(except, ::std::isfinite(v))    \
        << "Variable " << upp_varout(v)         \
        << " is finite!\n"
