#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"

#define upp_throw_if_not_finite(except, ...)            \
    upp_throw_if(except, !::std::isfinite(__VA_ARGS__)) \
        << "Variable " << upp_varout(__VA_ARGS__)       \
        << " is not finite!\n"
