#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"
#include "throw_if_not_finite.hpp"

#define upp_throw_if_not_positive(except, ...)      \
    upp_throw_if_not_finite(except, __VA_ARGS__);   \
    upp_throw_if(except, __VA_ARGS__ <= 0)          \
        << "Variable "                              \
        << upp_varout(__VA_ARGS__)                  \
        << " is not positive!\n"
