#pragma once

#include <cmath>

#include "throw_if.hpp"

#define upp_throw_if_not_nan(except, v)     \
    upp_throw_if(except, !::std::isnan(v))  \
        << "Variable " << upp_varout(v)     \
        << " is not nan!\n"
