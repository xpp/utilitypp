#pragma once

#include <utilitypp/io/varout.hpp>

#include "throw_if.hpp"
#include "throw_if_not_finite.hpp"

#define upp_throw_if_negative(except, v)    \
    upp_throw_if_not_finite(except, v);     \
    upp_throw_if(except, v < 0)             \
        << "Variable " << upp_varout(v)     \
        << " is negative!\n"
