#pragma once
#include <experimental/source_location>

namespace std
{
    using ::std::experimental::source_location;
}

#include <sstream>

namespace upp
{
    template<class ExceptionT>
    struct throw_stream
    {
        throw_stream(std::source_location location = std::source_location::current())
        {
            str << "from '" << location.file_name()
                << "' : " << location.line()
                << " (" << location.function_name() << ")\n";
        }
        throw_stream(throw_stream&&) = delete;
        throw_stream(const throw_stream&) = delete;
        throw_stream& operator=(throw_stream&&) = delete;
        throw_stream& operator=(const throw_stream&) = delete;

        ~throw_stream() noexcept(false)
        {
            throw ExceptionT{str.str()};
        }

        std::stringstream& operator<<(auto&& x)
        {
            str << std::forward<decltype(x)>(x);
            return str;
        }

        std::stringstream& stream()
        {
            return str;
        }

        const std::stringstream& stream() const
        {
            return str;
        }

    private:
        std::stringstream str;
    };
}
