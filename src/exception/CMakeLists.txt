################################################################################
#################################### library ###################################
################################################################################
scm_add_header_library(
    SOURCE_DIRECTORY_PREFIX library
    TARGET                  exception
    GENERATE_SUBDIR_HEADERS utilitypp
    DEPENDENCIES            io
                            preprocessor
    FILES                   utilitypp/exception/throw_stream.hpp

                            utilitypp/exception/throw_if/throw_if.hpp
                            utilitypp/exception/throw_if/throw.hpp

                            utilitypp/exception/throw_if/throw_if.hpp
                            utilitypp/exception/throw_if/throw_if_equals.hpp
                            utilitypp/exception/throw_if/throw_if_finite.hpp
                            utilitypp/exception/throw_if/throw_if_greater.hpp
                            utilitypp/exception/throw_if/throw_if_less.hpp
                            utilitypp/exception/throw_if/throw_if_nan.hpp
                            utilitypp/exception/throw_if/throw_if_negative.hpp
                            utilitypp/exception/throw_if/throw_if_null.hpp
                            utilitypp/exception/throw_if/throw_if_positive.hpp

                            utilitypp/exception/throw_if/throw_if_not.hpp
                            utilitypp/exception/throw_if/throw_if_not_equals.hpp
                            utilitypp/exception/throw_if/throw_if_not_finite.hpp
                            utilitypp/exception/throw_if/throw_if_not_greater.hpp
                            utilitypp/exception/throw_if/throw_if_not_less.hpp
                            utilitypp/exception/throw_if/throw_if_not_nan.hpp
                            utilitypp/exception/throw_if/throw_if_not_negative.hpp
                            utilitypp/exception/throw_if/throw_if_not_null.hpp
                            utilitypp/exception/throw_if/throw_if_not_positive.hpp

                            utilitypp/exception/throw_if/throw_if_not_in_closed_interval.hpp
                            utilitypp/exception/throw_if/throw_if_not_in_closed_open_interval.hpp
                            utilitypp/exception/throw_if/throw_if_not_in_open_closed_interval.hpp
                            utilitypp/exception/throw_if/throw_if_not_in_open_interval.hpp
)
################################################################################
#################################### tests #####################################
################################################################################
upp_add_test(exception throw_if)
################################################################################
################################# benchmarking #################################
################################################################################
if(NOT benchmark_FOUND)
    set_directory_properties(PROPERTIES EXCLUDE_FROM_ALL ON)
endif()
