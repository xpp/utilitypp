#define BOOST_TEST_MODULE util

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/exception/throw_if.hpp>


BOOST_AUTO_TEST_CASE( varout )
{
    using e = std::invalid_argument;
    static constexpr auto nan = std::nan("");
    static constexpr auto inf = std::numeric_limits<double>::infinity();

    BOOST_CHECK_THROW(upp_throw(e), e);

    BOOST_CHECK_THROW(upp_throw_if(e, true), e);
    upp_throw_if(e, false);

    BOOST_CHECK_THROW(upp_throw_if_finite(e, 1), e);
    upp_throw_if_finite(e, nan);
    upp_throw_if_finite(e, inf);

    BOOST_CHECK_THROW(upp_throw_if_not_finite(e, nan), e);
    BOOST_CHECK_THROW(upp_throw_if_not_finite(e, inf), e);
    upp_throw_if_not_finite(e, 1);

    BOOST_CHECK_THROW(upp_throw_if_nan(e, nan), e);
    upp_throw_if_nan(e, 1);
    upp_throw_if_nan(e, inf);

    BOOST_CHECK_THROW(upp_throw_if_not_nan(e, 1), e);
    BOOST_CHECK_THROW(upp_throw_if_not_nan(e, inf), e);
    upp_throw_if_not_nan(e, nan);

    BOOST_CHECK_THROW(upp_throw_if_not_positive(e, -1), e);
    BOOST_CHECK_THROW(upp_throw_if_not_positive(e, 0), e);
    BOOST_CHECK_THROW(upp_throw_if_not_positive(e, nan), e);
    upp_throw_if_not_positive(e, 1);

    BOOST_CHECK_THROW(upp_throw_if_negative(e, -1), e);
    upp_throw_if_negative(e, 1);
    upp_throw_if_negative(e, 0);

    BOOST_CHECK_THROW(upp_throw_if_equal(e, 1, 1), e);
    upp_throw_if_equal(e, 1, -1);

    BOOST_CHECK_THROW(upp_throw_if_equal_0(e, 0), e);
    upp_throw_if_equal_0(e, 1);
    upp_throw_if_equal_0(e, -1);

    BOOST_CHECK_THROW(upp_throw_if_not_equal(e, 1, -1), e);
    upp_throw_if_not_equal(e, 1, 1);

    BOOST_CHECK_THROW(upp_throw_if_not_equal_0(e, 1), e);
    BOOST_CHECK_THROW(upp_throw_if_not_equal_0(e, -1), e);
    upp_throw_if_not_equal_0(e, 0);

    BOOST_CHECK_THROW(upp_throw_if_less(e, -1, 0), e);
    upp_throw_if_less(e, 0, 0);
    upp_throw_if_less(e, 1, 0);
}
