#pragma once

#include <utilitypp/exception/throw_if.hpp>

#include "ndindex.hpp"

namespace upp
{
    template <class ElementContainerT, class IndexContainerT>
    class ndview
    {
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// typedefs ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        using element_container_t = ElementContainerT;
        using index_container_t   = IndexContainerT;
        using index_t = ndindex<index_container_t>;
        using value_t = typename element_container_t::value_type;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// ctor //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        ndview(const element_container_t& elements, const index_t& index);
        ndview(const element_container_t& elements, const index_container_t& index);

        ndview(ndview&&) = default;
        ndview(const ndview&) = default;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// assign /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        ndview& operator=(ndview&&) = default;
        ndview& operator=(const ndview&) = default;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// access /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        const element_container_t& elements() const;
        const index_t& index() const;
        const value_t& at(std::size_t i) const;
        value_t& at(std::size_t i);
        const value_t& at(const auto& i) const;
        value_t& at(const auto& i);
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// iterator ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        auto begin();
        auto end();
        auto begin() const;
        auto end() const;
        ///TODO cbegin, cend, rbegin, rend, crbegin, crend
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// size //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        std::size_t number_of_dimensions() const;
        const auto& size_of_dimensions() const;
        std::size_t size_of_dimension(std::size_t i) const;
        std::size_t number_of_elements() const;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// data //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    private:
        element_container_t _elements;
        index_t _index;
    };



    template<class ElementContainerT, class IndexContainerT>
    inline ndview<ElementContainerT, IndexContainerT>::ndview(const element_container_t& elements, const index_t& index) :
        _elements{elements},
        _index{index}
    {
        upp_throw_if_not_equal(std::invalid_argument, elements.size(), index.max_size_t());
    }

    template<class ElementContainerT, class IndexContainerT>
    inline ndview<ElementContainerT, IndexContainerT>::ndview(const element_container_t& elements, const index_container_t& index) :
        ndview(elements, ndindex{index})
    {}


    template<class ElementContainerT, class IndexContainerT>
    inline const typename ndview<ElementContainerT, IndexContainerT>::element_container_t& ndview<ElementContainerT, IndexContainerT>::elements() const
    {
        return _elements;
    }
    template<class ElementContainerT, class IndexContainerT>
    inline const typename ndview<ElementContainerT, IndexContainerT>::index_t& ndview<ElementContainerT, IndexContainerT>::index() const
    {
        return _index;
    }
    template<class ElementContainerT, class IndexContainerT>
    inline const typename ndview<ElementContainerT, IndexContainerT>::value_t& ndview<ElementContainerT, IndexContainerT>::at(std::size_t i) const
    {
        upp_throw_if_not_less(std::invalid_argument, i, _elements.size());
        return _elements.at(i);
    }
    template<class ElementContainerT, class IndexContainerT>
    inline typename ndview<ElementContainerT, IndexContainerT>::value_t& ndview<ElementContainerT, IndexContainerT>::at(std::size_t i)
    {
        upp_throw_if_not_less(std::invalid_argument, i, _elements.size());
        return _elements.at(i);
    }
    template<class ElementContainerT, class IndexContainerT>
    inline const typename ndview<ElementContainerT, IndexContainerT>::value_t& ndview<ElementContainerT, IndexContainerT>::at(const auto& i) const
    {
        return _elements.at(index().to_size_t(i));
    }
    template<class ElementContainerT, class IndexContainerT>
    inline typename ndview<ElementContainerT, IndexContainerT>::value_t& ndview<ElementContainerT, IndexContainerT>::at(const auto& i)
    {
        return _elements.at(index().to_size_t(i));
    }

    template<class ElementContainerT, class IndexContainerT>
    inline auto ndview<ElementContainerT, IndexContainerT>::begin()
    {
        return _elements.begin();
    }

    template<class ElementContainerT, class IndexContainerT>
    inline auto ndview<ElementContainerT, IndexContainerT>::end()
    {
        return _elements.end();
    }

    template<class ElementContainerT, class IndexContainerT>
    inline auto ndview<ElementContainerT, IndexContainerT>::begin() const
    {
        return _elements.begin();
    }

    template<class ElementContainerT, class IndexContainerT>
    inline auto ndview<ElementContainerT, IndexContainerT>::end() const
    {
        return _elements.end();
    }

    template<class ElementContainerT, class IndexContainerT>
    inline std::size_t ndview<ElementContainerT, IndexContainerT>::number_of_dimensions() const
    {
        return _index.number_of_dimensions();
    }

    template<class ElementContainerT, class IndexContainerT>
    inline const auto& ndview<ElementContainerT, IndexContainerT>::size_of_dimensions() const
    {
        return _index.size_of_dimensions();
    }

    template<class ElementContainerT, class IndexContainerT>
    inline std::size_t ndview<ElementContainerT, IndexContainerT>::size_of_dimension(std::size_t i) const
    {
        return _index.size_of_dimension(i);
    }

    template<class ElementContainerT, class IndexContainerT>
    inline std::size_t ndview<ElementContainerT, IndexContainerT>::number_of_elements() const
    {
        return _elements.size();
    }

}
