#pragma once

#include "ndindex.hpp"

namespace upp
{
    template<class SizeContainer>
    inline ndindex<SizeContainer>::ndindex(const SizeContainer& dimension_sizes) :
        _dimension_sizes{dimension_sizes}
    {
        for(std::size_t i = 0; i < dimension_sizes.size(); ++i)
        {
            upp_throw_if_equal(std::invalid_argument, dimension_sizes.at(i), 0);
        }
    }

    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, true > ndindex<SizeContainer>::begin() const
    {
        std::vector<std::size_t> idx(number_of_dimensions(), 0);
        return {this, idx, static_cast<std::size_t>(0)};
    }
    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, true > ndindex<SizeContainer>::end() const
    {
        std::vector<std::size_t> idx(number_of_dimensions(), 0);
        return {this, idx, max_size_t()};
    }
    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, false> ndindex<SizeContainer>::rbegin() const
    {
        std::vector<std::size_t> idx(number_of_dimensions(), 0);
        decrement(idx);
        return {this, idx, max_size_t() - 1};
    }
    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, false> ndindex<SizeContainer>::rend() const
    {
        std::vector<std::size_t> idx(number_of_dimensions(), 0);
        return {this, idx, std::numeric_limits<std::size_t>::max()};
    }

    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, true > ndindex<SizeContainer>::cbegin() const
    {
        return begin();
    }
    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, true > ndindex<SizeContainer>::cend() const
    {
        return end();
    }
    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, false> ndindex<SizeContainer>::crbegin() const
    {
        return rbegin();
    }
    template<class SizeContainer>
    inline ndindex_iterator<ndindex<SizeContainer>, false> ndindex<SizeContainer>::crend() const
    {
        return rend();
    }

    template<class SizeContainer>
    template<class IndexType>
    inline std::size_t ndindex<SizeContainer>::increment(IndexType& index, std::size_t amount) const
    {
        const std::size_t num_dim = _dimension_sizes.size();
        upp_throw_if_not_equal(std::invalid_argument, num_dim, index.size());
        std::size_t factor = 1;
        for(std::size_t i =  0; amount !=0 && i < _dimension_sizes.size(); ++i)
        {
            const auto idx = _dimension_sizes.size() - i - 1;
            const std::size_t dim_size = this->size_of_dimensions().at(idx);
            upp_throw_if_not_less(std::invalid_argument, index.at(idx), dim_size);
            factor*= dim_size;

            index.at(idx) += amount;
            amount         = index.at(idx) / dim_size;
            index.at(idx)  = index.at(idx) % dim_size;
        }
        return amount * factor;
    }
    template<class SizeContainer>
    template<class IndexType>
    inline std::size_t ndindex<SizeContainer>::decrement(IndexType& index, std::size_t amount) const
    {
        const std::size_t num_dim = _dimension_sizes.size();
        upp_throw_if_not_equal(std::invalid_argument, num_dim, index.size());
        std::size_t factor = 1;
        for(std::size_t i =  0; amount !=0 && i < num_dim; ++i)
        {
            const auto idx = num_dim - i - 1;
            const std::size_t dim_size = this->size_of_dimensions().at(idx);
            upp_throw_if_not_less(std::invalid_argument, index.at(idx), dim_size);
            factor*= dim_size;

            const std::size_t current_index_decrement = amount % dim_size;
            amount                                    = amount / dim_size;
            if (current_index_decrement > index.at(idx))
            {
                ++amount;
                index.at(idx) = dim_size - (current_index_decrement - index.at(idx));
            }
            else
            {
                index.at(idx) -= current_index_decrement;
            }
        }
        return amount * factor;
    }

    template<class SizeContainer>
    template<class IndexType>
    inline void ndindex<SizeContainer>::to_index(IndexType& index, std::size_t value) const
    {
        upp_throw_if_not_equal(std::invalid_argument, _dimension_sizes.size(), index.size());
        increment(index, value);
    }
    template<class SizeContainer>
    template<class IndexType>
    inline IndexType ndindex<SizeContainer>::to_index(std::size_t value) const
    {
        IndexType index(number_of_dimensions(), 0);
        to_index(index, value);
        return index;
    }

    template<class SizeContainer>
    template<class IndexType>
    inline std::size_t ndindex<SizeContainer>::to_size_t(const IndexType& index) const
    {
        upp_throw_if_not_equal(std::invalid_argument, _dimension_sizes.size(), index.size());
        std::size_t result = 0;
        for(std::size_t i =  0; i < _dimension_sizes.size(); ++i)
        {
            result = result * _dimension_sizes.at(i) + index.at(i);
        }
        return result;
    }

    template<class SizeContainer>
    inline std::size_t ndindex<SizeContainer>::number_of_dimensions() const
    {
        return _dimension_sizes.size();
    }

    template<class SizeContainer>
    inline const SizeContainer& ndindex<SizeContainer>::size_of_dimensions() const
    {
        return _dimension_sizes;
    }
    template<class SizeContainer>
    inline std::size_t ndindex<SizeContainer>::size_of_dimension(std::size_t i) const
    {
        upp_throw_if_not_less(std::invalid_argument, i, number_of_dimensions());
        return _dimension_sizes.at(i);
    }

    template<class SizeContainer>
    inline std::size_t ndindex<SizeContainer>::max_size_t() const
    {
        std::size_t max = 1;
        for(auto d : _dimension_sizes)
        {
            max *= d;
        }
        return max;
    }
}
