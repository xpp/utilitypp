#pragma once

#include "ndindex_iterator.hpp"

namespace upp
{
    template<class NDIndexT, bool Reverse>
    inline ndindex_iterator<NDIndexT, Reverse>::ndindex_iterator(
            const NDIndexT* ptr,
            std::vector<std::size_t>
            sz, std::size_t raw
    ) :
        _index{ptr},
        _current_index{std::move(sz)},
        _raw_index{raw}
    {}

    template<class NDIndexT, bool Reverse>
    inline ::upp::span<const std::size_t> ndindex_iterator<NDIndexT, Reverse>::dereference() const
    {
        return {_current_index};
    }

    template<class NDIndexT, bool Reverse>
    inline bool ndindex_iterator<NDIndexT, Reverse>::equal(const ndindex_iterator& other) const
    {
        return _raw_index == other._raw_index;
    }

    template<class NDIndexT, bool Reverse>
    inline void ndindex_iterator<NDIndexT, Reverse>::increment()
    {
        if constexpr (Reverse)
        {
            _index->increment(_current_index);
            ++_raw_index;
        }
        else
        {
            _index->decrement(_current_index);
            --_raw_index;
        }
    }

    template<class NDIndexT, bool Reverse>
    inline void ndindex_iterator<NDIndexT, Reverse>::decrement()
    {
        if constexpr (Reverse)
        {
            _index->decrement(_current_index);
            --_raw_index;
        }
        else
        {
            _index->increment(_current_index);
            ++_raw_index;
        }
    }
}
