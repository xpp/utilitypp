#pragma once

#include <utilitypp/exception/throw_if.hpp>

#include "ndindex_iterator.hpp"

namespace upp
{
    /**
     * example index:
     * size = [ 1, 2, 3 ] -> index for 6 elements
     * data =   [
     *              [
     *                  (0, 0, 0),  // element #0
     *                  (0, 0, 1),  // element #1
     *                  (0, 0, 2)   // element #2
     *              ],
     *              [
     *                  (0, 1, 0),  // element #3
     *                  (0, 1, 1),  // element #4
     *                  (0, 1, 2)   // element #5 -> index.at(0) == 0, index.at(1) == 1, index.at(2) == 2
     *              ]
     *          ]
     */
    template<class SizeContainer = std::vector<std::size_t>>
    class ndindex
    {
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// ctor //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        ndindex(const SizeContainer& dimension_sizes);

        ndindex(ndindex&&) = default;
        ndindex(const ndindex&) = default;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// assign /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        ndindex& operator=(ndindex&&) = default;
        ndindex& operator=(const ndindex&) = default;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// iterator ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        ndindex_iterator<ndindex, true > begin() const;
        ndindex_iterator<ndindex, true > end() const;
        ndindex_iterator<ndindex, false> rbegin() const;
        ndindex_iterator<ndindex, false> rend() const;

        ndindex_iterator<ndindex, true > cbegin() const;
        ndindex_iterator<ndindex, true > cend() const;
        ndindex_iterator<ndindex, false> crbegin() const;
        ndindex_iterator<ndindex, false> crend() const;

        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////// calculations //////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        template<class IndexType = std::vector<std::size_t>>
        std::size_t increment(IndexType& index, std::size_t amount = 1) const;
        template<class IndexType = std::vector<std::size_t>>
        std::size_t decrement(IndexType& index, std::size_t amount = 1) const;

        template<class IndexType = std::vector<std::size_t>>
        void to_index(IndexType& index, std::size_t value) const;

        template<class IndexType = std::vector<std::size_t>>
        IndexType to_index(std::size_t value) const;

        template<class IndexType = std::vector<std::size_t>>
        std::size_t to_size_t(const IndexType& index) const;

        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// size //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        std::size_t number_of_dimensions() const;
        const SizeContainer& size_of_dimensions() const;
        std::size_t size_of_dimension(std::size_t i) const;
        std::size_t max_size_t() const;
    private:
        SizeContainer _dimension_sizes;
    };
}

#include "ndindex.ipp"
