#define BOOST_TEST_MODULE ndimensional

#include <array>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/ndimensional/ndview.hpp>

//4d space with following discrete buckets -> 4*1*3*2 = 24 elements
static std::array<std::size_t, 4> idx{4, 1, 3, 2};
//container of elements (each element has the same value as its own ndindex)
static std::array all_idx =
{
    //                         4  1  3  2
    std::array<std::size_t, 4>{0, 0, 0, 0}, //  0
    std::array<std::size_t, 4>{0, 0, 0, 1}, //  1
    std::array<std::size_t, 4>{0, 0, 1, 0}, //  2
    std::array<std::size_t, 4>{0, 0, 1, 1}, //  3
    std::array<std::size_t, 4>{0, 0, 2, 0}, //  4
    std::array<std::size_t, 4>{0, 0, 2, 1}, //  5

    std::array<std::size_t, 4>{1, 0, 0, 0}, //  6
    std::array<std::size_t, 4>{1, 0, 0, 1}, //  7
    std::array<std::size_t, 4>{1, 0, 1, 0}, //  8
    std::array<std::size_t, 4>{1, 0, 1, 1}, //  9
    std::array<std::size_t, 4>{1, 0, 2, 0}, // 10
    std::array<std::size_t, 4>{1, 0, 2, 1}, // 11

    std::array<std::size_t, 4>{2, 0, 0, 0}, // 12
    std::array<std::size_t, 4>{2, 0, 0, 1}, // 13
    std::array<std::size_t, 4>{2, 0, 1, 0}, // 14
    std::array<std::size_t, 4>{2, 0, 1, 1}, // 15
    std::array<std::size_t, 4>{2, 0, 2, 0}, // 16
    std::array<std::size_t, 4>{2, 0, 2, 1}, // 17

    std::array<std::size_t, 4>{3, 0, 0, 0}, // 18
    std::array<std::size_t, 4>{3, 0, 0, 1}, // 19
    std::array<std::size_t, 4>{3, 0, 1, 0}, // 20
    std::array<std::size_t, 4>{3, 0, 1, 1}, // 21
    std::array<std::size_t, 4>{3, 0, 2, 0}, // 22
    std::array<std::size_t, 4>{3, 0, 2, 1}  // 23
};

template <class ElementContainerT, class IndexContainerT>
void test(const ElementContainerT& es, const IndexContainerT& is)
{
    upp::ndview view{es, is};

    auto e = es.begin();
    auto v = view.begin();
    //iteration over view and elements is identical
    for (; e != es.end() && v != view.end(); ++e, ++v)
    {
        BOOST_CHECK_EQUAL_COLLECTIONS(e->begin(), e->end(), v->begin(), v->end());
    }

    for (const auto& i : all_idx)
    {
        const auto& e = view.at(i); // access element with index
        BOOST_CHECK_EQUAL_COLLECTIONS(i.begin(), i.end(), e.begin(), e.end());
    }
}

BOOST_AUTO_TEST_CASE(test_ndindex_array)
{
    test(all_idx, idx);
}

BOOST_AUTO_TEST_CASE(test_ndindex_span)
{
    upp::span is{idx};
    upp::span es{all_idx};
    test(es, is);
}
