#define BOOST_TEST_MODULE ndimensional

#include <array>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/ndimensional/ndindex.hpp>

std::array all_idx =
{                           // 4  1  3  2
    std::array<std::size_t, 4>{0, 0, 0, 0},
    std::array<std::size_t, 4>{0, 0, 0, 1},
    std::array<std::size_t, 4>{0, 0, 1, 0},
    std::array<std::size_t, 4>{0, 0, 1, 1},
    std::array<std::size_t, 4>{0, 0, 2, 0},
    std::array<std::size_t, 4>{0, 0, 2, 1},

    std::array<std::size_t, 4>{1, 0, 0, 0},
    std::array<std::size_t, 4>{1, 0, 0, 1},
    std::array<std::size_t, 4>{1, 0, 1, 0},
    std::array<std::size_t, 4>{1, 0, 1, 1},
    std::array<std::size_t, 4>{1, 0, 2, 0},
    std::array<std::size_t, 4>{1, 0, 2, 1},

    std::array<std::size_t, 4>{2, 0, 0, 0},
    std::array<std::size_t, 4>{2, 0, 0, 1},
    std::array<std::size_t, 4>{2, 0, 1, 0},
    std::array<std::size_t, 4>{2, 0, 1, 1},
    std::array<std::size_t, 4>{2, 0, 2, 0},
    std::array<std::size_t, 4>{2, 0, 2, 1},

    std::array<std::size_t, 4>{3, 0, 0, 0},
    std::array<std::size_t, 4>{3, 0, 0, 1},
    std::array<std::size_t, 4>{3, 0, 1, 0},
    std::array<std::size_t, 4>{3, 0, 1, 1},
    std::array<std::size_t, 4>{3, 0, 2, 0},
    std::array<std::size_t, 4>{3, 0, 2, 1}
};

template<class T>
void test(T& sz)
{
    upp::ndindex index{sz};
    BOOST_CHECK_EQUAL(index.number_of_dimensions(), 4);
    BOOST_CHECK_EQUAL(index.max_size_t(), all_idx.size());

    const auto& sod = index.size_of_dimensions();
    BOOST_CHECK_EQUAL_COLLECTIONS(sod.begin(), sod.end(), sz.begin(), sz.end());

    BOOST_CHECK_EQUAL(index.size_of_dimension(0), 4);
    BOOST_CHECK_EQUAL(index.size_of_dimension(1), 1);
    BOOST_CHECK_EQUAL(index.size_of_dimension(2), 3);
    BOOST_CHECK_EQUAL(index.size_of_dimension(3), 2);


    std::vector<std::size_t> idx1{0, 0, 0, 0};
    std::vector<std::size_t> idx2{3, 0, 2, 1};
    auto fb = index.begin();
    auto rb = index.rbegin();
    auto fe = index.end();
    auto re = index.rend();
    for(
        std::size_t i = 0;
        i < all_idx.size();
        ++i,
        index.increment(idx1),
        index.decrement(idx2),
        ++fb,
        ++rb
    )
    {
        BOOST_CHECK(fb != fe);
        BOOST_CHECK(rb != re);
        const auto& idx3 = index.to_index(i);
        const auto& idxf = *fb;
        const auto& idxr = *rb;

        #define print_ar(n)             \
            std::cout << #n "\t  "      \
                      << n.at(0) << " " \
                      << n.at(1) << " " \
                      << n.at(2) << " " \
                      << n.at(3)<< std::endl;


        std::cout << "-------------------" << std::endl;
        std::cout << "fwd" << std::endl;
        const auto& idxi = all_idx.at(i);
        print_ar(idxi);
        print_ar(idx1);
        print_ar(idx3);
        print_ar(idxf);

        std::cout << "rev" << std::endl;
        const auto i2 = all_idx.size() - i - 1;
        const auto& idxri = all_idx.at(i2);
        print_ar(idxri);
        print_ar(idx2);
        print_ar(idxr);

        BOOST_CHECK_EQUAL_COLLECTIONS(idx1.begin(), idx1.end(), idxi.begin() , idxi.end() );
        BOOST_CHECK_EQUAL_COLLECTIONS(idx3.begin(), idx3.end(), idxi.begin() , idxi.end() );
        BOOST_CHECK_EQUAL_COLLECTIONS(idxf.begin(), idxf.end(), idxi.begin() , idxi.end() );
        BOOST_CHECK_EQUAL_COLLECTIONS(idx2.begin(), idx2.end(), idxri.begin(), idxri.end());
        BOOST_CHECK_EQUAL_COLLECTIONS(idxr.begin(), idxr.end(), idxri.begin(), idxri.end());
    }
    BOOST_CHECK(fb == fe);
    BOOST_CHECK(rb == re);
}

BOOST_AUTO_TEST_CASE( test_ndindex_array )
{
    std::array<std::size_t, 4> sz{4, 1, 3, 2};
    test(sz);
}

BOOST_AUTO_TEST_CASE( test_ndindex_vector )
{
    std::vector<std::size_t> sz{4, 1, 3, 2};
    test(sz);
}

BOOST_AUTO_TEST_CASE( test_ndindex_span )
{
    std::vector<std::size_t> szv{4, 1, 3, 2};
    upp::span sz(szv);
    test(sz);
}

