#pragma once

#include <string>

namespace upp
{
    std::string to_hex_string(std::uintmax_t i, int w = 0, char fill = '0');
}
