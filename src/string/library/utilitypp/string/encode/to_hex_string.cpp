#include <iomanip>

#include "../thread_local_string_stream.hpp"
#include "to_hex_string.hpp"

namespace upp
{
    std::string to_hex_string(std::uintmax_t i, int w, char fill)
    {
        thread_local_stringstream_reset() << std::setfill(fill)
                                          << std::hex
                                          << std::setw(w)
                                          << i;
        std::string result = thread_local_stringstream().str();
        return result;
    }
}
