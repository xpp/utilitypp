#pragma once

#include <string>

namespace upp
{
    bool is_hex_string(const std::string& str);
    bool is_hex_string(const char* str);
}
