#include <regex>

#include "is_hex_string.hpp"

namespace upp
{
    bool is_hex_string(const std::string& str)
    {
        return is_hex_string(str.c_str());
    }
    bool is_hex_string(const char* str)
    {
        static const std::regex re{R"(^(0x)?[0-9a-fA-F]+$)"};
        std::cmatch m;
        return std::regex_match(str, m, re);
    }
}
