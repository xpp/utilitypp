#include "thread_local_string_stream.hpp"

namespace upp
{
    std::stringstream& thread_local_stringstream()
    {
        static thread_local std::stringstream s;
        return s;
    }

    std::stringstream& thread_local_stringstream_reset()
    {
        thread_local_stringstream().str("");
        thread_local_stringstream().clear();
        return thread_local_stringstream();
    }
}
