#pragma once

#include <sstream>

namespace upp
{
    std::stringstream& thread_local_stringstream();
    std::stringstream& thread_local_stringstream_reset();
}
