#pragma once

#include <string_view>

namespace upp
{
    std::uintmax_t hex_string_to_uint(const std::string_view& str);

    inline std::uintmax_t hex_string_to_uint(const std::string& str)
    {
        return hex_string_to_uint(std::string_view{str});
    }

    inline std::uintmax_t hex_string_to_uint(const char* str)
    {
        return hex_string_to_uint(std::string_view{str});
    }
}
