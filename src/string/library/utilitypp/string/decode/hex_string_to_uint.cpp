#pragma once

#include <iomanip>

#include "../thread_local_string_stream.hpp"
#include "hex_string_to_uint.hpp"

namespace upp
{
    std::uintmax_t hex_string_to_uint(const std::string_view& str)
    {
        std::uintmax_t u = 0;
        thread_local_stringstream_reset() << std::hex << str;
        thread_local_stringstream() >> u;
        return u;
    }
}
