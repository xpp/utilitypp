#define BOOST_TEST_MODULE string

#include <random>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/string/encode.hpp>

BOOST_AUTO_TEST_CASE(test_to_hex_string)
{
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x00), "0");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x01), "1");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x20), "20");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xA0), "a0");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x0f), "f");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xaf), "af");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xAf), "af");

    BOOST_CHECK_EQUAL(upp::to_hex_string(0x00, 2), "00");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x01, 2), "01");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x20, 2), "20");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xA0, 2), "a0");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x0f, 2), "0f");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xaf, 2), "af");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xAf, 2), "af");

    BOOST_CHECK_EQUAL(upp::to_hex_string(0x00, 2, '#'), "#0");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x01, 2, '#'), "#1");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x20, 2, '#'), "20");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xA0, 2, '#'), "a0");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0x0f, 2, '#'), "#f");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xaf, 2, '#'), "af");
    BOOST_CHECK_EQUAL(upp::to_hex_string(0xAf, 2, '#'), "af");
}
