#define BOOST_TEST_MODULE string

#include <random>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/string/check_format.hpp>


BOOST_AUTO_TEST_CASE(test_is_hex_string)
{

    BOOST_CHECK(upp::is_hex_string("00"));
    BOOST_CHECK(upp::is_hex_string("01"));
    BOOST_CHECK(upp::is_hex_string("20"));
    BOOST_CHECK(upp::is_hex_string("A0"));
    BOOST_CHECK(upp::is_hex_string("0f"));
    BOOST_CHECK(upp::is_hex_string("af"));
    BOOST_CHECK(upp::is_hex_string("Af"));

    BOOST_CHECK(upp::is_hex_string("0x00"));
    BOOST_CHECK(upp::is_hex_string("0x01"));
    BOOST_CHECK(upp::is_hex_string("0x20"));
    BOOST_CHECK(upp::is_hex_string("0xA0"));
    BOOST_CHECK(upp::is_hex_string("0x0f"));
    BOOST_CHECK(upp::is_hex_string("0xaf"));
    BOOST_CHECK(upp::is_hex_string("0xAf"));

    BOOST_CHECK(!upp::is_hex_string("OAf"));
    BOOST_CHECK(!upp::is_hex_string("Ag"));
    BOOST_CHECK(!upp::is_hex_string("A e"));
    BOOST_CHECK(!upp::is_hex_string("Ae "));
    BOOST_CHECK(!upp::is_hex_string(" Ae"));
    BOOST_CHECK(!upp::is_hex_string("-Ae"));
}
