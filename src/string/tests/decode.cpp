#define BOOST_TEST_MODULE string

#include <random>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/string/decode.hpp>


BOOST_AUTO_TEST_CASE(test_hex_string_to_uint)
{
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("00"), 0x00);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("01"), 0x01);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("20"), 0x20);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("A0"), 0xA0);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0f"), 0x0f);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("af"), 0xaf);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("Af"), 0xAf);

    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0x00"), 0x00);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0x01"), 0x01);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0x20"), 0x20);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0xA0"), 0xA0);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0x0f"), 0x0f);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0xaf"), 0xaf);
    BOOST_CHECK_EQUAL(upp::hex_string_to_uint("0xAf"), 0xAf);
}
