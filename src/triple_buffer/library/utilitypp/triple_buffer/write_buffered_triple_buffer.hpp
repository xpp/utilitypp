//#pragma once

//#include "triple_buffer.hpp"

//namespace upp
//{
//    /**
//     * @see TripleBuffer
//     * @brief Same as the TripleBuffer, but partial writes of the data structure are ok.
//     * The write operation may be a bit slower and memory consumption may be 1/3 higher.
//     */
//    template <typename T>
//    class write_buffered_triple_buffer
//    {
//    public:
//        write_buffered_triple_buffer():
//            writeBuffer(T())
//        {}

//        write_buffered_triple_buffer(const T& init):
//            tripleBuffer {init},
//            writeBuffer(init)
//        {}

//        /// @return the write buffer
//        T& getWriteBuffer()
//        {
//            return writeBuffer;
//        }

//        /// @return the write buffer
//        const T& getWriteBuffer() const
//        {
//            return writeBuffer;
//        }
//        /// @return the current read buffer
//        const T& getReadBuffer() const
//        {
//            return tripleBuffer.getReadBuffer();
//        }
//        /// @return the most up to date read buffer
//        const T& getUpToDateReadBuffer() const
//        {
//            return tripleBuffer.getUpToDateReadBuffer();
//        }

//        /**
//         * @brief Swaps in the hidden buffer if it contains new data.
//         * @return True if new data is available
//         */
//        bool updateReadBuffer() const
//        {
//            return tripleBuffer.updateReadBuffer();
//        }

//        void commitWrite()
//        {
//            tripleBuffer.getWriteBuffer() = writeBuffer;
//            tripleBuffer.commitWrite();
//        }

//        void reinitAllBuffers(const T& init)
//        {
//            writeBuffer = init;
//            tripleBuffer.reinitAllBuffers(init);
//        }
//    private:
//        TripleBuffer<T> tripleBuffer;
//        T writeBuffer;
//    };
//}
