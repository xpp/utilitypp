//#pragma once

//#include <tuple>
//#include <type_traits>
//#include <atomic>

//namespace upp
//{
//    /**
//     * @brief A simple triple buffer for lockfree comunication between a single writer and a single reader.
//     *
//     * If more than one reader/writer is required, you have to synchronize the access.
//     * Writers must not have: writes to the same data cell (except it is an atomic et al) and no writes are allowed during swapping of the buffer.
//     * Readers must not read when the buffer is swapped.
//     * If a writer only writes parts of the data structure, data loss could occure! (use a write_buffered_triple_buffer instead)
//     *
//     */
//    template <typename T>
//    class triple_buffer
//    {
//    public:
//        template<class U = T>
//        triple_buffer(std::enable_if_t<std::is_default_constructible_v<U>>* = nullptr)
//        {}

//        template<class U = T>
//        triple_buffer(const T& init, std::enable_if_t<std::is_copy_constructible_v<U>>* = nullptr) :
//            buffer{init, init, init}
//        {}

//        template<class U = T>
//        triple_buffer(T&& init_read, T&& init_write, T&& init_hidden, std::enable_if_t<std::is_copy_constructible_v<U>>* = nullptr) :
//            buffer{std::move(init_read), std::move(init_hidden), std::move(init_write)}
//        {
//            static_assert (read_index_shift < hidden_index_shift);
//            static_assert (hidden_index_shift < write_index_shift);
//        }

//        // non-copyable behavior
//        triple_buffer<T>(const triple_buffer<T>&) = delete;
//        triple_buffer<T>& operator=(const triple_buffer<T>&) = delete;

//        /// @return the write buffer
//        T& getWriteBuffer()
//        {
//            return buffer[getWriteBufferIndex()];
//        }
//        /// @return the write buffer
//        const T& getWriteBuffer() const
//        {
//            return buffer[getWriteBufferIndex()];
//        }
//        /// @return the current read buffer
//        const T& getReadBuffer() const
//        {
//            return buffer[getReadBufferIndex() ];
//        }
//        /// @return the most up to date read buffer
//        const T& getUpToDateReadBuffer() const
//        {
//            updateReadBuffer();
//            return getReadBuffer();
//        }

//        /**
//         * @brief Swaps in the hidden buffer if it contains new data.
//         * @return True if new data is available
//         */
//        bool updateReadBuffer() const
//        {
//            std::uint_fast8_t flagsNow(flags.load(std::memory_order_consume));

//            if (!hasNewWrite(flagsNow))
//            {
//                return false;
//            }

//            while (!flags.compare_exchange_weak(flagsNow, swap_read_hidden(flagsNow), std::memory_order_release, std::memory_order_consume));
//            return true;
//        }

//        void commitWrite()
//        {
//            swapWriteAndHiddenBuffer();
//        }

//        template<class U = T>
//        typename std::enable_if<std::is_copy_constructible<U>::value>::type reinitAllBuffers(const T& init)
//        {
//            buffer[0] = init;
//            buffer[1] = init;
//            buffer[2] = init;
//        }
//        void reinitAllBuffers(T&& writeBuff, T&& hiddenBuff, T&& readBuff)
//        {
//            buffer[getWriteBufferIndex() ] = std::move(writeBuff);
//            buffer[getHiddenBufferIndex()] = std::move(hiddenBuff);
//            buffer[getReadBufferIndex()  ] = std::move(readBuff);
//        }

//    protected:

//        // /////////////////////////////////////////////////////////////////////////////// //
//        // get indices
//        std::uint_fast8_t getReadBufferIndex()   const
//        {
//            return (flags.load(std::memory_order_consume) & read_index_mask) >> read_index_shift ;
//        }
//        std::uint_fast8_t getWriteBufferIndex()  const
//        {
//            return (flags.load(std::memory_order_consume) & write_index_mask) >> write_index_shift;
//        }
//        std::uint_fast8_t getHiddenBufferIndex() const
//        {
//            return (flags.load(std::memory_order_consume) & hidden_index_mask) >> hidden_index_shift;
//        }

//        // /////////////////////////////////////////////////////////////////////////////// //
//        //buffer operations
//        /// @brief Swap the write buffer with the hidden buffer
//        void swapWriteAndHiddenBuffer()
//        {
//            std::uint_fast8_t flagsNow(flags.load(std::memory_order_consume));
//            while (!flags.compare_exchange_weak(flagsNow, swap_write_hidden(flagsNow), std::memory_order_release, std::memory_order_consume));
//        }
//        // void swapReadAndHiddenBuffer(); is done by updateReadBuffer()

//        // /////////////////////////////////////////////////////////////////////////////// //
//        //flag opertions
//        // check if the newWrite bit is 1
//        bool hasNewWrite(uint_fast8_t flags) const
//        {
//            return flags & dirty_bit_mask;
//        }
//        /// swap read and hidden indexes
//        /**
//         * @brief swap read and hidden indexes of given flags (set dirty to 0)
//         * @param flags the current flags
//         * @return the new flags
//         */
//        static constexpr std::uint_fast8_t swap_read_hidden(uint_fast8_t flags);

//        /**
//         * @brief swap write and hidden indexes of given flags (set dirty to 1)
//         * @param flags the current flags
//         * @return the new flags
//         */
//        static constexpr std::uint_fast8_t swap_write_hidden(uint_fast8_t flags);

//        // /////////////////////////////////////////////////////////////////////////////// //
//        // constants
//        static constexpr std::uint_fast8_t read_index_shift = 0;
//        static constexpr std::uint_fast8_t hidden_index_shift = 2;
//        static constexpr std::uint_fast8_t write_index_shift = 4;

//        static constexpr std::uint_fast8_t hidden_read_shift  = hidden_index_shift - read_index_shift;
//        static constexpr std::uint_fast8_t hidden_write_shift = write_index_shift - hidden_index_shift;

//        static constexpr std::uint_fast8_t read_index_mask   = 0b00'00'00'11 << read_index_shift  ; //0b00'00'00'11;
//        static constexpr std::uint_fast8_t hidden_index_mask = 0b00'00'00'11 << hidden_index_shift; //0b00'00'11'00;
//        static constexpr std::uint_fast8_t write_index_mask  = 0b00'00'00'11 << write_index_shift ; //0b00'11'00'00;
//        static constexpr std::uint_fast8_t dirty_bit_mask    = 0b10'00'00'00;
//        // initially dirty = 0, write 0, hidden = 1, read = 2
//        static const std::uint_fast8_t initial_flags          = 0b00'00'01'10; //= 2 + (1 << 2); //0b00000110; w=0, h=1, r=0

//        // /////////////////////////////////////////////////////////////////////////////// //
//        // data
//        mutable std::atomic_uint_fast8_t flags {initial_flags};
//        std::array<T, 3> buffer[3];
//    };

//    template<typename T>
//    std::uint_fast8_t triple_buffer<T>::swap_read_hidden(std::uint_fast8_t flags)
//    {
//        return 0      //set dirty bit to 0
//                | ((flags & hidden_index_mask) >> hidden_read_shift) // hidden index now is read   index
//                | ((flags & read_index_mask) << hidden_read_shift)     // read   index now is hidden index
//                | (flags & write_index_mask);   // keep write index
//    }

//    template<typename T>
//    std::uint_fast8_t triple_buffer<T>::swap_write_hidden(std::uint_fast8_t flags)
//    {
//        return dirty_bit_mask         //set dirty bit to 1
//               | ((flags & hidden_index_mask) << hidden_write_shift) // hidden index now is write  index
//               | ((flags & write_index_mask) >> hidden_write_shift)  // write  index now is hidden index
//               | (flags & read_index_mask);    // keep read index
//    }
//}
