#define BOOST_TEST_MODULE util
#include <sstream>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/io/varout.hpp>

BOOST_AUTO_TEST_CASE( varout )
{
    int i = 0;
    {
        std::stringstream s;
        s << upp_varout(i);
        BOOST_REQUIRE_EQUAL(s.str()   ,  "'i' (0)");
    }
    {
        std::stringstream s;
        s << upp_varout_no_parens(i);
        BOOST_REQUIRE_EQUAL(s.str()   ,  "'i' 0");
    }
}
