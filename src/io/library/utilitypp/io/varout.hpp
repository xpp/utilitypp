#pragma once

#include <utilitypp/preprocessor/stringify.hpp>

#define upp_varout(...) _detail_upp_varout_expand(__VA_ARGS__)
#define _detail_upp_varout_expand(...)                                \
    "'" << upp_stringify(__VA_ARGS__) << "' (" << __VA_ARGS__ << ")"

#define upp_varout_no_parens(...) _detail_upp_varout_no_parens_expand(__VA_ARGS__)
#define _detail_upp_varout_no_parens_expand(...)                      \
    "'" << upp_stringify(__VA_ARGS__) << "' " << __VA_ARGS__
