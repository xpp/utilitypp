#pragma once

#include <boost/current_function.hpp>

#include <utilitypp/preprocessor/stringify.hpp>

#define upp_streamout_file_line      __FILE__ ":" upp_stringify(__LINE__)
#define upp_streamout_file_line_func upp_streamout_file_line " (" << BOOST_CURRENT_FUNCTION << ")"

#define upp_streamout_multiline_file_line(...)                  \
    __VA_ARGS__ << "    File: " << __FILE__ << "\n" <<          \
    __VA_ARGS__ << "    Line: " << __LINE__ << "\n" <<          \

#define upp_streamout_multiline_file_line_func(...)             \
    __VA_ARGS__ << "    File    : " << __FILE__ << "\n" <<      \
    __VA_ARGS__ << "    Line    : " << __LINE__ << "\n" <<      \
    __VA_ARGS__ << "    Function: " << BOOST_CURRENT_FUNCTION
