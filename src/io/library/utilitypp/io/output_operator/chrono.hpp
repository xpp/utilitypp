#pragma once

#include <chrono>
#include <ostream>

namespace upp::output_operator::chrono
{
    std::ostream& operator << (std::ostream& out, const std::chrono::hours& t)
    {
        return out << t.count() << " h";
    }
    std::ostream& operator << (std::ostream& out, const std::chrono::minutes& t)
    {
        return out << t.count() << " m";
    }
    std::ostream& operator << (std::ostream& out, const std::chrono::seconds& t)
    {
        return out << t.count() << " s";
    }
    std::ostream& operator << (std::ostream& out, const std::chrono::milliseconds& t)
    {
        return out << t.count() << " ms";
    }
    std::ostream& operator << (std::ostream& out, const std::chrono::microseconds& t)
    {
        return out << t.count() << " us";
    }
    std::ostream& operator << (std::ostream& out, const std::chrono::nanoseconds& t)
    {
        return out << t.count() << " ns";
    }
}
