#define BOOST_TEST_MODULE template_traits

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/template_traits.hpp>

struct t {};

template<class...> struct types {};
template<int...> struct vals {};

template<class T, T...> struct typed_vals {};

#include <iostream>
#include <utility>

#define check(...) (upp::is_template_instance_v<__VA_ARGS__>)

BOOST_AUTO_TEST_CASE( test_is_template_instance_v )
{
    BOOST_CHECK(!check( t                                ));

    BOOST_CHECK( check( types                <         > ));
    BOOST_CHECK( check( types                <t        > ));
    BOOST_CHECK( check( types                <t, t     > ));

    BOOST_CHECK( check( vals                 <         > ));
    BOOST_CHECK( check( vals                 <1        > ));
    BOOST_CHECK( check( vals                 <1, 2     > ));

    BOOST_CHECK( check( typed_vals           <int      > ));
    BOOST_CHECK( check( typed_vals           <int, 1   > ));
    BOOST_CHECK( check( typed_vals           <int, 1, 2> ));

    BOOST_CHECK( check( std::ostream                     ));
    BOOST_CHECK( check( std::bool_constant   <true     > ));
    BOOST_CHECK( check( std::integer_sequence<int      > ));
    BOOST_CHECK( check( std::integer_sequence<int, 1   > ));
    BOOST_CHECK( check( std::integer_sequence<int, 1, 2> ));
    BOOST_CHECK( check( std::enable_if       <1        > ));
}
