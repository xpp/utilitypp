#include <type_traits>

namespace upp
{
    //default case: no template
    template<class T>
    struct is_template_instance : std::false_type {};

    //types
    template<template<class...> class Template, class...Args>
    struct is_template_instance<Template<Args...>> : std::true_type
    {
        template<template<class...> class Template2>
        using replace_template = Template2<Args...>;

        template<class...Args2>
        using replace_parameters = Template<Args2...>;
    };

    //typed vals
    template<template<class T, T...> class Template, class ArgT, ArgT...Vals>
    struct is_template_instance<Template<ArgT, Vals...>> : std::true_type
    {
        template<template<class T2, T2...> class Template2>
        using replace_template = Template2<ArgT, Vals...>;

        template<class ArgT2, ArgT2...Vals2>
        using replace_parameters = Template<ArgT2, Vals2...>;
    };

    //val and types
    template<class T, template<T, class...> class Template, T Val, class...Args>
    struct is_template_instance<Template<Val, Args...>> : std::true_type
    {
        template<template<T, class...> class Template2>
        using replace_template = Template2<Val, Args...>;

        template<T Val2, class...Args2>
        using replace_parameters = Template<Val2, Args2...>;
    };

    //vals
    template<class T, template<T...> class Template, T...Vals>
    struct is_template_instance<Template<Vals...>> : std::true_type
    {
        template<template<T...> class Template2>
        using replace_template = Template2<Vals...>;

        template<T...Vals2>
        using replace_parameters = Template<Vals2...>;
    };


    template<class T>
    static constexpr bool is_template_instance_v = is_template_instance<T>::value;
}
