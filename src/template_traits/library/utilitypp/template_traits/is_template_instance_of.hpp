#pragma once

#include <type_traits>

namespace upp
{
    //default case: no template
    template<template<class...> class Template, class T>
    struct is_template_instance_of : std::false_type {};

    template<template<class...> class Template, class...Ts>
    struct is_template_instance_of<Template, Template<Ts...>> : std::true_type {};

    template<template<class...> class Template, class T>
    static constexpr bool is_template_instance_of_v = is_template_instance_of<Template, T>::value;
}
