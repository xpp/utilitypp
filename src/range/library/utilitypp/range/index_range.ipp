#pragma once

#include <type_traits>

#include <utilitypp/exception/throw_if.hpp>

#include "index_range.hpp"

namespace upp
{
    template <class ValueT>
    inline index_range<ValueT>::index_range(value_t end) :
        index_range(0, end)
    {}

    template <class ValueT>
    inline index_range<ValueT>::index_range(value_t begin, value_t end, bool allow_overflow) :
        _begin{begin},
        _end{end}
    {
        upp_throw_if(std::invalid_argument, !allow_overflow && end < begin);
    }

    template <class ValueT>
    template<class T, class>
    inline index_range<ValueT>::index_range(const T& container) :
        index_range(container.size())
    {}


    template <class ValueT>
    inline detail::index_range_iterator<ValueT> index_range<ValueT>::begin() const
    {
        return _begin;
    }
    template <class ValueT>
    inline detail::index_range_iterator<ValueT> index_range<ValueT>::end() const
    {
        return _end;
    }

}

namespace upp::detail
{
    template <class ValueT>
    class index_range_iterator
    {
    public:
        using value_t = ValueT;
        index_range_iterator(value_t v) : _v{v} {}

        bool operator==(index_range_iterator other) const
        {
            return _v == other._v;
        }
        bool operator!=(index_range_iterator other) const
        {
            return _v != other._v;
        }
        operator value_t() const
        {
            return _v;
        }
        value_t operator*() const
        {
            return _v;
        }
        value_t operator++()
        {
            return ++_v;
        }
        private:
            value_t _v;
    };
}
