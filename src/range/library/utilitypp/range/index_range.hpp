#pragma once

#include <cstddef>
#include <type_traits>

#include <utilitypp/type_traits/range/is_sized_range.hpp>

namespace upp::detail
{
    template <class ValueT>
    class index_range_iterator;
}

namespace upp
{
    template <class ValueT = std::size_t>
    class index_range
    {
    public:
        using value_t = ValueT;
        using iterator = upp::detail::index_range_iterator<value_t>;

        template<class T, class = std::enable_if_t<upp::is_sized_range_v<T>>>
        index_range(const T& container);

        index_range(value_t end);
        index_range(value_t begin, value_t end, bool allow_overflow = false);

        iterator begin() const;
        iterator end() const;

    private:
        iterator _begin;
        iterator _end;
    };
}

#include "index_range.ipp"
