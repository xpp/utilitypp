#pragma once

#include <type_traits>

#include <boost/iterator/counting_iterator.hpp>

#include <utilitypp/iterator/zip_iterator.hpp>

#include "iterator_range.hpp"

namespace upp
{
    template <
        class ContainerT,
        class ContainerIteratorT = std::conditional_t
        <
            std::is_const_v<ContainerT>,
            typename ContainerT::const_iterator,
            typename ContainerT::iterator
            >,
        class ZipIter = zip_iterator<boost::counting_iterator<std::size_t>, ContainerIteratorT>
        >
    class indexed_container_range :
        public iterator_range<ZipIter>
    {
    public:
        indexed_container_range(ContainerT& c) :
            iterator_range<ZipIter>(
                zip_iterator{boost::counting_iterator<std::size_t>(0), c.begin()},
                zip_iterator{boost::counting_iterator<std::size_t>(c.size()), c.end()}
            )
        {}
    };
}
