#pragma once

namespace upp
{
    template<class It>
    struct iterator_range
    {
        iterator_range(It b, It e)
            : _begin{std::move(b)}, _end{std::move(e)}
        {}
        It begin() const
        {
            return _begin;
        }
        It end() const
        {
            return _end;
        }
    private:
        It _begin;
        It _end;
    };
}
