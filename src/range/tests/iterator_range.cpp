#define BOOST_TEST_MODULE range
#include <array>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/range/iterator_range.hpp>

BOOST_AUTO_TEST_CASE(compare)
{
    std::array<int, 10> ar{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::size_t i = 0;
    for (const auto e : upp::iterator_range{ar.begin(), ar.end()})
    {
        BOOST_CHECK_EQUAL(e, ar.at(i++));
    }
}
