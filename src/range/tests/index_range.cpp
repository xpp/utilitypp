#define BOOST_TEST_MODULE range
#include <boost/test/included/unit_test.hpp>

#include <array>

#include <utilitypp/range/index_range.hpp>

BOOST_AUTO_TEST_CASE( index_range_count_up )
{
    std::size_t i1 = 0;
    for(auto i2 : upp::index_range{100})
    {
        BOOST_REQUIRE_EQUAL(i1,  i2);
        ++i1;
    }
    BOOST_REQUIRE_EQUAL(i1,  100);
}

BOOST_AUTO_TEST_CASE( index_range_count_up_from_start )
{
    std::size_t i1 = 10;
    for(auto i2 : upp::index_range{10, 100})
    {
        BOOST_REQUIRE_EQUAL(i1,  i2);
        ++i1;
    }
    BOOST_REQUIRE_EQUAL(i1,  100);
}

BOOST_AUTO_TEST_CASE( index_range_container )
{
    std::size_t i1 = 0;
    std::array a{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    for(auto i2 : upp::index_range{10})
    {
        BOOST_REQUIRE_EQUAL(i1,  i2);
        BOOST_REQUIRE_EQUAL(a.at(i1),  i2);
        ++i1;
    }
    BOOST_REQUIRE_EQUAL(i1,  10);
}
