#define BOOST_TEST_MODULE range
#include <array>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/range/indexed_container_range.hpp>

BOOST_AUTO_TEST_CASE(indexed_container_range_mutable)
{
    std::array<int, 10> ar{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::size_t idx = 0;
    for (const auto [i, e] : upp::indexed_container_range{ar})
    {
        BOOST_CHECK_EQUAL(i, idx);
        BOOST_CHECK_EQUAL(e, ar.at(idx++));
        BOOST_CHECK_EQUAL(i, e);
        e += i;
    }
    idx = 0;
    for (const auto [i, e] : upp::indexed_container_range{ar})
    {
        BOOST_CHECK_EQUAL(i, idx);
        BOOST_CHECK_EQUAL(e, ar.at(idx++));
        BOOST_CHECK_EQUAL(2 * i, e);
    }
}
BOOST_AUTO_TEST_CASE(indexed_container_range_const)
{
    const std::array<int, 10> ar{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::size_t idx = 0;
    for (const auto [i, e] : upp::indexed_container_range{ar})
    {
        BOOST_CHECK_EQUAL(i, idx);
        BOOST_CHECK_EQUAL(e, ar.at(idx++));
        BOOST_CHECK_EQUAL(i, e);
    }
}
