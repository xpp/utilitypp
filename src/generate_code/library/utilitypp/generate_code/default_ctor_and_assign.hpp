#pragma once

//non constexpr versions

#define upp_default_copy_ctor(...)         __VA_ARGS__           (const __VA_ARGS__&) = default
#define upp_default_copy_assign(...)       __VA_ARGS__& operator=(const __VA_ARGS__&) = default
#define upp_default_copy(...)              upp_default_copy_ctor(__VA_ARGS__); upp_default_copy_assign(__VA_ARGS__)

#define upp_default_move_ctor(...)         __VA_ARGS__           (__VA_ARGS__&&) = default
#define upp_default_move_assign(...)       __VA_ARGS__& operator=(__VA_ARGS__&&) = default
#define upp_default_move(...)              upp_default_move_ctor(__VA_ARGS__); upp_default_move_assign(__VA_ARGS__)

#define upp_default_copy_move(...)         upp_default_copy(__VA_ARGS__); upp_default_move(__VA_ARGS__)

//constexpr versions

#define upp_default_cexpr_copy_ctor(...)   constexpr upp_default_copy_ctor(__VA_ARGS__)
#define upp_default_cexpr_copy_assign(...) constexpr upp_default_copy_assign(__VA_ARGS__)
#define upp_default_cexpr_copy(...)        upp_default_cexpr_copy_ctor(__VA_ARGS__); upp_default_cexpr_copy_assign(__VA_ARGS__)

#define upp_default_cexpr_move_ctor(...)   constexpr upp_default_move_ctor(__VA_ARGS__)
#define upp_default_cexpr_move_assign(...) constexpr upp_default_move_assign(__VA_ARGS__)
#define upp_default_cexpr_move(...)        upp_default_cexpr_move_ctor(__VA_ARGS__); upp_default_cexpr_move_assign(__VA_ARGS__)

#define upp_default_cexpr_copy_move(...)   upp_default_cexpr_copy(__VA_ARGS__); upp_default_cexpr_move(__VA_ARGS__)
