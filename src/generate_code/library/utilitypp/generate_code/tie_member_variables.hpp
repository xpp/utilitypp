#pragma once

#include <tuple>

#include <utilitypp/preprocessor/drop_first/drop_first_1.hpp>
#include <utilitypp/preprocessor/for_each.hpp>

#define _detail_upp_tie_members_macro(r, data, elem) ,(data).*elem

#define upp_tie_members(instance, ...)          \
    std::tie                                    \
    (                                           \
        upp_drop_first_1                        \
        (                                       \
            upp_for_each                        \
            (                                   \
                _detail_upp_tie_members_macro,  \
                instance,                       \
                __VA_ARGS__                     \
            )                                   \
        )                                       \
    )
