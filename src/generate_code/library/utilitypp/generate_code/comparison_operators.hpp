#pragma once

#include "tie_member_variables.hpp"

#define _detail_upp_dummy_type(suffix) using _detail_upp_dummy_type_##suffix = int

#define upp_operator_compare_in_class(T, Op ,...)       \
    bool operator Op(const T& other) const              \
    {                                                   \
        return upp_tie_members(*this, __VA_ARGS__) Op   \
               upp_tie_members(other, __VA_ARGS__);     \
    }

#define upp_operator_in_class_eq( T,...) upp_operator_compare_in_class(T, ==, __VA_ARGS__) _detail_upp_dummy_type(upp_operator_in_class_eq )
#define upp_operator_in_class_ieq(T,...) upp_operator_compare_in_class(T, !=, __VA_ARGS__) _detail_upp_dummy_type(upp_operator_in_class_ieq)

#define upp_operator_in_class_lt( T,...) upp_operator_compare_in_class(T, < , __VA_ARGS__) _detail_upp_dummy_type(upp_operator_in_class_lt )
#define upp_operator_in_class_gt( T,...) upp_operator_compare_in_class(T, > , __VA_ARGS__) _detail_upp_dummy_type(upp_operator_in_class_gt )

#define upp_operator_in_class_leq(T,...) upp_operator_compare_in_class(T, <=, __VA_ARGS__) _detail_upp_dummy_type(upp_operator_in_class_leq)
#define upp_operator_in_class_geq(T,...) upp_operator_compare_in_class(T, >=, __VA_ARGS__) _detail_upp_dummy_type(upp_operator_in_class_geq)

#define upp_operator_in_class_eq_ieq(T,...)           \
    upp_operator_in_class_eq( T, __VA_ARGS__);        \
    upp_operator_in_class_ieq(T, __VA_ARGS__)

#define upp_operator_in_class_comparison_all(T,...)   \
    upp_operator_in_class_eq_ieq( T, __VA_ARGS__);    \
    upp_operator_in_class_lt( T, __VA_ARGS__);        \
    upp_operator_in_class_gt( T, __VA_ARGS__);        \
    upp_operator_in_class_leq(T, __VA_ARGS__);        \
    upp_operator_in_class_geq(T, __VA_ARGS__)

