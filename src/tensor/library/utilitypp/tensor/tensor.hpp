#pragma once

#include <memory>
#include <functional>
#include <numeric>

#include <utilitypp/exception/throw_if.hpp>
#include <utilitypp/ndimensional/ndindex.hpp>
#include <utilitypp/ndimensional/ndview.hpp>

#include "tensor_options.hpp"
#include "tensor_block.hpp"

namespace upp
{
    inline constexpr struct tensor_construct_with_generator_t {} tensor_construct_with_generator;
    inline constexpr struct tensor_construct_with_emplace_t   {} tensor_construct_with_emplace;

    template<class Type, class Options = tensor_options<>>
    class tensor
    {
        static_assert(std::is_nothrow_destructible_v<Type>);
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// typedefs ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        using value_t   = Type;
        using options_t = Options;
        using block_t   = upp::detail::tensor_block<Type, Options>;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// ctor //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    protected:
        inline static constexpr struct init_ctor_tag_t {} init_ctor_tag{};
        tensor(init_ctor_tag_t, const std::vector<std::size_t>& sz)
        {
            const std::size_t number_of_dimensions = sz.size();
            if(number_of_dimensions == 0)
            {
                return;
            }
            const std::size_t number_of_elements = std::accumulate(sz.begin(), sz.end(), 1, std::multiplies<>{});
            upp_throw_if_equal_0(std::invalid_argument, number_of_elements);
            _block = upp::detail::make_tensor_block<value_t, options_t>(number_of_dimensions, number_of_elements);
            _dimension_sizes = _block->size_of_dimensions();
            _elements = _block->elements();
            upp_throw_if_not_equal(std::logic_error, _dimension_sizes.size(), number_of_dimensions);
            upp_throw_if_not_equal(std::logic_error, _elements.size(), number_of_elements);
            std::copy(sz.begin(), sz.end(), _dimension_sizes.begin());
        }
    public:
        tensor() ///TODO sfinae default_ctor
            : tensor(init_ctor_tag, {})
        {}


        template<class T = Type, class = std::enable_if_t<std::is_default_constructible_v<T>>>
        tensor(const std::vector<std::size_t>& sz)
            : tensor(init_ctor_tag, sz)
        {
             if constexpr(std::is_class_v<value_t> && !std::is_aggregate_v<value_t>)
             {
                for(auto it = _elements.begin(); it != _elements.end(); ++it, ++_block->_initialized)
                {
                    new(it) value_t;
                }
            }
        }

         template<class Val, class T = Type/*, class = std::enable_if_t<!std::is_class_v<T>*//* && std::is_assignable_v<T, Val>*//*>*/>
         tensor(const std::vector<std::size_t>& sz, const Val& val)
             : tensor(init_ctor_tag, sz)
         {
             for(auto it = _elements.begin(); it != _elements.end(); ++it, ++_block->_initialized)
             {
                 new(it) value_t(val);
             }
         }

        template<class T = Type, class = std::enable_if_t<std::is_class_v<T>>>
        tensor(const std::vector<std::size_t>& sz, tensor_construct_with_emplace_t, const auto& param0, const auto&...params) ///TODO sfinae ctor
            : tensor(init_ctor_tag, sz)
        {
            for(auto it = _elements.begin(); it != _elements.end(); ++it, ++_block->_initialized)
            {
                if constexpr(std::is_aggregate_v<value_t>)
                {
                    *it = {param0, params...};
                }
                else
                {
                    new(it) value_t;
                }
            }
        }
        template<class Gen>
        tensor(const std::vector<std::size_t>& sz, tensor_construct_with_generator_t, Gen gen) ///TODO sfinae ctor
            : tensor(init_ctor_tag, sz)
        {
            constexpr bool callable_vec     = std::is_invocable_v<Gen, std::vector<std::size_t>>;
            constexpr bool callable_span    = std::is_invocable_v<Gen, upp::span<std::size_t>>;
            constexpr bool callable_no_args = std::is_invocable_v<Gen>;
            static_assert
            (
                callable_vec  ||
                callable_span ||
                callable_no_args
            );

            const auto construct = [&gen](auto it, auto...params)
            {
                if constexpr(std::is_class_v<value_t> && !std::is_aggregate_v<value_t>)
                {
                    new(it) value_t(gen(params...));
                }
                else
                {
                    *it = gen(params...);
                }
            };

            if constexpr (callable_vec || callable_span)
            {
                std::vector<std::size_t> vec(sz.size(), 0);
                const ndindex idx{sz};
                for(auto it = _elements.begin(); it != _elements.end(); ++it, ++_block->_initialized, idx.increment(vec))
                {
                    if constexpr (callable_vec)
                    {
                        construct(it, vec);
                    }
                    else
                    {
                        construct(it, span{vec});
                    }
                }
            }
            else
            {
                for(auto it = _elements.begin(); it != _elements.end(); ++it, ++_block->_initialized)
                {
                    construct(it);
                }
            }
        }

        template<class T = Type, class = std::enable_if_t<std::is_class_v<T>>>
        tensor(const std::vector<std::size_t>& sz, const auto&...params) ///TODO sfinae ctor
            : tensor(sz, tensor_construct_with_emplace, params...)
        {}

        tensor(tensor&&) = default;
//        tensor(const tensor& other)              ///TODO sfinae cpy
//        {
//            ///TODO
//        }


        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// assign /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        tensor& operator=(tensor&&) = default;
//        tensor& operator=(const tensor& other)   ///TODO sfinae cpy ass
//        {
//            ///TODO
//            return *this;
//        }

//        template<class Gen>
//        tensor(const dimension_sizes_t& sz, Gen gen) :
//            tensor(sz)
//        {
//            std::generate(storage_front(), storage_back(), gen); ///TODO fnc on ref / tensor
//            ///TODO constexpr if gen with index
//        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// storage ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        value_t* data()
        {
            return _block ? _elements.begin() : nullptr;
        }
        const value_t* data() const
        {
            return _block ? _elements.begin() : nullptr;
        }

        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// iterator ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        value_t* elements_begin()
        {
            return data();
        }
        const value_t* elements_begin() const
        {
            return data();
        }

        value_t* elements_end()
        {
            return elements_begin() + size();
        }
        const value_t* elements_end() const
        {
            return elements_begin() + size();
        }

        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// size //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        std::size_t size() const
        {
            return _block ? _elements.size() : 0;
        }
        std::size_t size_in_bytes() const
        {
            return size() * sizeof (value_t);
        }

        std::size_t number_of_dimensions() const
        {
            return _dimension_sizes.size();
        }

        std::size_t size_of_dimension(std::size_t i) const
        {
            upp_throw_if_not_less(std::invalid_argument, i, number_of_dimensions());
            return _dimension_sizes.at(i);
        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// access /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
        span<value_t> elements()
        {
            return _elements;
        }
        const span<value_t>& elements() const
        {
            return _elements;
        }
    public:
    private:
        std::shared_ptr<block_t> _block;
        span<std::size_t> _dimension_sizes;
        span<value_t> _elements;
    };
}
