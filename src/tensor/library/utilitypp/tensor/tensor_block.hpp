#pragma once

#include <stdexcept>
#include <memory>

#include <utilitypp/exception/throw_if.hpp>
#include <utilitypp/span.hpp>

#include "tensor_options.hpp"

namespace upp::detail
{
    //declare
    template<class Type, class Options>
    class tensor_block;

    template<class Type, class Options>
    struct tensor_block_deleter;

    template<class Type, class Options>
    std::shared_ptr<tensor_block<Type, Options>>
    make_tensor_block(std::size_t number_of_dimensions, std::size_t number_of_elements);

    //define
    template<class Type, class Options>
    class tensor_block
    {
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// typedefs ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        using value_t = Type;
        using options_t = Options;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////////////  counts ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        std::size_t number_of_dimensions()
        {
            return _number_of_dimensions;
        }
        std::size_t number_of_elements()
        {
            return _number_of_elements;
        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////// ptrs / spans //////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        std::size_t* address_of_dimensions()
        {
            return 1 + &_number_of_elements;
        }

        upp::span<std::size_t> size_of_dimensions()
        {
            return upp::span<std::size_t>{address_of_dimensions(), _number_of_dimensions};
        }
        upp::span<value_t> elements()
        {
            void* first = static_cast<void*>(address_of_dimensions() + _number_of_dimensions);
            std::size_t dummy_space = std::numeric_limits<std::size_t>::max();
            std::align(alignof(value_t), sizeof(value_t), first, dummy_space);
            return upp::span<value_t>{static_cast<value_t*>(first), _number_of_elements};
        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// sizes /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
        std::uint8_t* _ptr;
        std::size_t _initialized{0};
        std::size_t _number_of_dimensions;
        std::size_t _number_of_elements;
    private:
        friend std::shared_ptr<tensor_block<Type, Options>> make_tensor_block<>(std::size_t, std::size_t);
        friend struct tensor_block_deleter<Type, Options>;
        tensor_block() = default;
        ~tensor_block() = default;
        ///TODO custom new/delete + only allow one obj non placement
    };


    template<class Type, class Options>
    struct tensor_block_deleter
    {

        using block_t = tensor_block<Type, Options>;
        using value_t        = Type;
        void operator()(block_t* block)
        {
            auto ptr = block->_ptr;
            //https://en.cppreference.com/w/cpp/types/is_destructible#Notes
            //Storage occupied by trivially destructible objects may be reused
            //without calling the destructor.
            if constexpr(!std::is_trivially_destructible_v<value_t>)
            {
                auto elements = block->elements();
                for(
                    auto it = elements.begin();
                    it != elements.end() && block->_initialized > 0;
                    ++it, --block->_initialized
                )
                {
                    it->~Type();
                }
            }

            block->~block_t();
            delete[] ptr;
        }
    };


    template<class Type, class Options>
    std::shared_ptr<tensor_block<Type, Options>>
    make_tensor_block(std::size_t number_of_dimensions, std::size_t number_of_elements)
    {
        using block_t   = tensor_block<Type, Options>;
        using deleter_t = tensor_block_deleter<Type, Options>;
//        using value_t   = Type;
        //allocate memory
        static_assert(alignof(block_t) == alignof(std::size_t));
        std::size_t space =
                alignof(block_t) + sizeof(block_t    )                      +
                alignof(Type   ) + sizeof(Type       ) * number_of_elements +
                                   sizeof(std::size_t) * number_of_dimensions;
        std::uint8_t* ptr = new std::uint8_t[space];
        //align addr
        void* first = static_cast<void*>(ptr);
        void* block_raw = std::align(alignof(block_t), sizeof(block_t), first, space);
        if(block_raw == nullptr)
        {
            delete[] ptr;
            return nullptr;
        }
        block_t* block = static_cast<block_t*>(block_raw);
        //placement contruct
        new(block) block_t;
        //fill
        block->_number_of_dimensions = number_of_dimensions;
        block->_number_of_elements = number_of_elements;
        block->_ptr = ptr;
        return {block, deleter_t{}};
    }
}
