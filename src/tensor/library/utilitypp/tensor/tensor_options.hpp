#pragma once

namespace upp
{
    template<class...>
    struct tensor_options
    {
        static constexpr bool force_heap_allocation = false; ///TODO param
        static constexpr bool collumn_major         = false; ///TODO param, use it
        static constexpr bool element_wise_mul_div  = false; ///TODO param, use it
    };
}
