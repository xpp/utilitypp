#define BOOST_TEST_MODULE tensor

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/tensor.hpp>

class T
{
public:
    T()             {}
    T(T&&)          {}
    T(const T&)     {}
    virtual ~T()    {}
};

static_assert (!std::is_aggregate_v<T>);

#define test_sizes_1_2_3()                             \
    BOOST_CHECK_EQUAL(t.number_of_dimensions(), 3);     \
    BOOST_CHECK_EQUAL(t.size(), 6);                     \
    BOOST_CHECK_EQUAL(t.size_of_dimension(0), 1);       \
    BOOST_CHECK_EQUAL(t.size_of_dimension(1), 2);       \
    BOOST_CHECK_EQUAL(t.size_of_dimension(2), 3);       \
    BOOST_CHECK_EQUAL(t.size_in_bytes(), 6 * sizeof(T))

BOOST_AUTO_TEST_CASE( test_tensor_ctor_default )
{
    upp::tensor<T> t({1,2,3});
    test_sizes_1_2_3();
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_value )
{
    T val;
    upp::tensor<T> t({1,2,3}, val);
    test_sizes_1_2_3();
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen )
{
    auto gen = []() -> T
    {
        return {};
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen_vec )
{
    auto gen =  [](std::vector<std::size_t> v) -> T
    {
        BOOST_CHECK_EQUAL(v.size(), 3);
        return {};
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen_span )
{
    auto gen =  [](upp::span<std::size_t> v) -> T
    {
        BOOST_CHECK_EQUAL(v.size(), 3);
        return {};
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
}
