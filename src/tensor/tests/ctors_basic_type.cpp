#define BOOST_TEST_MODULE tensor

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/tensor.hpp>

using T = std::size_t;

#define test_sizes_1_2_3()                             \
    BOOST_CHECK_EQUAL(t.number_of_dimensions(), 3);     \
    BOOST_CHECK_EQUAL(t.size(), 6);                     \
    BOOST_CHECK_EQUAL(t.size_of_dimension(0), 1);       \
    BOOST_CHECK_EQUAL(t.size_of_dimension(1), 2);       \
    BOOST_CHECK_EQUAL(t.size_of_dimension(2), 3);       \
    BOOST_CHECK_EQUAL(t.size_in_bytes(), 6 * sizeof(T))

BOOST_AUTO_TEST_CASE( test_tensor_ctor_default )
{
    upp::tensor<T> t({1,2,3});
    test_sizes_1_2_3();
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_value )
{
    upp::tensor<T> t({1,2,3}, 1);
    test_sizes_1_2_3();
    for(auto e : t.elements())
    {
        BOOST_CHECK_EQUAL(e, 1);
    }
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen )
{
    std::size_t i = 0;
    auto gen = [&i]
    {
        return i++;
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
    for(std::size_t i = 0; i < t.size(); ++i)
    {
        BOOST_CHECK_EQUAL(t.elements().at(i), i);
    }
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen_vec )
{
    auto gen =  [](std::vector<std::size_t> v)
    {
        BOOST_CHECK_EQUAL(v.size(), 3);
        std::size_t i = 0;
        for(auto e : v)
        {
            i = 10*i + e;
        }
        return i;
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
    BOOST_CHECK_EQUAL(t.elements().at(0),  0);
    BOOST_CHECK_EQUAL(t.elements().at(1),  1);
    BOOST_CHECK_EQUAL(t.elements().at(2),  2);
    BOOST_CHECK_EQUAL(t.elements().at(3), 10);
    BOOST_CHECK_EQUAL(t.elements().at(4), 11);
    BOOST_CHECK_EQUAL(t.elements().at(5), 12);
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen_span )
{
    auto gen =  [](upp::span<std::size_t> v)
    {
        BOOST_CHECK_EQUAL(v.size(), 3);
        std::size_t i = 0;
        for(auto e : v)
        {
            i = 10*i + e;
        }
        return i;
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
    BOOST_CHECK_EQUAL(t.elements().at(0),  0);
    BOOST_CHECK_EQUAL(t.elements().at(1),  1);
    BOOST_CHECK_EQUAL(t.elements().at(2),  2);
    BOOST_CHECK_EQUAL(t.elements().at(3), 10);
    BOOST_CHECK_EQUAL(t.elements().at(4), 11);
    BOOST_CHECK_EQUAL(t.elements().at(5), 12);
}
