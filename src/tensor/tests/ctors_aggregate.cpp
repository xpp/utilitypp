#define BOOST_TEST_MODULE tensor

#include <array>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/tensor.hpp>

using T = std::array<std::size_t, 3>;

#define test_sizes_1_2_3()                             \
    BOOST_CHECK_EQUAL(t.number_of_dimensions(), 3);     \
    BOOST_CHECK_EQUAL(t.size(), 6);                     \
    BOOST_CHECK_EQUAL(t.size_of_dimension(0), 1);       \
    BOOST_CHECK_EQUAL(t.size_of_dimension(1), 2);       \
    BOOST_CHECK_EQUAL(t.size_of_dimension(2), 3);       \
    BOOST_CHECK_EQUAL(t.size_in_bytes(), 6 * sizeof(T))

BOOST_AUTO_TEST_CASE( test_tensor_ctor_default )
{
    upp::tensor<T> t({1,2,3});
    test_sizes_1_2_3();
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_value )
{
    T val = {1,2,3};
    upp::tensor<T> t({1,2,3}, val);
    test_sizes_1_2_3();
    for(auto e : t.elements())
    {
        BOOST_CHECK_EQUAL(e.at(0), val.at(0));
        BOOST_CHECK_EQUAL(e.at(1), val.at(1));
        BOOST_CHECK_EQUAL(e.at(2), val.at(2));
    }
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen )
{
    std::size_t i = 0;
    auto gen = [&i]() -> T
    {
        return {i++, 0, 0};
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
    for(std::size_t i = 0; i < t.size(); ++i)
    {
        BOOST_CHECK_EQUAL(t.elements().at(i).at(0), i);
    }
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen_vec )
{
    auto gen =  [](std::vector<std::size_t> v) -> T
    {
        BOOST_CHECK_EQUAL(v.size(), 3);
        return {v.at(0), v.at(1), v.at(2)};
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
    BOOST_CHECK_EQUAL(t.elements().at(0).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(0).at(1), 0);
    BOOST_CHECK_EQUAL(t.elements().at(0).at(2), 0);

    BOOST_CHECK_EQUAL(t.elements().at(1).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(1).at(1), 0);
    BOOST_CHECK_EQUAL(t.elements().at(1).at(2), 1);

    BOOST_CHECK_EQUAL(t.elements().at(2).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(2).at(1), 0);
    BOOST_CHECK_EQUAL(t.elements().at(2).at(2), 2);

    BOOST_CHECK_EQUAL(t.elements().at(3).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(3).at(1), 1);
    BOOST_CHECK_EQUAL(t.elements().at(3).at(2), 0);

    BOOST_CHECK_EQUAL(t.elements().at(4).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(4).at(1), 1);
    BOOST_CHECK_EQUAL(t.elements().at(4).at(2), 1);

    BOOST_CHECK_EQUAL(t.elements().at(5).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(5).at(1), 1);
    BOOST_CHECK_EQUAL(t.elements().at(5).at(2), 2);
}

BOOST_AUTO_TEST_CASE( test_tensor_ctor_gen_span )
{
    auto gen =  [](upp::span<std::size_t> v) -> T
    {
        BOOST_CHECK_EQUAL(v.size(), 3);
        return {v.at(0), v.at(1), v.at(2)};
    };
    upp::tensor<T> t({1,2,3}, upp::tensor_construct_with_generator, gen);
    test_sizes_1_2_3();
    BOOST_CHECK_EQUAL(t.elements().at(0).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(0).at(1), 0);
    BOOST_CHECK_EQUAL(t.elements().at(0).at(2), 0);

    BOOST_CHECK_EQUAL(t.elements().at(1).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(1).at(1), 0);
    BOOST_CHECK_EQUAL(t.elements().at(1).at(2), 1);

    BOOST_CHECK_EQUAL(t.elements().at(2).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(2).at(1), 0);
    BOOST_CHECK_EQUAL(t.elements().at(2).at(2), 2);

    BOOST_CHECK_EQUAL(t.elements().at(3).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(3).at(1), 1);
    BOOST_CHECK_EQUAL(t.elements().at(3).at(2), 0);

    BOOST_CHECK_EQUAL(t.elements().at(4).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(4).at(1), 1);
    BOOST_CHECK_EQUAL(t.elements().at(4).at(2), 1);

    BOOST_CHECK_EQUAL(t.elements().at(5).at(0), 0);
    BOOST_CHECK_EQUAL(t.elements().at(5).at(1), 1);
    BOOST_CHECK_EQUAL(t.elements().at(5).at(2), 2);
}
