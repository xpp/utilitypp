#define BOOST_TEST_MODULE type_traits

#include <vector>
#include <deque>
#include <set>
#include <map>
#include <string>
#include <string_view>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/type_traits/range/is_sized_range.hpp>

struct foo {};

BOOST_AUTO_TEST_CASE( test_is_sized_range_of_type_v )
{
    //true cases
    static_assert ( upp::is_sized_range_of_type_v<std::vector<int>  , int                      >);
    static_assert ( upp::is_sized_range_of_type_v<std::deque <int>  , int                      >);
    static_assert ( upp::is_sized_range_of_type_v<std::set   <int>  , int                      >);
    static_assert ( upp::is_sized_range_of_type_v<std::map<int, int>, std::pair<const int, int>>);
    static_assert ( upp::is_sized_range_of_type_v<std::string       , char                     >);
    static_assert ( upp::is_sized_range_of_type_v<std::string_view  , char                     >);

    static_assert (!upp::is_sized_range_of_type_v<std::vector<int>  , void>);
    static_assert (!upp::is_sized_range_of_type_v<std::deque <int>  , void>);
    static_assert (!upp::is_sized_range_of_type_v<std::set   <int>  , void>);
    static_assert (!upp::is_sized_range_of_type_v<std::map<int, int>, void>);
    static_assert (!upp::is_sized_range_of_type_v<std::string       , void>);
    static_assert (!upp::is_sized_range_of_type_v<std::string_view  , void>);

    static_assert (!upp::is_sized_range_of_type_v<std::int64_t, int>);
    static_assert (!upp::is_sized_range_of_type_v<foo         , int>);
}
