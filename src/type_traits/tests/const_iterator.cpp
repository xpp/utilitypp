#define BOOST_TEST_MODULE type_traits

#include <vector>
#include <deque>
#include <set>
#include <map>
#include <string>
#include <string_view>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/type_traits/const_iterator.hpp>

struct foo {};

BOOST_AUTO_TEST_CASE( test_has_const_iterator_v )
{
    //true cases
    static_assert ( upp::has_const_iterator_v<std::vector<int>>);
    static_assert ( upp::has_const_iterator_v<std::deque<int>>);
    static_assert ( upp::has_const_iterator_v<std::set<int>>);
    static_assert ( upp::has_const_iterator_v<std::map<int, int>>);
    static_assert ( upp::has_const_iterator_v<std::string>);
    static_assert ( upp::has_const_iterator_v<std::string_view>);

    static_assert (!upp::has_const_iterator_v<std::int64_t>);
    static_assert (!upp::has_const_iterator_v<foo>);

}
