#define BOOST_TEST_MODULE type_traits

#include <vector>
#include <deque>
#include <set>
#include <map>
#include <string>
#include <string_view>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/type_traits/range/is_range.hpp>

struct foo {};

BOOST_AUTO_TEST_CASE( test_is_range_v )
{
    //true cases
    static_assert ( upp::is_range_v<std::vector<int>>);
    static_assert ( upp::is_range_v<std::deque<int>>);
    static_assert ( upp::is_range_v<std::set<int>>);
    static_assert ( upp::is_range_v<std::map<int, int>>);
    static_assert ( upp::is_range_v<std::string>);
    static_assert ( upp::is_range_v<std::string_view>);

    static_assert (!upp::is_range_v<std::int64_t>);
    static_assert (!upp::is_range_v<foo>);

}
