#pragma once

#include "../size.hpp"
#include "is_range.hpp"

namespace upp
{
    template<class T>
    static constexpr bool is_sized_range_v = is_range_v<T> && has_size_v<T>;

    template<class RT, class T>
    static constexpr bool is_sized_range_of_type_v =
        is_sized_range_v<RT> &&
        std::is_same_v<std::experimental::detected_or_t<void, value_type, RT>, T>;
}
