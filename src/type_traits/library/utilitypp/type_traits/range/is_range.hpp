#pragma once

#include <type_traits>

#include "../begin.hpp"
#include "../end.hpp"
#include "../value_type.hpp"
#include "../iterator.hpp"

namespace upp
{
    template<class T>
    static constexpr bool is_range_v =
        has_begin_v<T> && has_end_v<T> &&
        has_iterator_v<T> && has_value_type_v<T>;

    template<class RT, class T>
    static constexpr bool is_range_of_type_v =
        is_range_v<RT> &&
        std::is_same_v<std::experimental::detected_or_t<void, value_type, RT>, T>;
}
