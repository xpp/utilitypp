#pragma once

#include <type_traits>

namespace upp
{
    template <class T>
    using remove_cvr = std::remove_cv<std::remove_reference_t<T>>;
    template <class T>
    using remove_cvr_t = typename remove_cvr<T>::type;
}
