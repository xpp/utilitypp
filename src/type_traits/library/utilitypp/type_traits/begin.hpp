#pragma once

#include <experimental/type_traits>

namespace upp::detector
{
    template<class T>
    using begin_t = decltype(std::declval<T&>().begin());
}

namespace upp
{
    template <class T>
    static constexpr bool has_begin_v =
        std::experimental::is_detected_v<detector::begin_t, T>;
}

