#pragma once

#include <experimental/type_traits>

namespace upp::detector
{
    template<class T>
    using end_t = decltype(std::declval<T&>().end());
}

namespace upp
{
    template <class T>
    static constexpr bool has_end_v =
        std::experimental::is_detected_v<detector::end_t, T>;
}

