#pragma once

#include <experimental/type_traits>

namespace upp::detector
{
    template<class T>
    using size_t = decltype(std::declval<T&>().size());
}

namespace upp
{
    template <class T>
    static constexpr bool has_size_v =
        std::experimental::is_detected_v<detector::size_t, T>;
}

