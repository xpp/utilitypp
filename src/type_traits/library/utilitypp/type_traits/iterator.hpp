#pragma once

#include <experimental/type_traits>

namespace upp::detector
{
    template<class T>
    using iterator_t = typename T::iterator;
}

namespace upp
{
    template <class T>
    using iterator_t = typename T::iterator;

    template <class T>
    static constexpr bool has_iterator_v =
        std::experimental::is_detected_v<detector::iterator_t, T>;
}

