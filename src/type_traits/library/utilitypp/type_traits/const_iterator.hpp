#pragma once

#include <experimental/type_traits>

namespace upp::detector
{
    template<class T>
    using const_iterator_t = typename T::const_iterator;
}

namespace upp
{
    template <class T>
    using const_iterator_t = typename T::const_iterator;

    template <class T>
    static constexpr bool has_const_iterator_v =
        std::experimental::is_detected_v<detector::const_iterator_t, T>;
}

