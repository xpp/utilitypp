#pragma once

#include <type_traits>

namespace upp::detail
{
    template <class T, class = void>
    struct extract_value_type_has_member_value_type : std::false_type {};

    template <class T>
    struct extract_value_type_has_member_value_type<T, std::void_t<typename T::value_type>> : std::true_type {};

    template <class T, class = void>
    struct extract_value_type
    {
        static constexpr bool value = false;
    };

    template <class T>
    struct extract_value_type<T,  std::enable_if_t<extract_value_type_has_member_value_type<T>::value>>
    {
        static constexpr bool value = true;
        using value_t = typename T::value_type;
    };

    template <class T>
    struct extract_value_type<T, std::void_t<std::enable_if_t<!extract_value_type_has_member_value_type<T>::value>, typename T::value_t>>
    {
        static constexpr bool value = true;
        using value_t = typename T::value_t;
    };
}

namespace upp
{
    template <class T>
    using value_type = typename detail::extract_value_type<T>::value_t;

    template <class T>
    static constexpr bool has_value_type_v =
        detail::extract_value_type<T>::value;
}

