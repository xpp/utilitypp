#pragma once

// This file is generated!

#include "type_traits/begin.hpp"
#include "type_traits/const_iterator.hpp"
#include "type_traits/end.hpp"
#include "type_traits/iterator.hpp"
#include "type_traits/range.hpp"
#include "type_traits/remove_cvr.hpp"
#include "type_traits/size.hpp"
#include "type_traits/value_type.hpp"
