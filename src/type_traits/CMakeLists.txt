################################################################################
#################################### library ###################################
################################################################################
scm_add_header_library(
    SOURCE_DIRECTORY_PREFIX library
    TARGET                  type_traits
    GENERATE_SUBDIR_HEADERS utilitypp
    FILES                   utilitypp/type_traits/range/is_range.hpp
                            utilitypp/type_traits/range/is_sized_range.hpp

                            utilitypp/type_traits/begin.hpp
                            utilitypp/type_traits/const_iterator.hpp
                            utilitypp/type_traits/end.hpp
                            utilitypp/type_traits/iterator.hpp
                            utilitypp/type_traits/range.hpp
                            utilitypp/type_traits/remove_cvr.hpp
                            utilitypp/type_traits/size.hpp
                            utilitypp/type_traits/value_type.hpp
)
################################################################################
#################################### tests #####################################
################################################################################
upp_add_test(type_traits begin)
upp_add_test(type_traits const_iterator)
upp_add_test(type_traits end)
upp_add_test(type_traits is_range)
upp_add_test(type_traits is_range_of_type)
upp_add_test(type_traits is_sized_range)
upp_add_test(type_traits is_sized_range_of_type)
upp_add_test(type_traits iterator)
upp_add_test(type_traits size)
upp_add_test(type_traits value_type)
################################################################################
################################# benchmarking #################################
################################################################################
if(NOT benchmark_FOUND)
    set_directory_properties(PROPERTIES EXCLUDE_FROM_ALL ON)
endif()
