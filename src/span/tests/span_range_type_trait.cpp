#define BOOST_TEST_MODULE span

#include <array>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/span.hpp>
#include <utilitypp/type_traits/range/is_range.hpp>
#include <utilitypp/type_traits/range/is_sized_range.hpp>

BOOST_AUTO_TEST_CASE( test_span_range_type_trait )
{
    static_assert (upp::has_begin_v             <upp::span<int>     >);
    static_assert (upp::has_end_v               <upp::span<int>     >);
    static_assert (upp::has_iterator_v          <upp::span<int>     >);
    static_assert (upp::has_value_type_v        <upp::span<int>     >);

    static_assert (upp::is_range_v              <upp::span<int>     >);
    static_assert (upp::is_sized_range_v        <upp::span<int>     >);
    static_assert (upp::is_range_of_type_v      <upp::span<int>, int>);
    static_assert (upp::is_sized_range_of_type_v<upp::span<int>, int>);
}
