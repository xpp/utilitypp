#pragma once

#include <stdexcept>
#include <memory>

#include <utilitypp/exception/throw_if.hpp>

namespace upp
{
    template<class T>
    class span
    {
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// typedefs ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        using value_t     = T;
        using iterator_t = value_t*;
        //convention of std::vector
        using value_type = value_t;
        using iterator   = iterator_t;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// ctor //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        constexpr span() = default;
        constexpr span(span&&) = default;
        constexpr span(const span&) = default;

        template<class Container>
        constexpr span(Container& container):
            span(container.begin(), container.end())
        {}

        template<class It>
        constexpr span(It first, It last):
            span(&(*first), std::distance(first, last))
        {}

        constexpr span(value_t* first, std::size_t size) :
            _first{first},
            _size{size}
        {
            upp_throw_if(std::invalid_argument, first == nullptr && size != 0)
                << "if first is null, the size has to be null as well";
        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// assign /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        constexpr span& operator=(span&&) = default;
        constexpr span& operator=(const span&) = default;
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// querry /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        constexpr std::size_t size() const
        {
            return _size;
        }
        constexpr bool empty() const
        {
            return _size == 0;
        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // ////////////////////////////////////// iterator ////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        constexpr value_t* begin()
        {
            return _first;
        }
        constexpr const value_t* begin() const
        {
            return _first;
        }
        constexpr value_t* end()
        {
            return begin() + size();
        }
        constexpr const value_t* end() const
        {
            return begin() + size();
        }
        constexpr const value_t* cbegin() const
        {
            return _first;
        }
        constexpr const value_t* cend() const
        {
            return begin() + size();
        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////////////// access /////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    public:
        constexpr value_t& at(std::size_t i)
        {
            upp_throw_if_not_less(std::invalid_argument, i, size());
            return _first[i];
        }
        constexpr const value_t& at(std::size_t i) const
        {
            upp_throw_if_not_less(std::invalid_argument, i, size());
            return _first[i];
        }
        // ////////////////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////////////// data //////////////////////////////////////// //
        // ////////////////////////////////////////////////////////////////////////////////////// //
    private:
        value_t* _first{nullptr};
        std::size_t _size{0};
    };

    template<class Container>
    span(Container&) -> span<typename Container::value_type>;
    template<class Container>
    span(const Container&) -> span<const typename Container::value_type>;

    template<class It>
    span(It, It) -> span<typename std::iterator_traits<It>::value_type>;
}
