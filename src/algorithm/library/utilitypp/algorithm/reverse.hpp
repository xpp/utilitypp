#pragma once

namespace upp
{
    template<class T>
    T reversed(const T& v)
    {
        return T(v.rbegin(), v.redn());
    }
}
