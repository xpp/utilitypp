#define BOOST_TEST_MODULE iterator
#include <array>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/iterator/zip_iterator.hpp>

BOOST_AUTO_TEST_CASE(zip_iterator)
{
    std::array<int, 10> ar{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::size_t cnt = 0;
    for (
        auto it = upp::zip_iterator{ar.begin(), ar.rbegin()};
        it != upp::zip_iterator{ar.end(), ar.rend()};
        ++it, ++cnt
    )
    {
        const auto [l, r] = *it;
        BOOST_REQUIRE_LE(cnt, ar.size());
        BOOST_CHECK_EQUAL(l, cnt);
        BOOST_CHECK_EQUAL(r, ar.at(ar.size() - 1 - cnt));
    }
}
