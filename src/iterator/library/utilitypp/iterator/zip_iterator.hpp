#pragma once

#include <tuple>
#include <utility>

#include <boost/iterator/iterator_facade.hpp>

namespace upp::detail
{
    template<class Ituple, class Rtuple, class Idx>
    class zip_iterator_base;

    template<class Ituple, class Rtuple, std::size_t...Idxs>
    class zip_iterator_base < Ituple, Rtuple, std::index_sequence<Idxs...>> :
                public boost::iterator_facade <
                zip_iterator_base<Ituple, Rtuple, std::index_sequence<Idxs...>>,
                Rtuple,
                boost::forward_traversal_tag,
                Rtuple
                >
    {
    public:
        using iterator_tuple_t = Ituple;
        using reference = Rtuple;
        using difference_type = std::ptrdiff_t;
    private:
        iterator_tuple_t _iters;
    public:
        zip_iterator_base(zip_iterator_base&&) = default;
        zip_iterator_base(const zip_iterator_base&) = default;

        zip_iterator_base& operator=(zip_iterator_base&&) = default;
        zip_iterator_base& operator=(const zip_iterator_base&) = default;

        zip_iterator_base(iterator_tuple_t its) : _iters{std::move(its)} {}
    private:
        friend class boost::iterator_core_access;

        // Implementation of Iterator Operations
        // =====================================
        reference dereference() const
        {
            return std::forward_as_tuple(*std::get<Idxs>(_iters)...);
        }

        bool equal(const zip_iterator_base& other) const
        {
            return _iters == other._iters;
        }

        // Advancing a zip iterator means to advance all iterators in the
        // iterator tuple.
        void advance(difference_type n)
        {
            ((std::get<Idxs>(_iters) += n), ...);
        }
        // Incrementing a zip iterator means to increment all iterators in
        // the iterator tuple.
        void increment()
        {
            (++std::get<Idxs>(_iters), ...);
        }

        // Decrementing a zip iterator means to decrement all iterators in
        // the iterator tuple.
        void decrement()
        {
            (--std::get<Idxs>(_iters), ...);
        }

        //    // Distance is calculated using the first iterator in the tuple.
        //    template<typename Otheriterator_tuple_t>
        //    typename super_t::difference_type distance_to(
        //        const zip_iterator<Otheriterator_tuple_t>& other
        //    ) const
        //    {
        //        return fusion::at_c<0>(other.get_iterator_tuple()) -
        //               fusion::at_c<0>(this->get_iterator_tuple());
        //    }


    };
}

namespace upp
{
    template<class It0, class...Its>
    class zip_iterator :
        public detail::zip_iterator_base
        <
        std::tuple<It0, Its...>,
        std::tuple
        <
        typename std::iterator_traits<It0>::reference&&,
        typename std::iterator_traits<Its>::reference&& ...
        >,
        std::make_index_sequence < 1 + sizeof...(Its) >
        >
    {
    public:
        using base = detail::zip_iterator_base
                     <
                     std::tuple<It0, Its...>,
                     std::tuple
                     <
                     typename std::iterator_traits<It0>::reference &&,
                     typename std::iterator_traits<Its>::reference && ...
                     >,
                     std::make_index_sequence < 1 + sizeof...(Its) >
                     >;

        zip_iterator(It0 it0, Its...its) :
            base({std::move(it0), std::move(its)...})
        {}
        using base::base;
        using base::operator=;
    };
}
