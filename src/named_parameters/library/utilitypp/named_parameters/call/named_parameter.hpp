#pragma once

#include <type_traits>

#include "../common/fixed_string_id_helper.hpp"

namespace upp::named_parameter
{
    template <class Id, class T>
    struct bound_named_parameter
    {
        using id_t = Id;
        using value_t = T;

        const value_t value;

        constexpr const T& operator()() const
        {
            return value;
        }
    };
}

namespace upp::named_parameter
{
    template <detail::_id::fixed_string Str, class T>
    struct unbound_named_parameter
    {
        _upp_detail_named_parameter_fixed_str_to_id_t(Str, id_t);
        using value_t = T;

        template<class T2>
        constexpr bound_named_parameter<id_t, value_t> operator =(T2&& t) const
        {
            return {std::forward<T2>(t)};
        }
    };
}

namespace upp::named_parameter
{
//    template <detail::_id::fixed_string Str, class T>
//    static constexpr unbound_named_parameter<Str, T> named_parameter{};

    template<class T, std::size_t N>
    inline consteval auto named_parameter(const char (&str)[N])
    {
        return unbound_named_parameter<str, T>{};
    }

}
