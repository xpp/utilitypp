#pragma once

#include <type_traits>

#include <boost/mp11/algorithm.hpp>

#include <utilitypp/template_traits/is_template_instance_of.hpp>

#include "named_parameter.hpp"


/*

check if apply valid
 - check if def valid
                                    - check if no non named after first named
 - check if no double given named
 -cmp check if no named with same name (or named for one non named)
 -cmp check if all params defined if they have no default
 -cmp check if non named match types
 -cmp check if named match types
 -cmp check if all given named have definition
*/
namespace upp::named_parameter::detail::_is_invokeable
{
    template<bool LastParamWasNamed, class...>
    struct check_list_except_unique : std::true_type
    {
    protected:
        template<class...Pres>
        using prep_un_plist = std::tuple<Pres...>;

        template<class...Pres>
        using prep_n_plist = std::tuple<Pres...>;

    public:
        using unnamed_parameter_list = prep_un_plist<>;
        using named_parameter_list = prep_n_plist<>;
    };

    template<bool LastParamWasNamed, class T0, class...Ts>
    struct check_list_except_unique<LastParamWasNamed, T0, Ts...>
    {
    protected:
        ///TODO static constexpr bool param_is_unbound_named =
        static constexpr bool is_n_param = upp::is_template_instance_of_v<bound_named_parameter, T0>;

        using next = check_list_except_unique<is_n_param, Ts...>;

        template<class...Pres>
        using base_un_plist = typename next::template prep_un_plist<Pres...>;
        template<class...Pres>
        using extended_un_plist = typename next::template prep_un_plist<T0, Pres...>;
        template<class...Pres>
        using prep_un_plist = std::conditional_t<is_n_param, base_un_plist<Pres...>, extended_un_plist<Pres...>>;

        template<class...Pres>
        using base_n_plist = typename next::template prep_n_plist<Pres...>;
        template<class...Pres>
        using extended_n_plist = typename next::template prep_n_plist<T0, Pres...>;
        template<class...Pres>
        using prep_n_plist = std::conditional_t <is_n_param, extended_n_plist<Pres...>, base_n_plist<Pres...>>;
    public:
        static constexpr bool value =
            (!LastParamWasNamed || is_n_param) &&
            next::value;

        using unnamed_parameter_list = prep_un_plist<>;
        using named_parameter_list = prep_n_plist<>;
    };
}
