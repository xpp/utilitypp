#pragma once

#include "../common/fixed_string_id_helper.hpp"

namespace upp::named_parameter
{
    template <detail::_id::fixed_string Str>
    struct unbound_parameter
    {
        _upp_detail_named_parameter_fixed_str_to_id_t(Str, id_t);

        template<class T>
        constexpr bound_parameter<id_t, T> operator =(T&& t)&&
        {
            return {std::forward<T>(t)};
        }
    };
}
