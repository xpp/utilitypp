#pragma once

#include "../common/fixed_string_id_helper.hpp"

namespace upp::named_parameter
{
    template <detail::_id::fixed_string Str, class T>
    struct parameter_definition
    {
        _upp_detail_named_parameter_fixed_str_to_id_t(Str, id_t);
        using value_t = T;
    };
}
