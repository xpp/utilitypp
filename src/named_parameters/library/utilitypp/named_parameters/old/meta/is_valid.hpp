#pragma once

//is_named_parameter
namespace upp::named_parameter
{
    template<class>
    struct is_named_parameter : std::false_type {};
}

