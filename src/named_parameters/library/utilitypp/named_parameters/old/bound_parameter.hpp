#pragma once

#include "../common/id.hpp"

namespace upp::named_parameter
{
    template<class, class>
    struct bound_parameter;

    template<class T, char...Cs>
    struct bound_parameter<id<Cs...>, T>
    {
        using value_t = T;
        using id_t = id<Cs...>;

        value_t value;
        value_t&& forward()
        {
            return std::forward<T>(value);
        }
    };
}

