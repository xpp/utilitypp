#pragma once

#include "forward.hpp"

namespace upp::named_parameter::detail::_parameter_set
{
    template <class CRTP, class...Ps>
    struct find_trait
    {
    private:
        template<class Void, class Id, std::size_t Idx, class...Ts>
        struct find_helper
        {
            static constexpr std::size_t index = sizeof...(Ts);
        };
        template<class Id, std::size_t Idx, class T, class...Ts>
        struct find_helper <
            std::enable_if_t<std::is_same_v<Id, typename T::id_t>>,
                    Id, Idx, T, Ts...
                    >
        {
            static constexpr std::size_t index = Idx;
        };

        template<class Id, std::size_t Idx, class T, class...Ts>
        struct find_helper <
            std::enable_if_t < !std::is_same_v<Id, typename T::id_t >>,
                    Id, Idx, T, Ts...
                    > : find_helper < void, Id, Idx + 1, Ts... >
        {};
    public:
        template<class Id>
        static constexpr std::size_t find_index = find_helper<void, Id, 0, Ps...>::index;

        template<class Id>
        static constexpr bool has = find_index<Id> < sizeof...(Ps);

        template<class Id>
        using find = CRTP::template at<find_index<Id>>;

    };
}
