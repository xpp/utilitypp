#pragma once

#include "forward.hpp"

namespace upp::named_parameter::detail::_parameter_set
{
    template <class CRTP, class...Ps>
    struct access_trait
    {
        template<std::size_t I>
        using at = decltype(std::get<I>(std::declval<std::tuple<Ps...>>()));
    };
}
