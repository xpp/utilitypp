#pragma once

#include "forward.hpp"

namespace upp::named_parameter::detail::_parameter_set
{
    template <class CRTP, class...Ps>
    struct is_uniqe_trait
    {
    private:
        template<class...>
        struct _is_unique_helper : std::true_type
        {};

        template<class A, class B, class...Tail>
        struct _is_unique_helper<A, B, Tail...> : std::conditional_t
            <
            std::is_same_v<typename A::id_t, typename B::id_t>,
            std::false_type,
            _is_unique_helper<B, Tail...>
            >
        {};
    public:
        static constexpr bool is_unique = CRTP::template sort<>::template _is_unique_helper<Ps...>::value;
    };
}
