#pragma once

#include <type_traits>

#include <boost/mp11/algorithm.hpp>

//id
namespace upp::named_parameter
{
    template<char...Cs>
    struct id
    {
        static constexpr std::size_t size = sizeof...(Cs);
        static constexpr char c_str[] = {Cs..., '\0'};
    };
}

//id related type_traits : is_id
namespace upp::named_parameter
{
    template<class>
    struct is_id : std::false_type {};

    template<char...Cs>
    struct is_id<id<Cs...>> : std::true_type {};

    template<class T>
    static constexpr bool is_id_v = is_id<T>::value;
}

//id related type_traits : has_id
namespace upp::named_parameter
{
    template<class T, class = void>
    struct has_id_t : std::false_type {};

    template<class T>
    struct has_id_t<T, std::void_t<typename T::id_t>> :
                std::bool_constant<is_id_v<typename T::id_t>>
    {};

    template<class T>
    static constexpr bool has_id_t_v = has_id_t<T>::value;
}

//id related meta functrs
namespace upp::named_parameter::detail::_list_has_unique_ids
{
    template<class List>
    struct check_unique : std::false_type {};

    template<template<class...> class Temp, class...Ts>
    requires(has_id_t_v<Ts>&& ...)
    struct check_unique<Temp<Ts...>>
    {
        template<class L, class R> requires is_id_v<L>&& is_id_v<R>
        struct id_less
        {
        private:
            static consteval bool check_lt_functor()
            {
                for (int i = 0; i < std::min(L::size, R::size); ++i)
                {
                    if (L::c_str[i] < R::C_str[i])
                    {
                        return true;
                    }
                    else if (L::c_str[i] > R::C_str[i])
                    {
                        return false;
                    }
                }
                //all chars were the same -> if L is shorter, then L was less
                return L::size < R::size;
            }
        public:
            static constexpr bool value = check_lt_functor();
        };

        using ids = std::tuple<typename Ts::id_t...>;
        using ids_sorted = boost::mp11::mp_sort<ids, id_less>;
        using ids_unique = boost::mp11::mp_unique<ids_sorted>;

        static constexpr auto len_ids = boost::mp11::mp_size<ids>::value;
        static constexpr auto len_ids_unique = boost::mp11::mp_size<ids_unique>::value;

        static constexpr bool value = len_ids == len_ids_unique;
    };
}

namespace upp::named_parameter
{
    template<class List>
    struct list_has_unique_ids : std::bool_constant<detail::_list_has_unique_ids::check_unique<List>::value>
    {};

    template<class List>
    static constexpr bool list_has_unique_ids_v = list_has_unique_ids<List>::value;
}
