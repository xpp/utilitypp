#pragma once

#include "id.hpp"

#define _upp_detail_named_parameter_fixed_str_to_id_t(Str, tname)                               \
    template<class> struct _upp_detail_named_parameter_fixed_str_to_id_t_helper;                \
    template<std::size_t...Is>                                                                  \
    struct _upp_detail_named_parameter_fixed_str_to_id_t_helper<std::index_sequence<Is...>>     \
    {                                                                                           \
        using type = id<Str.c_str[Is]...>;                                                      \
    };                                                                                          \
    using tname = typename _upp_detail_named_parameter_fixed_str_to_id_t_helper                 \
                  <                                                                             \
                  std::make_index_sequence<Str.size - 1>                                        \
                  >::type

namespace upp::named_parameter::detail::_id
{
    template<auto N>
    struct fixed_string
    {
        consteval fixed_string(const char (&str)[N])
        {
            for (std::size_t i = 0; i != N; ++i)
            {
                c_str[i] = str[i];
            }
        }
        template<auto N2> requires (N == N2)
        consteval fixed_string(const fixed_string<N2>& str)
        {
            for (std::size_t i = 0; i != N; ++i)
            {
                c_str[i] = str.c_str[i];
            }
        }
        template<auto N2> requires (N == N2)
        consteval fixed_string(fixed_string<N2>&& str)
        {
            for (std::size_t i = 0; i != N; ++i)
            {
                c_str[i] = str.c_str[i];
            }
        }
        char c_str[N];
        static constexpr std::size_t size = N;
    };

    template<auto N> fixed_string(const char (&)[N]) -> fixed_string<N>;
    template<auto N> fixed_string(fixed_string<N>&&) -> fixed_string<N>;
    template<auto N> fixed_string(const fixed_string<N>&) -> fixed_string<N>;
}
