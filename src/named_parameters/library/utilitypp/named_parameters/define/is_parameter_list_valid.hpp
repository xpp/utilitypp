#pragma once

#include <type_traits>

#include <boost/mp11/algorithm.hpp>

#include <utilitypp/template_traits/is_template_instance_of.hpp>

#include "../common/id.hpp"

#include "parameter_definition.hpp"


/*
check if def valid
 - each param unique name
 - no param type void
 - each param my wrapper type
 - no non default after first default
*/
namespace upp::named_parameter::definition::detail::_is_parameter_list_valid
{
    template<class T>
    static constexpr bool check_instance_of_v =
        upp::is_template_instance_of_v<parameter, T> ||
        upp::is_template_instance_of_v<required_parameter, T> ||
        upp::is_template_instance_of_v<default_parameter, T>;

    template<bool LastHadDefault, class...Ts>
    struct check_except_unique : std::true_type {};

    template<bool LastHadDefault, class T0, class...Ts>
    struct check_except_unique<LastHadDefault, T0, Ts...>
    {
        static constexpr bool value =
            check_instance_of_v<T0>&&
            !std::is_same_v<typename T0::value_t, void>&&
            (!LastHadDefault || T0::has_default) &&
            check_except_unique<T0::has_default, Ts...>::value;
    };
}

namespace upp::named_parameter::definition
{
    template<class List>
    struct is_parameter_list_valid : std::false_type {};

    template<template<class...> class Temp, class...Ts>
    struct is_parameter_list_valid<Temp<Ts...>>
     : std::bool_constant <
       detail::_is_parameter_list_valid::check_except_unique<false, Ts...>::value &&
       list_has_unique_ids_v<Temp<Ts...>>
       > {};

    template<class List>
    static constexpr bool is_parameter_list_valid_v = is_parameter_list_valid<List>::value;
}
