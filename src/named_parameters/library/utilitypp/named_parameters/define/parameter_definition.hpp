#pragma once

#include <type_traits>

#include "../common/fixed_string_id_helper.hpp"

namespace upp::named_parameter::definition
{
    template <detail::_id::fixed_string Str, class T, bool HasDefault>
    struct parameter
    {
        _upp_detail_named_parameter_fixed_str_to_id_t(Str, id_t);
        using value_t = T;
        static constexpr bool has_default = HasDefault;
    };

    template <detail::_id::fixed_string Str, class T>
    struct required_parameter
    {
        _upp_detail_named_parameter_fixed_str_to_id_t(Str, id_t);
        using value_t = T;
        static constexpr bool has_default = false;
    };

    template <detail::_id::fixed_string Str, class T>
    struct default_parameter
    {
        _upp_detail_named_parameter_fixed_str_to_id_t(Str, id_t);
        using value_t = T;
        static constexpr bool has_default = true;
    };

    ///TODO something to deal with template params!
}
