#pragma once

#include "is_parameter_list_valid.hpp"

namespace upp::named_parameter::definition
{
    template<class...Ts>
    struct parameter_list
    {
        using parameter_list_t = parameter_list<Ts...>;
        static_assert(
            is_parameter_list_valid_v<parameter_list_t>,
            "The parameter list is invalid! This could have several reasons: "
            "1) Multiple parameters use the same name. "
            "2) Some parameter uses the type void. "
            "3) Some definition in the list is not one of the valid definition types. "
            "4) Some parameter with no default value is after an parameter with a default."
        );
    };
}
