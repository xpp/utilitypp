#define BOOST_TEST_MODULE named_parameters

#include <string>

#include <boost/test/included/unit_test.hpp>

//#include <utilitypp/named_parameters.hpp>

//std::string f(char q, char w, char e = 'e')
//{
//    return std::string{} + q + w + e;
//}

//std::size_t f(std::string q, std::string w, std::string e = "e")
//{
//    return q.size() + w.size() + e.size();
//}
///*
//     using d =  dispatchor<s1,s2,s3,s4>; // checks
//     if constexpr (d::selected == 1)
//     {
//        static_assert(d::params == 3);
//         return f(
//            d::get<0>(ts...), // might get from list beginning, might get from named
//            d::get<1>(ts...),
//            d::has<2>(ts...) ? d::get<2>(ts...) : defaultvalue
//         );
//     }
//     [...]
//     else
//     {
//         //print some msg
//     }
//*/
//template <class...Ts>
//requires(upp::named_parameter::is_named_parameter_v<Ts> || ...)
//auto&& f(Ts&& ...ts)
//{
//    namespace np = upp::named_parameter;
//    //overload std::string f(char q, char w, char e = 'e')
//    static const auto params1 = std::make_tuple(
//                                    np::required_parameter<"q", char>,
//                                    np::default_value<"w", char> = 'W',
//                                    np::default_value<"e", char> = 'E'
//                                );

//    //overload std::size_t f(std::string q, std::string w, std::string e = "e")
//    static const auto params2 = std::make_tuple(
//                                    np::required_parameter<"q", std::string>,
//                                    np::default_parameter<"w", std::string >= "W",
//                                    np::default_parameter<"e", std::string >= "E"
//                                );

//    //dispatch
//    if constexpr(np::is_invokeable_v<decltype(params1), Ts...>)
//    {
//        return np::invoke(&f, params1, std::forward<Ts>(ts)...)
//    }
//    else if constexpr(np::is_invokeable_v<decltype(params2), Ts...>)
//    {
//        return np::invoke(&f, params2, std::forward<Ts>(ts)...)
//    }
//    else
//    {
//        static_assert("no overload was matched!");
//    }
//}

BOOST_AUTO_TEST_CASE(test_function)
{

}
