#define BOOST_TEST_MODULE named_parameters

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/named_parameters/common/id.hpp>

using id_qwer = upp::named_parameter::id<'q', 'w', 'e', 'r'>;

struct w_id
{
    using id_t = id_qwer;
};

struct wo_id {};

BOOST_AUTO_TEST_CASE(test_concepts)
{
    ///TODO
//    static_assert(upp::named_parameter::is_id<id_qwer>);
//    static_assert(!upp::named_parameter::is_id<int>);
//    static_assert(!upp::named_parameter::is_id<w_id>);
//    static_assert(!upp::named_parameter::is_id<wo_id>);

//    static_assert(upp::named_parameter::has_id_t<w_id>);
//    static_assert(!upp::named_parameter::has_id_t<id_qwer>);
//    static_assert(!upp::named_parameter::has_id_t<int>);
//    static_assert(!upp::named_parameter::has_id_t<wo_id>);
}
