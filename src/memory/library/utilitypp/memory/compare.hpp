#pragma once

#include <utilitypp/exception/throw_if/throw_if_null.hpp>

namespace upp
{
    template<class T>
    inline bool memcmp_bytes(const T* ptr, std::uint8_t v)
    {
        upp_throw_if_null(std::invalid_argument, ptr);
        auto uptr = reinterpret_cast<const std::uint8_t*>(ptr);
        const auto end = reinterpret_cast<const std::uint8_t*>(ptr + 1);
        for (; uptr != end && *uptr == v; ++uptr);
        return uptr == end;
    }
}
