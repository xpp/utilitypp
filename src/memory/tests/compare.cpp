#define BOOST_TEST_MODULE compare
#include <sstream>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/memory/compare.hpp>

struct ints
{
    int _0 = 0;
    int _1 = 0;
};

BOOST_AUTO_TEST_CASE(compare)
{
    ints i;
    BOOST_CHECK(upp::memcmp_bytes(&i, 0));
    i._0 = -1;
    BOOST_CHECK(!upp::memcmp_bytes(&i, 0));
    i._1 = -1;
    BOOST_CHECK(upp::memcmp_bytes(&i, 0xff));
    i._0 = 0;
    BOOST_CHECK(!upp::memcmp_bytes(&i, 0));
}
