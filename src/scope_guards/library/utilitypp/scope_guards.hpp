#pragma once

// This file is generated!

#include "scope_guards/on_scope_exit.hpp"
#include "scope_guards/on_scope_fail.hpp"
#include "scope_guards/on_scope_success.hpp"
