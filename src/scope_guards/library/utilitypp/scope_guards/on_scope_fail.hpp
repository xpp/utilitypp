#pragma once

#include <exception>

#include <utilitypp/preprocessor/cat/cat_3.hpp>

namespace upp
{
    namespace detail
    {
        struct on_scope_exit_mode_fail {};

        template <class FType>
        struct on_scope_fail
        {
            explicit on_scope_fail(FType&& f)
                : f{std::move(f)} , num_except{std::uncaught_exceptions()}
            {}
            ~on_scope_fail()
            {
                if(num_except < std::uncaught_exceptions())
                {
                    f();
                }
            }
            FType f;
            const int num_except;
        };

        template <class F> inline auto operator+(on_scope_exit_mode_fail   , F&& fn)
        {
            return on_scope_fail   {std::forward<F>(fn)};
        }
    }
}

#define upp_on_scope_fail    const auto                         \
    upp_cat_3(_upp_on_scope_fail    , __COUNTER__, __LINE__) =  \
    ::upp::detail::on_scope_exit_mode_fail{} + [&]()
