#pragma once

#include <utility>

#include <utilitypp/preprocessor/cat/cat_3.hpp>

namespace upp
{
    namespace detail
    {
        struct on_scope_exit_mode_exit {};

        template <class FType>
        struct on_scope_exit
        {
            explicit on_scope_exit(FType&& f) : f{std::move(f)} {}
            ~on_scope_exit() {f();}
            FType f;
        };

        template <class F> inline auto operator+(on_scope_exit_mode_exit, F&& fn)
        {
            return on_scope_exit{std::forward<F>(fn)};
        }
    }
}

#define upp_on_scope_exit    const auto                         \
    upp_cat_3(_upp_on_scope_exit   , __COUNTER__, __LINE__) =   \
    ::upp::detail::on_scope_exit_mode_exit{} + [&]()
