#pragma once

#include <exception>

#include <utilitypp/preprocessor/cat/cat_3.hpp>

namespace upp
{
    namespace detail
    {
        struct on_scope_exit_mode_success {};

        template <class FType>
        struct on_scope_success
        {
            explicit on_scope_success(FType&& f)
                : f{std::move(f)} , num_except{std::uncaught_exceptions()}
            {}
            ~on_scope_success()
            {
                if(num_except == std::uncaught_exceptions())
                {
                    f();
                }
            }
            FType f;
            const int num_except;
        };

        template <class F> inline auto operator+(on_scope_exit_mode_success, F&& fn)
        {
            return on_scope_success{std::forward<F>(fn)};
        }
    }
}

#define upp_on_scope_success const auto                         \
    upp_cat_3(_upp_on_scope_success, __COUNTER__, __LINE__) =   \
    ::upp::detail::on_scope_exit_mode_success{} + [&]()
