#define BOOST_TEST_MODULE util
#include <boost/test/included/unit_test.hpp>

#include <utilitypp/scope_guards.hpp>
#include <utilitypp/io/varout.hpp>

BOOST_AUTO_TEST_CASE( on_scope_exit )
{
    int called_outer_on_exit    = -1;
    int called_outer_on_fail    = -1;
    int called_outer_on_success = -1;
    int called_try_on_exit      = -1;
    int called_try_on_fail      = -1;
    int called_try_on_success   = -1;
    int called_catch_on_exit    = -1;
    int called_catch_on_fail    = -1;
    int called_catch_on_success = -1;
    int i = -1;
    {
        upp_on_scope_exit    {called_outer_on_exit    = ++i;};
        upp_on_scope_fail    {called_outer_on_fail    = ++i;};
        upp_on_scope_success {called_outer_on_success = ++i;};
        try
        {
            upp_on_scope_exit    {called_try_on_exit    = ++i;};
            upp_on_scope_fail    {called_try_on_fail    = ++i;};
            upp_on_scope_success {called_try_on_success = ++i;};
            throw 1;
        }
        catch(...)
        {
            upp_on_scope_exit    {called_catch_on_exit    = ++i;};
            upp_on_scope_fail    {called_catch_on_fail    = ++i;};
            upp_on_scope_success {called_catch_on_success = ++i;};
        }
    }

    std::cout << upp_varout(i) << "\n\n";

    std::cout << upp_varout(called_outer_on_exit   ) << "\n";
    std::cout << upp_varout(called_outer_on_fail   ) << "\n";
    std::cout << upp_varout(called_outer_on_success) << "\n\n";

    std::cout << upp_varout(called_try_on_exit   ) << "\n";
    std::cout << upp_varout(called_try_on_fail   ) << "\n";
    std::cout << upp_varout(called_try_on_success) << "\n\n";

    std::cout << upp_varout(called_catch_on_exit   ) << "\n";
    std::cout << upp_varout(called_catch_on_fail   ) << "\n";
    std::cout << upp_varout(called_catch_on_success) << "\n";

    BOOST_REQUIRE_EQUAL(called_try_on_success  , -1);
    BOOST_REQUIRE_EQUAL(called_try_on_fail     ,  0);
    BOOST_REQUIRE_EQUAL(called_try_on_exit     ,  1);

    BOOST_REQUIRE_EQUAL(called_catch_on_success,  2);
    BOOST_REQUIRE_EQUAL(called_catch_on_fail   , -1);
    BOOST_REQUIRE_EQUAL(called_catch_on_exit   ,  3);

    BOOST_REQUIRE_EQUAL(called_outer_on_success,  4);
    BOOST_REQUIRE_EQUAL(called_outer_on_fail   , -1);
    BOOST_REQUIRE_EQUAL(called_outer_on_exit   ,  5);
}
