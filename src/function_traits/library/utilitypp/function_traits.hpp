#pragma once

// This file is generated!

#include "function_traits/number_of_parameters.hpp"
#include "function_traits/return_type.hpp"
#include "function_traits/return_type_is.hpp"
#include "function_traits/return_type_is_not.hpp"
