#pragma once

#include <type_traits>

namespace upp
{
    template <std::size_t V>
    using size_t_constant = std::integral_constant<std::size_t, V>;

    template <class F>
    struct number_of_parameters {};

    //free
    template <class R, class...Ps>
    struct number_of_parameters < R(Ps...) > : size_t_constant<sizeof...(Ps)>
    {};

    template <class R, class...Ps>
    struct number_of_parameters < R(*)(Ps...) > : size_t_constant<sizeof...(Ps)>
    {};

    //member
    template <class C, class R, class...Ps>
    struct number_of_parameters < R(C::*)(Ps...) > : size_t_constant<sizeof...(Ps)>
    {};

    template <class C, class R, class...Ps>
    struct number_of_parameters < R(C::*)(Ps...) const> : size_t_constant<sizeof...(Ps)>
    {};

    //rval member
    template <class C, class R, class...Ps>
    struct number_of_parameters < R(C::*)(Ps...) &&> : size_t_constant<sizeof...(Ps)>
    {};

    template <class C, class R, class...Ps>
    struct number_of_parameters < R(C::*)(Ps...) const &&> : size_t_constant<sizeof...(Ps)>
    {};

    //convenience
    template <class F>
    static constexpr std::size_t number_of_parameters_v = number_of_parameters<F>::value;
}
