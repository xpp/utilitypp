#pragma once

#include <type_traits>

#include "return_type.hpp"

namespace upp
{
    template <class T, class F, class = void >
    struct return_type_is_not {};

    template <class T, class F>
    struct return_type_is_not < T, F, std::void_t<return_type_t<F>> >
        : std::bool_constant<!std::is_same_v<return_type_t<F>, T>>
    {};

    template <class T, class F>
    static constexpr bool return_type_is_not_v = return_type_is_not<T, F>::value;

    //specializations
    template <class F>
    using return_type_is_not_void = return_type_is_not<void, F>;

    template <class F>
    static constexpr bool return_type_is_not_void_v = return_type_is_not_void<F>::value;
}
