#pragma once

#include <type_traits>

namespace upp
{
    template <class F>
    struct return_type {};

    //free
    template <class R, class...Ps>
    struct return_type < R(Ps...) > : std::type_identity<R>
    {};

    template <class R, class...Ps>
    struct return_type < R(*)(Ps...) > : std::type_identity<R>
    {};

    //member
    template <class C, class R, class...Ps>
    struct return_type < R(C::*)(Ps...) > : std::type_identity<R>
    {};

    template <class C, class R, class...Ps>
    struct return_type < R(C::*)(Ps...) const > : std::type_identity<R>
    {};

    //rval member
    template <class C, class R, class...Ps>
    struct return_type < R(C::*)(Ps...) &&> : std::type_identity<R>
    {};

    template <class C, class R, class...Ps>
    struct return_type < R(C::*)(Ps...) const &&> : std::type_identity<R>
    {};

    //convenience
    template <class F>
    using return_type_t = typename return_type<F>::type;
}
