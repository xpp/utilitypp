#pragma once

#include <type_traits>

#include "return_type.hpp"

namespace upp
{
    template <class T, class F, class = void >
    struct return_type_is {};

    template <class T, class F>
    struct return_type_is < T, F, std::void_t<return_type_t<F>> > : std::is_same<return_type_t<F>, T>
    {};

    template <class T, class F>
    static constexpr bool return_type_is_v = return_type_is<T, F>::value;

    //specializations
    template <class F>
    using return_type_is_void = return_type_is<void, F>;

    template <class F>
    static constexpr bool return_type_is_void_v = return_type_is_void<F>::value;
}
