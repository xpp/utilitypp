#define BOOST_TEST_MODULE function_traits

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/function_traits/number_of_parameters.hpp>

#define consume_functions(name,...)                             \
    void consume_##name(__VA_ARGS__);                           \
    struct struct_consume_##name                                \
    {                                                           \
        void               consume_##name(__VA_ARGS__);         \
        void         const_consume_##name(__VA_ARGS__) const;   \
        void          rval_consume_##name(__VA_ARGS__) &&;      \
        void    const_rval_consume_##name(__VA_ARGS__) const &&;\
        static void static_consume_##name(__VA_ARGS__);         \
    }

consume_functions(0,);
consume_functions(1, int);
consume_functions(2, int, int);
consume_functions(3, int*, char&, long);

BOOST_AUTO_TEST_CASE( test_number_of_parameters_v )
{
    using upp::number_of_parameters_v;

    static_assert(number_of_parameters_v<decltype (                              consume_0 )> == 0);
    static_assert(number_of_parameters_v<decltype (&                             consume_0 )> == 0);
    static_assert(number_of_parameters_v<decltype (&struct_consume_0::           consume_0 )> == 0);
    static_assert(number_of_parameters_v<decltype (&struct_consume_0::     const_consume_0 )> == 0);
    static_assert(number_of_parameters_v<decltype (&struct_consume_0::      rval_consume_0 )> == 0);
    static_assert(number_of_parameters_v<decltype (&struct_consume_0::const_rval_consume_0 )> == 0);
    static_assert(number_of_parameters_v<decltype (&struct_consume_0::    static_consume_0 )> == 0);

    static_assert(number_of_parameters_v<decltype (                              consume_1 )> == 1);
    static_assert(number_of_parameters_v<decltype (&                             consume_1 )> == 1);
    static_assert(number_of_parameters_v<decltype (&struct_consume_1::           consume_1 )> == 1);
    static_assert(number_of_parameters_v<decltype (&struct_consume_1::     const_consume_1 )> == 1);
    static_assert(number_of_parameters_v<decltype (&struct_consume_1::      rval_consume_1 )> == 1);
    static_assert(number_of_parameters_v<decltype (&struct_consume_1::const_rval_consume_1 )> == 1);
    static_assert(number_of_parameters_v<decltype (&struct_consume_1::    static_consume_1 )> == 1);

    static_assert(number_of_parameters_v<decltype (                              consume_2 )> == 2);
    static_assert(number_of_parameters_v<decltype (&                             consume_2 )> == 2);
    static_assert(number_of_parameters_v<decltype (&struct_consume_2::           consume_2 )> == 2);
    static_assert(number_of_parameters_v<decltype (&struct_consume_2::     const_consume_2 )> == 2);
    static_assert(number_of_parameters_v<decltype (&struct_consume_2::      rval_consume_2 )> == 2);
    static_assert(number_of_parameters_v<decltype (&struct_consume_2::const_rval_consume_2 )> == 2);
    static_assert(number_of_parameters_v<decltype (&struct_consume_2::    static_consume_2 )> == 2);

    static_assert(number_of_parameters_v<decltype (                              consume_3 )> == 3);
    static_assert(number_of_parameters_v<decltype (&                             consume_3 )> == 3);
    static_assert(number_of_parameters_v<decltype (&struct_consume_3::           consume_3 )> == 3);
    static_assert(number_of_parameters_v<decltype (&struct_consume_3::     const_consume_3 )> == 3);
    static_assert(number_of_parameters_v<decltype (&struct_consume_3::      rval_consume_3 )> == 3);
    static_assert(number_of_parameters_v<decltype (&struct_consume_3::const_rval_consume_3 )> == 3);
    static_assert(number_of_parameters_v<decltype (&struct_consume_3::    static_consume_3 )> == 3);
}
