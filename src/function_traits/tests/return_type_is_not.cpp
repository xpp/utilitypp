#define BOOST_TEST_MODULE function_traits

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/function_traits/return_type_is_not.hpp>

#define return_functions(name,...)                  \
    __VA_ARGS__ return_##name();                    \
    struct struct_return_##name                     \
    {                                               \
        __VA_ARGS__ return_##name();                \
        __VA_ARGS__ const_return_##name() const;    \
        static __VA_ARGS__ static_return_##name();  \
    }

return_functions(void, void);
return_functions(int, int);
return_functions(int_ref, int&);
return_functions(int_ptr, int*);

BOOST_AUTO_TEST_CASE( test_return_type_is_not_v )
{
    using upp::return_type_is_not_v;

    //true cases
    BOOST_CHECK(!(return_type_is_not_v<void, decltype (                            return_void )>));
    BOOST_CHECK(!(return_type_is_not_v<void, decltype (&                           return_void )>));
    BOOST_CHECK(!(return_type_is_not_v<void, decltype (&struct_return_void::       return_void )>));
    BOOST_CHECK(!(return_type_is_not_v<void, decltype (&struct_return_void:: const_return_void )>));
    BOOST_CHECK(!(return_type_is_not_v<void, decltype (&struct_return_void::static_return_void )>));

    BOOST_CHECK(!(return_type_is_not_v<int, decltype (                           return_int )>));
    BOOST_CHECK(!(return_type_is_not_v<int, decltype (&                          return_int )>));
    BOOST_CHECK(!(return_type_is_not_v<int, decltype (&struct_return_int::       return_int )>));
    BOOST_CHECK(!(return_type_is_not_v<int, decltype (&struct_return_int:: const_return_int )>));
    BOOST_CHECK(!(return_type_is_not_v<int, decltype (&struct_return_int::static_return_int )>));

    BOOST_CHECK(!(return_type_is_not_v<int&, decltype (                               return_int_ref )>));
    BOOST_CHECK(!(return_type_is_not_v<int&, decltype (&                              return_int_ref )>));
    BOOST_CHECK(!(return_type_is_not_v<int&, decltype (&struct_return_int_ref::       return_int_ref )>));
    BOOST_CHECK(!(return_type_is_not_v<int&, decltype (&struct_return_int_ref:: const_return_int_ref )>));
    BOOST_CHECK(!(return_type_is_not_v<int&, decltype (&struct_return_int_ref::static_return_int_ref )>));

    BOOST_CHECK(!(return_type_is_not_v<int*, decltype (                               return_int_ptr )>));
    BOOST_CHECK(!(return_type_is_not_v<int*, decltype (&                              return_int_ptr )>));
    BOOST_CHECK(!(return_type_is_not_v<int*, decltype (&struct_return_int_ptr::       return_int_ptr )>));
    BOOST_CHECK(!(return_type_is_not_v<int*, decltype (&struct_return_int_ptr:: const_return_int_ptr )>));
    BOOST_CHECK(!(return_type_is_not_v<int*, decltype (&struct_return_int_ptr::static_return_int_ptr )>));

    //check false cases
    BOOST_CHECK((return_type_is_not_v<int&, decltype (                           return_int )>));
    BOOST_CHECK((return_type_is_not_v<int&, decltype (&                          return_int )>));
    BOOST_CHECK((return_type_is_not_v<int&, decltype (&struct_return_int::       return_int )>));
    BOOST_CHECK((return_type_is_not_v<int&, decltype (&struct_return_int:: const_return_int )>));
    BOOST_CHECK((return_type_is_not_v<int&, decltype (&struct_return_int::static_return_int )>));

    BOOST_CHECK((return_type_is_not_v<int*, decltype (                           return_int )>));
    BOOST_CHECK((return_type_is_not_v<int*, decltype (&                          return_int )>));
    BOOST_CHECK((return_type_is_not_v<int*, decltype (&struct_return_int::       return_int )>));
    BOOST_CHECK((return_type_is_not_v<int*, decltype (&struct_return_int:: const_return_int )>));
    BOOST_CHECK((return_type_is_not_v<int*, decltype (&struct_return_int::static_return_int )>));

    BOOST_CHECK((return_type_is_not_v<int, decltype (                               return_int_ref )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&                              return_int_ref )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&struct_return_int_ref::       return_int_ref )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&struct_return_int_ref:: const_return_int_ref )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&struct_return_int_ref::static_return_int_ref )>));

    BOOST_CHECK((return_type_is_not_v<int, decltype (                               return_int_ptr )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&                              return_int_ptr )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&struct_return_int_ptr::       return_int_ptr )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&struct_return_int_ptr:: const_return_int_ptr )>));
    BOOST_CHECK((return_type_is_not_v<int, decltype (&struct_return_int_ptr::static_return_int_ptr )>));
}
