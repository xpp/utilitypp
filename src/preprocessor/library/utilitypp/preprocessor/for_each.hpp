#pragma once

#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/variadic/to_seq.hpp>

#define ditdp_for_each(Macro, Data, ...) BOOST_PP_SEQ_FOR_EACH(Macro, Data, BOOST_PP_VARIADIC_TO_SEQ(__VA_ARGS__))
