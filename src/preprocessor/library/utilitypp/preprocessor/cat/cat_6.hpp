#pragma once

#define upp_cat_6(...) _detail_upp_cat_6(__VA_ARGS__)
#define _detail_upp_cat_6(_1, _2, _3, _4, _5, _6)   \
    _1 ## _2 ## _3 ## _4 ## _5 ## _6
