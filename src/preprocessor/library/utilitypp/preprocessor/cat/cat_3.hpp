#pragma once

#define upp_cat_3(...) _detail_upp_cat_3(__VA_ARGS__)
#define _detail_upp_cat_3(_1, _2, _3) _1 ## _2 ## _3
