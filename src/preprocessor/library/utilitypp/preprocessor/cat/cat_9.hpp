#pragma once

#define upp_cat_9(...) _detail_upp_cat_9(__VA_ARGS__)
#define _detail_upp_cat_9(_1, _2, _3, _4, _5, _6, _7, _8, _9)   \
    _1 ## _2 ## _3 ## _4 ## _5 ## _6 ## _7 ## _8 ## _9
