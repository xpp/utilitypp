#pragma once

#define upp_cat_2(...) _detail_upp_cat_2(__VA_ARGS__)
#define _detail_upp_cat_2(_1, _2) _1 ## _2
