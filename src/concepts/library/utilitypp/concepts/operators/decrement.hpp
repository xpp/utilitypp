#pragma once

#include <concepts>

namespace upp::concepts
{
    template <class T>
    concept pre_decrementable = requires(T t)
    {
        { --t } -> std::same_as<T&>;
    };
    template <class T>
    concept post_decrementable = requires(T t)
    {
        { t-- } -> std::same_as<T&>;
    };
}

