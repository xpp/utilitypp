#pragma once

#include <concepts>

namespace upp::concepts
{
    //following https://en.cppreference.com/w/cpp/concepts/assignable_from

    template<class L, class R>
    concept assignable = std::assignable_from<L, R>;

    #define make_assignable_concept(op, name)                   \
        template<class L, class R>                              \
        concept name##_assignable =                             \
            std::is_lvalue_reference_v<L> &&                    \
            std::common_reference_with<                         \
                const std::remove_reference_t<L>&,              \
                const std::remove_reference_t<R>&               \
            > &&                                                \
            requires(L l, R&& r)                                \
            {                                                   \
                { l = std::forward<R>(r) } -> std::same_as<L>;  \
            }

    make_assignable_concept(+=, addition);
    make_assignable_concept(-=, subtraction);
    make_assignable_concept(*=, multiplication);
    make_assignable_concept(/=, division);
    make_assignable_concept(%=, modulo);
    make_assignable_concept(&=, bitwise_and);
    make_assignable_concept(|=, bitwise_or);
    make_assignable_concept(^=, bitwise_xor);
    make_assignable_concept(<<=, bitwise_left_shift);
    make_assignable_concept(>>=, bitwise_right_shift);

    #undef make_assignable_concept
}

