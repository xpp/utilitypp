#pragma once

#include <concepts>

namespace upp::concepts
{
    template <class T>
    concept pre_incrementable = requires(T t)
    {
        { ++t } -> std::same_as<T&>;
    };
    template <class T>
    concept post_incrementable = requires(T t)
    {
        { t++ } -> std::same_as<T&>;
    };
}

