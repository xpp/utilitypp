#pragma once

#include <concepts>

// all of these follow
// https://en.cppreference.com/w/cpp/concepts/equality_comparable
// with following modification: the comparable version is defined via the
// omparable_with version and takes two template parameters
// (the second defaults to the first)

#define upp_make_compare_concept(op, name)                                      \
    namespace upp::concepts::detail::name                                       \
    {                                                                           \
        template<class T, class U = T>                                          \
        concept weak_##name##_comparable =                                      \
            requires(                                                           \
                const std::remove_reference_t<T>& t,                            \
                const std::remove_reference_t<U>& u                             \
            )                                                                   \
        {                                                                       \
            { t op u } -> std::boolean;                                         \
            { u op t } -> std::boolean;                                         \
        };                                                                      \
    }                                                                           \
    namespace upp::concepts                                                     \
    {                                                                           \
        template <class T, class U>                                             \
        concept name##_comparable_with =                                        \
            upp::concepts::detail::name::weak_##name##_comparable<T>&&          \
            upp::concepts::detail::name::weak_##name##_comparable<U>&&          \
            std::common_reference_with <                                        \
                const std::remove_reference_t<T>&,                              \
                const std::remove_reference_t<U>&                               \
            > &&                                                                \
            upp::concepts::detail::name::weak_##name##_comparable<              \
                std::common_reference_t<                                        \
                    const std::remove_reference_t<T>&,                          \
                    const std::remove_reference_t<U>&                           \
                >                                                               \
            > &&                                                                \
            upp::concepts::detail::name::weak_##name##_comparable<T, U>;        \
                                                                                \
        template <class T, class U = T>                                         \
        concept name##_comparable = name##_comparable_with<T, T>;               \
    }

///TODO
//upp_make_compare_concept(== , equal)
//upp_make_compare_concept(!= , inequal)
//upp_make_compare_concept(<  , less_than)
//upp_make_compare_concept(<= , less_equal)
//upp_make_compare_concept(>  , greater_than)
//upp_make_compare_concept(>= , greater_equal)
//upp_make_compare_concept(<=>, three_way)

#undef upp_make_compare_concept

