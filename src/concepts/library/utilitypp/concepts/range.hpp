#pragma once

namespace upp
{
//    // *INDENT-OFF*
//    template<class T, class Val>
//    concept Range = requires(T a)
//    {
//        {a.begin()};
//        {a.end()};
//        {*a.begin()} -> std::convertible_to<Val>;
//        {*a.begin()} -> std::assignable_from<Val>;
//    };
//    // *INDENT-ON*
}
