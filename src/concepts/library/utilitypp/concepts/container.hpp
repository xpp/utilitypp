#pragma once

#include "range.hpp"

namespace upp
{
//    // *INDENT-OFF*
//    template<class T, class Val>
//    concept Container =
//        Range<T, Val> &&
//        requires(T a)
//        {
//            {a.size()} -> std::same_as<std::size_t>;
//        };
//    // *INDENT-ON*
}
