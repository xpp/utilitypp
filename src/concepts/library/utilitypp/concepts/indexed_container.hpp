#pragma once

#include "container.hpp"

namespace upp
{
//    // *INDENT-OFF*
//template<class T, class Val = typename T::value_type>
//    concept IndexedContainer =
//        Container<T, Val> &&
//        requires(T a, std::size_t i)
//        {
//            {a.at(i)} -> std::convertible_to<Val>;
//            {a.at(i)} -> std::assignable_from<Val>;
//        };
//    // *INDENT-ON*
}
