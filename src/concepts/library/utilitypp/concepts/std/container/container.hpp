#pragma once

#include <type_traits>
#include <iterator>

#include "../basic/copy_constructible.hpp"
#include "../basic/default_constructible.hpp"
#include "../basic/move_constructible.hpp"
#include "../basic/copy_assignable.hpp"
#include "../basic/move_assignable.hpp"
#include "../basic/destructible.hpp"

#include "../library_wide/equality_comparable.hpp"
#include "../library_wide/swappable.hpp"

#include "../iterator/legacy_forward_iterator.hpp"

#include "erasable.hpp"
#include "copy_insertable.hpp"

namespace upp::concepts::detail::container
{
    template<class C, class value_type>
    concept member_type_requirements =
        requires()
    {
        //sub types
        typename C::value_type;
        typename C::reference;
        typename C::const_reference;
        typename C::iterator;
        typename C::const_iterator;
        typename C::difference_type;
        typename C::size_type;

        //sub type properties
        std::same_as<typename C::value_type, value_type>;

        std::same_as<typename C::reference, value_type&>;
        std::same_as<typename C::const_reference, const value_type&>;
        legacy_forward_iterator<typename C::iterator>;
        legacy_forward_iterator<typename C::const_iterator>;
        std::convertible_to<typename C::iterator, typename C::const_iterator>;

        std::signed_integral<typename C::difference_type>;
        std::unsigned_integral<typename C::size_type>;
        (std::numeric_limits<typename C::difference_type>::max() <= std::numeric_limits<typename C::size_type>::max());///TODO concepts incoporates/includes, includes_positive_portion
        std::same_as<typename C::difference_type, typename std::iterator_traits<typename C::iterator>::difference_type>;
        std::same_as<typename C::difference_type, typename std::iterator_traits<typename C::iterator>::const_iterator>;
    };
}

namespace upp::concepts
{
    ///this can't check complexity and high level conditions
    template<class C, class value_type>
    concept container =
        //type requirements
        erasable<value_type>&&
        destructible<value_type>&&

        //container requirements
        detail::container::member_type_requirements<C, value_type>&&
        default_constructible<C>&&
        copy_constructible<C>&&
        move_constructible<C>&&
        copy_assignable<C>&&
        move_assignable<C>&&
        destructible<C>&&
        swappable<C>&&

        //conditional requirements
        (!equality_comparable<value_type> || equality_comparable<C>) &&

        (!copy_insertable<value_type> || copy_constructible<C>) &&

        //container requirements without an attached concept
        requires(C a, C b, const C c, value_type t)
    {
        {
            a.begin()
        }
        -> std::same_as<typename C::iterator>;
        {
            c.begin()
        }
        -> std::same_as<typename C::const_iterator>;
        {
            a.end()
        }
        -> std::same_as<typename C::iterator>;
        {
            c.end()
        }
        -> std::same_as<typename C::const_iterator>;

        {
            a.cbegin()
        }
        -> std::same_as<typename C::const_iterator>;
        {
            c.cbegin()
        }
        -> std::same_as<typename C::const_iterator>;
        {
            a.cend()
        }
        -> std::same_as<typename C::const_iterator>;
        {
            c.cend()
        }
        -> std::same_as<typename C::const_iterator>;

        {
            a.swap(b)
        }
        -> std::same_as<void>;

        {
            a.size()
        }
        -> std::same_as<typename C::size_type>;
        {
            a.max_size()
        }
        -> std::same_as<typename C::size_type>;

        {
            a.empty()
        }
        -> std::convertible_to<bool>;
    };
}
