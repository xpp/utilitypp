#pragma once

#include <type_traits>

namespace upp::concepts
{
///TODO erasable
    template<class T>
    concept erasable = true;
}
