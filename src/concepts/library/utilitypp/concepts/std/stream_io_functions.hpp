#pragma once

// This file is generated!

#include "stream_io_functions/formatted_input_function.hpp"
#include "stream_io_functions/formatted_output_function.hpp"
#include "stream_io_functions/unformatted_input_function.hpp"
#include "stream_io_functions/unformatted_output_function.hpp"
