#pragma once

// This file is generated!

#include "basic/copy_assignable.hpp"
#include "basic/copy_constructible.hpp"
#include "basic/default_constructible.hpp"
#include "basic/destructible.hpp"
#include "basic/move_assignable.hpp"
#include "basic/move_constructible.hpp"
