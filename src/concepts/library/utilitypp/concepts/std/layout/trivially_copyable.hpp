#pragma once

#include <type_traits>

namespace upp::concepts
{
    template<class T>
    concept trivially_copyable = std::is_trivially_copyable_v<T>;
}
