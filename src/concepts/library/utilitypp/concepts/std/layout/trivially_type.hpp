#pragma once

#include <type_traits>

namespace upp::concepts
{
    template<class T>
    concept trivial_type = std::is_trivial_v<T>;
}
