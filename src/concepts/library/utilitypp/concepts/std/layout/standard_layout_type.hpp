#pragma once

#include <type_traits>

namespace upp::concepts
{
    template<class T>
    concept standard_layout_type = std::is_standard_layout_v<T>;
}
