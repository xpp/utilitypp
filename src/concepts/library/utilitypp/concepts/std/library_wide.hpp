#pragma once

// This file is generated!

#include "library_wide/allocator.hpp"
#include "library_wide/binary_predicate.hpp"
#include "library_wide/callable.hpp"
#include "library_wide/compare.hpp"
#include "library_wide/equality_comparable.hpp"
#include "library_wide/function_object.hpp"
#include "library_wide/hash.hpp"
#include "library_wide/less_than_comparable.hpp"
#include "library_wide/nullable_pointer.hpp"
#include "library_wide/predicate.hpp"
#include "library_wide/swappable.hpp"
#include "library_wide/value_swappable.hpp"
