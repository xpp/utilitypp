#pragma once

#include <concepts>

namespace upp::concepts
{
    using std::equality_comparable;
    using std::equality_comparable_with;
}
