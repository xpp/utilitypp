#pragma once

#include <type_traits>

#include "../basic/default_constructible.hpp"
#include "../basic/copy_constructible.hpp"
#include "../basic/copy_assignable.hpp"
#include "../basic/destructible.hpp"

#include "equality_comparable.hpp"

namespace upp::concepts
{
    /// the implicit value sematics can't be checked!
    template<class T>
    concept nullable_pointer =
        default_constructible<T>&&
        equality_comparable<T>&&
        copy_constructible<T>&&
        copy_assignable<T>&&
        destructible<T>&&
        requires(T p, std::nullptr_t np, const std::nullptr_t cnp)
    {
        //        { T q( np)  } -> std::same_as<T>;
        //        { T q(cnp)  } -> std::same_as<T>;

        //        { T q =  np } -> std::same_as<T>;
        //        { T q = cnp } -> std::same_as<T>;

        {
            T(np)
        }
        -> std::same_as<T>;
        {
            T(cnp)
        }
        -> std::same_as<T>;

        {
            p = np
        }
        -> std::same_as<T>;
        {
            p = cnp
        }
        -> std::same_as<T>;

        {
            p == cnp
        }
        -> std::convertible_to<T>;
        {
            cnp == p
        }
        -> std::convertible_to<T>;
        {
            p == np
        }
        -> std::convertible_to<T>;
        {
            np == p
        }
        -> std::convertible_to<T>;

        {
            p != cnp
        }
        -> std::convertible_to<T>;
        {
            cnp != p
        }
        -> std::convertible_to<T>;
        {
            p != np
        }
        -> std::convertible_to<T>;
        {
            np != p
        }
        -> std::convertible_to<T>;
    };
}
