#pragma once

#include <type_traits>

namespace upp::concepts
{
    template<class T>
    concept callable = std::is_invocable_v<T>;
}
