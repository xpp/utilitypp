#pragma once

#include <concepts>

namespace upp::concepts
{
    using std::swappable;
    using std::swappable_with;
}
