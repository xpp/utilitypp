#pragma once

#include <type_traits>

namespace upp::concepts
{
    template<class T>
    concept move_assignable = std::is_move_assignable_v<T>;
}
