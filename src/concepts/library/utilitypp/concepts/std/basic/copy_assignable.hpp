#pragma once

#include <type_traits>

namespace upp::concepts
{
    template<class T>
    concept copy_assignable = std::is_copy_assignable_v<T>;
}
