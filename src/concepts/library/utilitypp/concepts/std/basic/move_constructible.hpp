#pragma once

#include <concepts>

namespace upp::concepts
{
    using std::move_constructible;
}
