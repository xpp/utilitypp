#pragma once

#include <concepts>

namespace upp::concepts
{
    ///TODO //using std::default_constructible;
    template <class T>
    concept default_constructible = std::constructible_from<T>;
}
