#include <Eigen/Geometry>

namespace detail::generate_quat_seq
{
    inline void mk_pix2xy(int* pix2x, int* pix2y)
    {
        /* =======================================================================
             * subroutine mk_pix2xy
             * =======================================================================
             * constructs the array giving x and y in the face from pixel number
             * for the nested (quad-cube like) ordering of pixels
             *
             * the bits corresponding to x and y are interleaved in the pixel number
             * one breaks up the pixel number by even and odd bits
             * =======================================================================
             */

        int i, kpix, jpix, IX, IY, IP, ID;
        for (i = 0; i < 1023; i++)
        {
            pix2x[i] = 0;
        }

        for (kpix = 0; kpix < 1024; kpix++)
        {
            jpix = kpix;
            IX = 0;
            IY = 0;
            IP = 1 ;//              ! bit position (in x and y)
            while (jpix != 0) // ! go through all the bits
            {
                ID = static_cast<int>(std::fmod(jpix, 2)); //  ! bit value (in kpix), goes in ix
                jpix = jpix / 2;
                IX = ID * IP + IX;

                ID = static_cast<int>(std::fmod(jpix, 2)); //  ! bit value (in kpix), goes in iy
                jpix = jpix / 2;
                IY = ID * IP + IY;
                IP = 2 * IP; //         ! next bit (in x and y)
            }
            pix2x[kpix] = IX;//     ! in 0,31
            pix2y[kpix] = IY;//     ! in 0,31
        }
        return;
    }

    inline std::array<double, 2> pix2ang_nest(long nside, long ipix)
    {

        /*
              c=======================================================================
              subroutine pix2ang_nest(nside, ipix, theta, phi)
              c=======================================================================
              c     gives theta and phi corresponding to pixel ipix (NESTED)
              c     for a parameter nside
              c=======================================================================
            */

        int npix, npface, face_num;
        int  ipf, ip_low, ip_trunc, ip_med, ip_hi;
        int     ix, iy, jrt, jr, nr, jpt, jp, kshift, nl4;
        double z, fn, fact1, fact2;
        double piover2 = 0.5 * M_PI;
        int ns_max = 8192;

        static int pix2x[1024], pix2y[1024];
        //      common /pix2xy/ pix2x, pix2y

        int jrll[12], jpll[12];// ! coordinate of the lowest corner of each face
        //      data jrll/2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4/ ! in unit of nside
        //      data jpll/1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7/ ! in unit of nside/2
        jrll[0] = 2;
        jrll[1] = 2;
        jrll[2] = 2;
        jrll[3] = 2;
        jrll[4] = 3;
        jrll[5] = 3;
        jrll[6] = 3;
        jrll[7] = 3;
        jrll[8] = 4;
        jrll[9] = 4;
        jrll[10] = 4;
        jrll[11] = 4;
        jpll[0] = 1;
        jpll[1] = 3;
        jpll[2] = 5;
        jpll[3] = 7;
        jpll[4] = 0;
        jpll[5] = 2;
        jpll[6] = 4;
        jpll[7] = 6;
        jpll[8] = 1;
        jpll[9] = 3;
        jpll[10] = 5;
        jpll[11] = 7;


        if (nside < 1 || nside > ns_max)
        {
            fprintf(stderr, "%s (%d): nside out of range: %ld\n", __FILE__, __LINE__, nside);
            exit(0);
        }
        npix = 12 * nside * nside;
        if (ipix < 0 || ipix > npix - 1)
        {
            fprintf(stderr, "%s (%d): ipix out of range: %ld\n", __FILE__, __LINE__, ipix);
            exit(0);
        }

        /* initiates the array for the pixel number -> (x,y) mapping */
        if (pix2x[1023] <= 0)
        {
            mk_pix2xy(pix2x, pix2y);
        }

        fn = 1.*nside;
        fact1 = 1. / (3.*fn * fn);
        fact2 = 2. / (3.*fn);
        nl4   = 4 * nside;

        //c     finds the face, and the number in the face
        npface = nside * nside;

        face_num = ipix / npface; //  ! face number in {0,11}
        ipf = static_cast<int>(std::fmod(ipix, npface)); //  ! pixel number in the face {0,npface-1}

        //c     finds the x,y on the face (starting from the lowest corner)
        //c     from the pixel number
        ip_low = static_cast<int>(std::fmod(ipf, 1024)); //       ! content of the last 10 bits
        ip_trunc =   ipf / 1024 ; //       ! truncation of the last 10 bits
        ip_med = static_cast<int>(std::fmod(ip_trunc, 1024)); //  ! content of the next 10 bits
        ip_hi  =     ip_trunc / 1024   ; //! content of the high weight 10 bits

        ix = 1024 * pix2x[ip_hi] + 32 * pix2x[ip_med] + pix2x[ip_low];
        iy = 1024 * pix2y[ip_hi] + 32 * pix2y[ip_med] + pix2y[ip_low];

        //c     transforms this in (horizontal, vertical) coordinates
        jrt = ix + iy;//  ! 'vertical' in {0,2*(nside-1)}
        jpt = ix - iy;//  ! 'horizontal' in {-nside+1,nside-1}

        //c     computes the z coordinate on the sphere
        jr =  jrll[face_num] * nside - jrt - 1;
        nr = nside;//                  ! equatorial region (the most frequent)
        z  = (2 * nside - jr) * fact2;
        kshift = static_cast<int>(std::fmod(jr - nside, 2));
        if (jr < nside)  //then     ! north pole region
        {
            nr = jr;
            z = 1. - nr * nr * fact1;
            kshift = 0;
        }
        else
        {
            if (jr > 3 * nside) // then ! south pole region
            {
                nr = nl4 - jr;
                z = - 1. + nr * nr * fact1;
                kshift = 0;
            }
        }
        const double theta = std::acos(z);

        //c     computes the phi coordinate on the sphere, in [0,2Pi]
        //      jp = (jpll[face_num+1]*nr + jpt + 1 + kshift)/2;//  ! 'phi' number in the ring in {1,4*nr}
        jp = (jpll[face_num] * nr + jpt + 1 + kshift) / 2;
        if (jp > nl4)
        {
            jp = jp - nl4;
        }
        if (jp < 1)
        {
            jp = jp + nl4;
        }

        const double phi = (jp - (kshift + 1) * 0.5) * (piover2 / nr);
        return {theta, phi};
    }

    inline std::array<double, 3> find_point(int base_grid, long int point, long int level, long int healpix_point, double s1_point)
    {
        int position = point % 8;
        long int quo = 0;
        double interval = 30 / level;
        // the choosing of the order of the first resolution 4 points depends on which base healpix grid we are now dividing

        if (base_grid == 6 or base_grid == 7)
        {
            switch (position) //this position tells which of the eight points of the cube to consider
            {
                case 0:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;

            }
        }
        else if (base_grid == 3 or base_grid == 1 or base_grid == 9 or base_grid == 11)
        {
            switch (position)
            {
                case 0:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;

            }
        }
        else if (base_grid == 2 or base_grid == 0 or base_grid == 10 or base_grid == 8)
        {
            switch (position)
            {
                case 0:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;

            }
        }
        else if (base_grid == 4 or base_grid == 5)
        {
            switch (position)
            {
                case 0:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;

            }
        }

        quo = point / 8;
        if (quo == 0)
        {
            long int nside = pow(2, level);
            const auto [theta, phi] = pix2ang_nest(nside, healpix_point);
            const double psi = s1_point * M_PI / 180;
            return {theta, phi, psi};
        }
        return find_point(base_grid, quo - 1, level + 1, 4 * healpix_point, s1_point);
    }
}

template <class FloatT = double>
std::vector<Eigen::Quaternion<FloatT>> incremental_quat_seq_1(std::size_t num_points)
{
    static const char Sequence_base[72][2]
    {
        { 6, 0}, { 6, 3}, { 6, 1}, { 6, 4}, { 6, 2}, { 6, 5},
        { 4, 0}, { 4, 3}, { 4, 1}, { 4, 4}, { 4, 2}, { 4, 5},
        { 1, 0}, { 1, 3}, { 1, 1}, { 1, 4}, { 1, 2}, { 1, 5},

        {11, 0}, {11, 3}, {11, 1}, {11, 4}, {11, 2}, {11, 5},
        { 9, 0}, { 9, 3}, { 9, 1}, { 9, 4}, { 9, 2}, { 9, 5},
        { 3, 0}, { 3, 3}, { 3, 1}, { 3, 4}, { 3, 2}, { 3, 5},

        { 5, 0}, { 5, 3}, { 5, 1}, { 5, 4}, { 5, 2}, { 5, 5},
        { 7, 0}, { 7, 3}, { 7, 1}, { 7, 4}, { 7, 2}, { 7, 5},
        {10, 0}, {10, 3}, {10, 1}, {10, 4}, {10, 2}, {10, 5},

        { 0, 0}, { 0, 3}, { 0, 1}, { 0, 4}, { 0, 2}, { 0, 5},
        { 2, 0}, { 2, 3}, { 2, 1}, { 2, 4}, { 2, 2}, { 2, 5},
        { 8, 0}, { 8, 3}, { 8, 1}, { 8, 4}, { 8, 2}, { 8, 5}
    };

    std::vector<Eigen::Quaternion<FloatT>> result;
    result.reserve(num_points);

    const auto map_to_angle = [&](auto v)
    {
        switch (v) //mapping index on S1 to its angle value
        {
        // *INDENT-OFF*
        case 0: return 30;      //30 + 0
        case 1: return 90;      //30 + 60
        case 2: return 150;     //30 + 120
        case 3: return 210;     //30 + 180
        case 4: return 270;     //30 + 240
        case 5: return 330;     //30 + 300
            // *INDENT-ON*
        }
        throw "ERROR";
    };

    const auto& store_hopf = [&result](FloatT theta, FloatT phi, FloatT psi)
    {
        // *INDENT-OFF*
        const auto qw = static_cast<FloatT>(std::sin(theta / 2) * std::sin(phi + psi / 2));
        const auto qx = static_cast<FloatT>(std::cos(theta / 2) * std::cos(      psi / 2));
        const auto qy = static_cast<FloatT>(std::cos(theta / 2) * std::sin(      psi / 2));
        const auto qz = static_cast<FloatT>(std::sin(theta / 2) * std::cos(phi + psi / 2));
        result.emplace_back(qw, qx, qy, qz);
        // *INDENT-ON*
    };

    for (std::size_t i = 0; i < std::min(num_points, 72ul); i++)
    {
        //const long int cur_point = i / 72;
        const auto [base_0, base_1] = Sequence_base[i];
        const double point_S1 = map_to_angle(base_1); //mapping index on S1 to its angle value

        const auto [theta, phi] = detail::generate_quat_seq::pix2ang_nest(1, base_0);
        const double psi = point_S1 * M_PI / 180;
        store_hopf(theta, phi, psi);
    }

    if (num_points <= 72)
    {
        return result;
    }

    for (std::size_t i = 0; i < num_points - 72; i++) //this will only be called if points are more than 72.
    {
        const long int base_grid = i % 72;
        const long int cur_point = i / 72;
        const auto& base_seq_val = Sequence_base[base_grid];
        const double point_S1 = map_to_angle(base_seq_val[1]); //mapping index on S1 to its angle value

        const long int point_healpix = 4 * base_seq_val[0];

        const auto [theta, phi, psi] = detail::generate_quat_seq::find_point(base_seq_val[0], cur_point, 1, point_healpix, point_S1);
        //current point value,level,current point in healpix, current point for S1
        store_hopf(theta, phi, psi);
    }

    return result;
}

template <class FloatT = double>
std::vector<Eigen::Quaternion<FloatT>> incremental_quat_seq_2(std::size_t num_points)
{
    static const std::uint16_t Sequence_base[72][2]
    {
        { 6, 30}, { 6, 210}, { 6, 90}, { 6, 270}, { 6, 150}, { 6, 330},
        { 4, 30}, { 4, 210}, { 4, 90}, { 4, 270}, { 4, 150}, { 4, 330},
        { 1, 30}, { 1, 210}, { 1, 90}, { 1, 270}, { 1, 150}, { 1, 330},

        {11, 30}, {11, 210}, {11, 90}, {11, 270}, {11, 150}, {11, 330},
        { 9, 30}, { 9, 210}, { 9, 90}, { 9, 270}, { 9, 150}, { 9, 330},
        { 3, 30}, { 3, 210}, { 3, 90}, { 3, 270}, { 3, 150}, { 3, 330},

        { 5, 30}, { 5, 210}, { 5, 90}, { 5, 270}, { 5, 150}, { 5, 330},
        { 7, 30}, { 7, 210}, { 7, 90}, { 7, 270}, { 7, 150}, { 7, 330},
        {10, 30}, {10, 210}, {10, 90}, {10, 270}, {10, 150}, {10, 330},

        { 0, 30}, { 0, 210}, { 0, 90}, { 0, 270}, { 0, 150}, { 0, 330},
        { 2, 30}, { 2, 210}, { 2, 90}, { 2, 270}, { 2, 150}, { 2, 330},
        { 8, 30}, { 8, 210}, { 8, 90}, { 8, 270}, { 8, 150}, { 8, 330}
    };

    std::vector<Eigen::Quaternion<FloatT>> result;
    result.reserve(num_points);

    const auto& store_hopf = [&result](FloatT theta, FloatT phi, FloatT psi)
    {
        // *INDENT-OFF*
        const auto qw = static_cast<FloatT>(std::sin(theta / 2) * std::sin(phi + psi / 2));
        const auto qx = static_cast<FloatT>(std::cos(theta / 2) * std::cos(      psi / 2));
        const auto qy = static_cast<FloatT>(std::cos(theta / 2) * std::sin(      psi / 2));
        const auto qz = static_cast<FloatT>(std::sin(theta / 2) * std::cos(phi + psi / 2));
        result.emplace_back(qw, qx, qy, qz);
        // *INDENT-ON*
    };

    for (std::size_t i = 0; i < num_points; ++i) //this will only be called if points are more than 72.
    {
        if (i < 72)
        {
            const auto [base_0, point_S1] = Sequence_base[i];

            const auto [theta, phi] = detail::generate_quat_seq::pix2ang_nest(1, base_0);
            const double psi = point_S1 * M_PI / 180;
            store_hopf(theta, phi, psi);
        }
        else
        {
            const auto [base_0, point_S1] = Sequence_base[i % 72];

            const long int point_healpix = 4 * base_0;

            const auto [theta, phi, psi] = detail::generate_quat_seq::find_point(base_0, i / 72 - 1, 1, point_healpix, point_S1);
            //current point value,level,current point in healpix, current point for S1
            store_hopf(theta, phi, psi);
        }
    }
    return result;
}

template <class FloatT = double>
Eigen::Quaternion<FloatT> incremental_quat_seq_at(std::size_t i)
{
    static const std::uint16_t Sequence_base[72][2]
    {
        { 6, 30}, { 6, 210}, { 6, 90}, { 6, 270}, { 6, 150}, { 6, 330},
        { 4, 30}, { 4, 210}, { 4, 90}, { 4, 270}, { 4, 150}, { 4, 330},
        { 1, 30}, { 1, 210}, { 1, 90}, { 1, 270}, { 1, 150}, { 1, 330},

        {11, 30}, {11, 210}, {11, 90}, {11, 270}, {11, 150}, {11, 330},
        { 9, 30}, { 9, 210}, { 9, 90}, { 9, 270}, { 9, 150}, { 9, 330},
        { 3, 30}, { 3, 210}, { 3, 90}, { 3, 270}, { 3, 150}, { 3, 330},

        { 5, 30}, { 5, 210}, { 5, 90}, { 5, 270}, { 5, 150}, { 5, 330},
        { 7, 30}, { 7, 210}, { 7, 90}, { 7, 270}, { 7, 150}, { 7, 330},
        {10, 30}, {10, 210}, {10, 90}, {10, 270}, {10, 150}, {10, 330},

        { 0, 30}, { 0, 210}, { 0, 90}, { 0, 270}, { 0, 150}, { 0, 330},
        { 2, 30}, { 2, 210}, { 2, 90}, { 2, 270}, { 2, 150}, { 2, 330},
        { 8, 30}, { 8, 210}, { 8, 90}, { 8, 270}, { 8, 150}, { 8, 330}
    };

    const auto& store_hopf = [](FloatT theta, FloatT phi, FloatT psi)
    {
        // *INDENT-OFF*
        const auto qw = static_cast<FloatT>(std::sin(theta / 2) * std::sin(phi + psi / 2));
        const auto qx = static_cast<FloatT>(std::cos(theta / 2) * std::cos(      psi / 2));
        const auto qy = static_cast<FloatT>(std::cos(theta / 2) * std::sin(      psi / 2));
        const auto qz = static_cast<FloatT>(std::sin(theta / 2) * std::cos(phi + psi / 2));
        return Eigen::Quaternion<FloatT>{qw, qx, qy, qz};
        // *INDENT-ON*
    };

    if (i < 72)
    {
        const auto [base_0, point_S1] = Sequence_base[i];

        const auto [theta, phi] = detail::generate_quat_seq::pix2ang_nest(1, base_0);
        const double psi = point_S1 * M_PI / 180;
        return store_hopf(theta, phi, psi);
    }

    const auto [base_0, point_S1] = Sequence_base[i % 72];
    const long int point_healpix = 4 * base_0;
    const auto [theta, phi, psi] = detail::generate_quat_seq::find_point(base_0, i / 72 - 1, 1, point_healpix, point_S1);
    //current point value,level,current point in healpix, current point for S1
    return store_hopf(theta, phi, psi);
}

template <class FloatT = double>
std::vector<Eigen::Quaternion<FloatT>> incremental_quat_seq_3(std::size_t num_points)
{
    std::vector<Eigen::Quaternion<FloatT>> result;
    result.reserve(num_points);
    for (std::size_t i = 0; i < num_points; ++i)
    {
        result.emplace_back(incremental_quat_seq_at(i));
    }
    return result;
}

template <class FloatT = double>
Eigen::Quaternion<FloatT> incremental_quat_seq_at_2(std::size_t i)
{
    static constexpr std::uint8_t  base_0s[12] = { 6, 4, 1, 11, 9, 3, 5, 7, 10, 0, 2, 8};
    static constexpr std::uint16_t point_S1s[6] = {30, 210, 90, 270, 150, 330};
    static constexpr auto hopf_to_quat = [](FloatT theta, FloatT phi, FloatT psi)
    {
        // *INDENT-OFF*
        const auto qw = static_cast<FloatT>(std::sin(theta / 2) * std::sin(phi + psi / 2));
        const auto qx = static_cast<FloatT>(std::cos(theta / 2) * std::cos(      psi / 2));
        const auto qy = static_cast<FloatT>(std::cos(theta / 2) * std::sin(      psi / 2));
        const auto qz = static_cast<FloatT>(std::sin(theta / 2) * std::cos(phi + psi / 2));
        return Eigen::Quaternion<FloatT>{qw, qx, qy, qz};
        // *INDENT-ON*
    };

    const auto point_S1 = point_S1s[i % 6];
    const auto base_0   = base_0s[(i % 72) / 6];

    if (i < 72)
    {
        const auto [theta, phi] = detail::generate_quat_seq::pix2ang_nest(1, base_0);
        const double psi = point_S1 * M_PI / 180;
        return hopf_to_quat(theta, phi, psi);
    }

    const auto [theta, phi, psi] = detail::generate_quat_seq::find_point(base_0, i / 72 - 1, 1, 4 * base_0, point_S1);
    //current point value,level,current point in healpix, current point for S1
    return hopf_to_quat(theta, phi, psi);
}

template <class FloatT = double>
std::vector<Eigen::Quaternion<FloatT>> incremental_quat_seq_4(std::size_t num_points)
{
    std::vector<Eigen::Quaternion<FloatT>> result;
    result.reserve(num_points);
    for (std::size_t i = 0; i < num_points; ++i)
    {
        result.emplace_back(incremental_quat_seq_at_2(i));
    }
    return result;
}

#include <utilitypp/geometry.hpp>

template <class FloatT = double>
std::vector<Eigen::Quaternion<FloatT>> incremental_quat_seq_upp(std::size_t num_points)
{
    std::vector<Eigen::Quaternion<FloatT>> result;
    result.reserve(num_points);
    for (std::size_t i = 0; i < num_points; ++i)
    {
        result.emplace_back(upp::equidistant_orientation_sequence::at<FloatT, Eigen::Quaternion<FloatT>>(i));
    }
    return result;
}

#include <benchmark/benchmark.h>


// ////////////////////////////////////////////////////////////////////////// //
// ///////////////////////////////// gen seq //////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
#define benchmark_seq_version(v)                                                \
    void gen_seq_##v(benchmark::State& state)                                   \
    {                                                                           \
        for (auto _ : state)                                                    \
        {                                                                       \
            benchmark::DoNotOptimize(incremental_quat_seq_##v(state.range(0))); \
        }                                                                       \
    }                                                                           \
    BENCHMARK(gen_seq_##v)->RangeMultiplier(128ul)->Range(1ul, 1ul << 20)

benchmark_seq_version(1);
benchmark_seq_version(2);
benchmark_seq_version(3);
benchmark_seq_version(4);
benchmark_seq_version(upp);
// ////////////////////////////////////////////////////////////////////////// //
// ///////////////////////////////// gen seq //////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
#include <random>

static std::mt19937 gen{std::random_device{}()};
static std::uniform_int_distribution<std::size_t> d{0, 8192 * 72};

void seq_at(benchmark::State& state)
{
    for (auto _ : state)
    {
        benchmark::DoNotOptimize(incremental_quat_seq_at(d(gen)));
    }
}
BENCHMARK(seq_at);

void seq_at_2(benchmark::State& state)
{
    for (auto _ : state)
    {
        benchmark::DoNotOptimize(incremental_quat_seq_at_2(d(gen)));
    }
}
BENCHMARK(seq_at_2);

void seq_at_upp(benchmark::State& state)
{
    for (auto _ : state)
    {
        benchmark::DoNotOptimize(upp::equidistant_orientation_sequence::at(d(gen)));
    }
}
BENCHMARK(seq_at_upp);
// ////////////////////////////////////////////////////////////////////////// //
// ////////////////////////////////// test ////////////////////////////////// //
// ////////////////////////////////////////////////////////////////////////// //
#include <thread>
void test(benchmark::State& state)
{
    const auto cmp = [](const auto & v1, const auto & v2, auto name)
    {
        if (v1.size() != v2.size())
        {
            throw std::logic_error{"wrong size "};
        }

        for (std::size_t i = 0; i < v1.size(); ++i)
        {
            if (
                std::abs(v1.at(i).w() - v2.at(i).w()) > 1e-8 ||
                std::abs(v1.at(i).x() - v2.at(i).x()) > 1e-8 ||
                std::abs(v1.at(i).y() - v2.at(i).y()) > 1e-8 ||
                std::abs(v1.at(i).z() - v2.at(i).z()) > 1e-8
            )
            {
                throw std::logic_error{"fail " + std::to_string(i) + ": " + name};
            }
        }
    };

    const auto v1 = incremental_quat_seq_1(1ul << 20);
    cmp(v1, incremental_quat_seq_2(1ul << 20), "incremental_quat_seq_2");
    cmp(v1, incremental_quat_seq_3(1ul << 20), "incremental_quat_seq_3");
    cmp(v1, incremental_quat_seq_4(1ul << 20), "incremental_quat_seq_4");
    cmp(v1, incremental_quat_seq_upp(1ul << 20), "incremental_quat_seq_upp");

    for (auto _ : state)
        ;
}
BENCHMARK(test);
// ////////////////////////////////////////////////////////////////////////// //
BENCHMARK_MAIN();
