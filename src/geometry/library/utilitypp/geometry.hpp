#pragma once

// This file is generated!

#include "geometry/bounding_sphere.hpp"
#include "geometry/dense_pose_grid.hpp"
#include "geometry/equidistant_orientation_sequence.hpp"
#include "geometry/oriented_box.hpp"
