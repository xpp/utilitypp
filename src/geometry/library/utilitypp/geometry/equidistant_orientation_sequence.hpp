#pragma once

#include <cmath>
#include <cstdint>
#include <array>

namespace upp::detail::geometry
{
    std::array<double, 2> pix2ang_nest(int nside, int ipix);
    std::array<double, 3> find_point(int base_grid, long int point, long int level, long int healpix_point, double s1_point);
}

namespace upp::equidistant_orientation_sequence
{
    template <class FloatT = double, class ReturnT = std::array<FloatT, 4>>
    ReturnT at(std::size_t i)
    {
        using namespace upp::detail::geometry;
        static constexpr std::uint8_t  base_0s[12] = { 6, 4, 1, 11, 9, 3, 5, 7, 10, 0, 2, 8};
        static constexpr std::uint16_t point_S1s[6] = {30, 210, 90, 270, 150, 330};
        static constexpr auto hopf_to_quat = [](FloatT theta, FloatT phi, FloatT psi) ->ReturnT
        {
            // *INDENT-OFF*
            const auto qw = static_cast<FloatT>(std::sin(theta / 2) * std::sin(phi + psi / 2));
            const auto qx = static_cast<FloatT>(std::cos(theta / 2) * std::cos(      psi / 2));
            const auto qy = static_cast<FloatT>(std::cos(theta / 2) * std::sin(      psi / 2));
            const auto qz = static_cast<FloatT>(std::sin(theta / 2) * std::cos(phi + psi / 2));
            return {qw, qx, qy, qz};
            // *INDENT-ON*
        };

        const auto point_S1 = point_S1s[i % 6];
        const auto base_0   = base_0s[(i % 72) / 6];

        if (i < 72)
        {
            const auto [theta, phi] = pix2ang_nest(1, base_0);
            const double psi = point_S1 * M_PI / 180;
            return hopf_to_quat(theta, phi, psi);
        }

        const auto [theta, phi, psi] = find_point(base_0, i / 72 - 1, 1, 4 * base_0, point_S1);
        //current point value,level,current point in healpix, current point for S1
        return hopf_to_quat(theta, phi, psi);
    }
}
