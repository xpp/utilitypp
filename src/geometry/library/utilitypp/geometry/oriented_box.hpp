#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace upp
{
    template <
        class ScalarT,
        int Dim     = 3,
        int Mode    = Eigen::AffineCompact,
        int Options = 0
        >
    class oriented_box
    {
    public:
        using float_t = ScalarT;
        static constexpr int dimension = Dim;
        using transform_t = Eigen::Transform<float_t, dimension, Mode, Options>;
        using vector_t = Eigen::Matrix<float_t, dimension, 1, Options>;
        using rotation_t = Eigen::Matrix<float_t, dimension, dimension, Options>;
        using homogeneous_t = Eigen::Matrix < float_t, dimension + 1, dimension + 1, Options >;

    public:
        static constexpr float_t eps = static_cast<float_t>(1e8);
        static constexpr float_t pi  = static_cast<float_t>(M_PI);

    public:
        oriented_box(oriented_box&&) = default;
        oriented_box(const oriented_box&) = default;
        oriented_box& operator=(oriented_box&&) = default;
        oriented_box& operator=(const oriented_box&) = default;

        oriented_box(
            const vector_t& corner,
            const vector_t& extend0,
            const vector_t& extend1,
            const vector_t& extend2);

        const vector_t& dimensions() const;
        const transform_t& transformation() const;
        transform_t transformation_centered() const;

        auto translation() const;
        auto rotation() const;

        auto axis_x() const;
        auto axis_y() const;
        auto axis_z() const;

        float_t volume() const;

        void scale(float_t factor);
        void scale_centered(float_t factor);

        vector_t from_box_frame(const vector_t& p) const;
        vector_t to_box_frame(const vector_t& p) const;

        bool contains(const vector_t& p);

        vector_t center() const;
    private:
        transform_t _t;
        vector_t _d;
    };

    using oriented_box_3f = oriented_box<float, 3>;
    using oriented_box_4f = oriented_box<float, 4>;

    using oriented_box_3d = oriented_box<double, 3>;
    using oriented_box_4d = oriented_box<double, 4>;
}

#include "oriented_box.ipp"
