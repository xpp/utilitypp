#include <utilitypp/exception/throw_if.hpp>

#include "equidistant_orientation_sequence.hpp"

namespace upp::detail::geometry
{
    inline void mk_pix2xy(int* pix2x, int* pix2y)
    {
        /* =======================================================================
             * subroutine mk_pix2xy
             * =======================================================================
             * constructs the array giving x and y in the face from pixel number
             * for the nested (quad-cube like) ordering of pixels
             *
             * the bits corresponding to x and y are interleaved in the pixel number
             * one breaks up the pixel number by even and odd bits
             * =======================================================================
             */

        int i, kpix, jpix, IX, IY, IP, ID;
        for (i = 0; i < 1023; i++)
        {
            pix2x[i] = 0;
        }

        for (kpix = 0; kpix < 1024; kpix++)
        {
            jpix = kpix;
            IX = 0;
            IY = 0;
            IP = 1 ;//              ! bit position (in x and y)
            while (jpix != 0) // ! go through all the bits
            {
                ID = static_cast<int>(std::fmod(jpix, 2)); //  ! bit value (in kpix), goes in ix
                jpix = jpix / 2;
                IX = ID * IP + IX;

                ID = static_cast<int>(std::fmod(jpix, 2)); //  ! bit value (in kpix), goes in iy
                jpix = jpix / 2;
                IY = ID * IP + IY;
                IP = 2 * IP; //         ! next bit (in x and y)
            }
            pix2x[kpix] = IX;//     ! in 0,31
            pix2y[kpix] = IY;//     ! in 0,31
        }
        return;
    }

    std::array<double, 2> pix2ang_nest(int nside, int ipix)
    {

        /*
              c=======================================================================
              subroutine pix2ang_nest(nside, ipix, theta, phi)
              c=======================================================================
              c     gives theta and phi corresponding to pixel ipix (NESTED)
              c     for a parameter nside
              c=======================================================================
            */

        static constexpr double piover2 = 0.5 * M_PI;
        static constexpr int ns_max = 8192;

        static int pix2x[1024], pix2y[1024];
        //      common /pix2xy/ pix2x, pix2y

        // ! coordinate of the lowest corner of each face
        static constexpr int jrll[12] = {2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4};
        static constexpr int jpll[12] = {1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7};
        //      data jrll/2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4/ ! in unit of nside
        //      data jpll/1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7/ ! in unit of nside/2

        upp_throw_if(std::invalid_argument, nside < 1 || nside > ns_max)
                << "nside out of range [1, " << ns_max << ']';
        const int npix = 12 * nside * nside;
        upp_throw_if(std::invalid_argument, ipix < 0 || ipix > npix - 1)
                << "ipix out of range [0," << npix - 1 << ']';

        /* initiates the array for the pixel number -> (x,y) mapping */
        if (pix2x[1023] <= 0)
        {
            mk_pix2xy(pix2x, pix2y);
        }

        const double fn = 1.*nside;
        const double fact1 = 1. / (3.*fn * fn);
        const double fact2 = 2. / (3.*fn);
        const int nl4   = 4 * nside;

        //c     finds the face, and the number in the face
        const int npface = nside * nside;

        const int face_num = ipix / npface; //  ! face number in {0,11}
        const int ipf = static_cast<int>(std::fmod(ipix, npface)); //  ! pixel number in the face {0,npface-1}

        //c     finds the x,y on the face (starting from the lowest corner)
        //c     from the pixel number
        const int ip_low = static_cast<int>(std::fmod(ipf, 1024)); //       ! content of the last 10 bits
        const int ip_trunc =   ipf / 1024 ; //       ! truncation of the last 10 bits
        const int ip_med = static_cast<int>(std::fmod(ip_trunc, 1024)); //  ! content of the next 10 bits
        const int ip_hi  =     ip_trunc / 1024   ; //! content of the high weight 10 bits

        const int ix = 1024 * pix2x[ip_hi] + 32 * pix2x[ip_med] + pix2x[ip_low];
        const int iy = 1024 * pix2y[ip_hi] + 32 * pix2y[ip_med] + pix2y[ip_low];

        //c     transforms this in (horizontal, vertical) coordinates
        const int jrt = ix + iy;//  ! 'vertical' in {0,2*(nside-1)}
        const int jpt = ix - iy;//  ! 'horizontal' in {-nside+1,nside-1}

        //c     computes the z coordinate on the sphere
        int jr =  jrll[face_num] * nside - jrt - 1;
        int nr = nside;//                  ! equatorial region (the most frequent)
        double z  = (2 * nside - jr) * fact2;

        int kshift = static_cast<int>(std::fmod(jr - nside, 2));
        if (jr < nside)  //then     ! north pole region
        {
            nr = jr;
            z = 1. - nr * nr * fact1;
            kshift = 0;
        }
        else
        {
            if (jr > 3 * nside) // then ! south pole region
            {
                nr = nl4 - jr;
                z = - 1. + nr * nr * fact1;
                kshift = 0;
            }
        }
        const double theta = std::acos(z);

        //c     computes the phi coordinate on the sphere, in [0,2Pi]
        //      jp = (jpll[face_num+1]*nr + jpt + 1 + kshift)/2;//  ! 'phi' number in the ring in {1,4*nr}
        int jp = (jpll[face_num] * nr + jpt + 1 + kshift) / 2;
        if (jp > nl4)
        {
            jp = jp - nl4;
        }
        if (jp < 1)
        {
            jp = jp + nl4;
        }

        const double phi = (jp - (kshift + 1) * 0.5) * (piover2 / nr);
        return {theta, phi};
    }

    std::array<double, 3> find_point(int base_grid, long int point, long int level, long int healpix_point, double s1_point)
    {
        int position = point % 8;
        long int quo = 0;
        double interval = 30 / level;
        // the choosing of the order of the first resolution 4 points depends on which base healpix grid we are now dividing

        if (base_grid == 6 or base_grid == 7)
        {
            switch (position) //this position tells which of the eight points of the cube to consider
            {
                case 0:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;

            }
        }
        else if (base_grid == 3 or base_grid == 1 or base_grid == 9 or base_grid == 11)
        {
            switch (position)
            {
                case 0:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;

            }
        }
        else if (base_grid == 2 or base_grid == 0 or base_grid == 10 or base_grid == 8)
        {
            switch (position)
            {
                case 0:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;

            }
        }
        else if (base_grid == 4 or base_grid == 5)
        {
            switch (position)
            {
                case 0:
                    healpix_point += 0;
                    s1_point -= (interval / 2);
                    break;
                case 1:
                    healpix_point += 3;
                    s1_point += (interval / 2);
                    break;
                case 2:
                    healpix_point += 0;
                    s1_point += (interval / 2);
                    break;
                case 3:
                    healpix_point += 3;
                    s1_point -= (interval / 2);
                    break;
                case 4:
                    healpix_point += 2;
                    s1_point -= (interval / 2);
                    break;
                case 5:
                    healpix_point += 1;
                    s1_point += (interval / 2);
                    break;
                case 6:
                    healpix_point += 2;
                    s1_point += (interval / 2);
                    break;
                case 7:
                    healpix_point += 1;
                    s1_point -= (interval / 2);
                    break;

            }
        }

        quo = point / 8;
        if (quo == 0)
        {
            long int nside = pow(2, level);
            const auto [theta, phi] = pix2ang_nest(nside, healpix_point);
            const double psi = s1_point * M_PI / 180;
            return {theta, phi, psi};
        }
        return find_point(base_grid, quo - 1, level + 1, 4 * healpix_point, s1_point);
    }
}
