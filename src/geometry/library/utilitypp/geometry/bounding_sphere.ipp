#pragma once

#include <utilitypp/exception/throw_if.hpp>

#include "bounding_sphere.hpp"

namespace upp
{
    template<class S, int D, int O> inline
    bounding_sphere<S, D, O>::bounding_sphere(
        const vector_t& center,
        float_t squared_radius
    ):
        _center{center}, _squared_radius{squared_radius}
    {
        upp_throw_if_less(std::invalid_argument, squared_radius, 0)
                << "squared_radius has to be >= 0! (squared_radius = "
                << squared_radius << ")";
    }


    template<class S, int D, int O> inline
    S bounding_sphere<S, D, O>::squared_radius() const
    {
        return _squared_radius;
    }
    template<class S, int D, int O> inline
    const typename bounding_sphere<S, D, O>::vector_t& bounding_sphere<S, D, O>::center() const
    {
        return _center;
    }

    template<class S, int D, int O> inline
    bounding_side bounding_sphere<S, D, O>::test(vector_t& p) const
    {
        const auto squared_distance = (_center - p).squaredNorm();
        if (squared_distance < _squared_radius)
        {
            return bounding_side::bounded;
        }
        if (squared_distance > _squared_radius)
        {
            return bounding_side::unbounded;
        }
        return bounding_side::boundary;
    }
}
