#pragma once

#include <random>

#include <boost/math/constants/constants.hpp>
#include <boost/iterator/iterator_facade.hpp>

#include <Eigen/Geometry>

namespace upp::detail::_dense_pose_grid
{
    using size_t = std::uint64_t;
    using pos_idx_t = Eigen::Matrix<size_t, 3, 1>;

    template <class FloatT> using box_t  = Eigen::AlignedBox<FloatT, 3>;
    template <class FloatT> using ori_t  = Eigen::Quaternion<FloatT>;
    template <class FloatT> using pos_t  = Eigen::Matrix    <FloatT, 3, 1>;
    template <class FloatT> using pose_t = Eigen::Transform <FloatT, 3, Eigen::AffineCompact>;

    enum class ori_owning
    {
        owning,
        referencing
    };

    //    static constexpr size_t samples_per_cell = 6;
}

namespace upp
{
    template <
        class FloatT,
        template<class...> class OriContainerTemplate,
        detail::_dense_pose_grid::ori_owning OwningMode
        >
    class dense_pose_grid;

    template <class ParamsT>
    class dense_pose_grid_iterator;
}

namespace upp::detail::_dense_pose_grid
{
    template <
        class FloatT,
        template<class...> class OriContainerTemplate,
        detail::_dense_pose_grid::ori_owning OwningMode
        >
    struct template_params
    {
        static const auto owning_mode = OwningMode;

        using template_params_t = template_params<FloatT, OriContainerTemplate, OwningMode>;

        using float_t             = FloatT;
        using size_t              = _dense_pose_grid::size_t;
        using pos_idx_t           = _dense_pose_grid::pos_idx_t;
        using box_t               = _dense_pose_grid::box_t    <float_t>;
        using ori_t               = _dense_pose_grid::ori_t    <float_t>;
        using pos_t               = _dense_pose_grid::pos_t    <float_t>;
        using pose_t              = _dense_pose_grid::pose_t   <float_t>;
        using distribution_t      = std::uniform_real_distribution    <float_t>;

        using iterator_t          = dense_pose_grid_iterator           <template_params_t>;
        using grid_t              = dense_pose_grid<FloatT, OriContainerTemplate, OwningMode>;

        using ori_container_t     = OriContainerTemplate<ori_t>;
        using ori_container_ref_t = std::conditional_t <
                                    owning_mode == ori_owning::owning,
                                    ori_container_t,
                                    const ori_container_t*
                                    >;
    };
}

namespace upp
{
    template <class ParamsT>
    class dense_pose_grid_iterator : public boost::iterator_facade <
        dense_pose_grid_iterator<ParamsT>,
        typename ParamsT::pose_t,
        boost::forward_traversal_tag,
        typename ParamsT::pose_t
        >
    {
    private:
        static constexpr float inf = std::numeric_limits<float>::infinity();
        static constexpr float pi = boost::math::constants::pi<float>();
    public:
        using iterator_t          = dense_pose_grid_iterator<ParamsT>;

        using float_t             = typename ParamsT::float_t            ;
        using ori_container_t     = typename ParamsT::ori_container_t    ;
        using size_t              = typename ParamsT::size_t             ;
        using pos_idx_t           = typename ParamsT::pos_idx_t          ;
        using box_t               = typename ParamsT::box_t              ;
        using ori_t               = typename ParamsT::ori_t              ;
        using pos_t               = typename ParamsT::pos_t              ;
        using pose_t              = typename ParamsT::pose_t             ;
        using distribution_t      = typename ParamsT::distribution_t     ;

        using grid_t              = typename ParamsT::grid_t             ;
        using ori_container_ref_t = typename ParamsT::ori_container_ref_t;

    public:
        dense_pose_grid_iterator(const grid_t& grid, size_t idx);

        dense_pose_grid_iterator()                                           = default;
        dense_pose_grid_iterator(dense_pose_grid_iterator&&)                 = default;
        dense_pose_grid_iterator(const dense_pose_grid_iterator&)            = default;

        dense_pose_grid_iterator& operator=(dense_pose_grid_iterator&&)      = default;
        dense_pose_grid_iterator& operator=(const dense_pose_grid_iterator&) = default;

        const pos_t& position() const;
        ori_t orientation() const;
        pose_t pose() const;

        pos_idx_t position_index() const;
        size_t orientation_index() const;
        size_t pose_index() const;

        void skip_cell();

        //boost::iterator_facade
    private:
        friend class boost::iterator_core_access;
        void increment();
        bool equal(const dense_pose_grid_iterator& other) const;
        pose_t dereference() const;

    private:
        void set(size_t idx);

    private:
        const grid_t*  _grid;
        distribution_t _pd;
        distribution_t _po{0, std::nextafter(1.f, inf)};
        size_t         _idx;
        pos_t          _pos;
        ori_t          _ori_offset;
        std::mt19937   _gen;
        bool           _skip_cell = false;
    };
}

#include "iterator.ipp"
