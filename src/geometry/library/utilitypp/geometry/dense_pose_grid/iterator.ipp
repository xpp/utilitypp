#pragma once

#include <utilitypp/exception/throw_if.hpp>

#include "grid.hpp"
#include "iterator.hpp"

namespace upp
{
    template <class T> inline
    dense_pose_grid_iterator<T>::dense_pose_grid_iterator(const grid_t& grid, size_t idx) :
        _grid{&grid},
        _pd{-grid.cell_size() / 2, grid.cell_size() / 2}
    {
        set(idx);
    }

    template <class T> inline
    const typename dense_pose_grid_iterator<T>::pos_t&
    dense_pose_grid_iterator<T>::position() const
    {
        return _pos;
    }
    template <class T> inline
    typename dense_pose_grid_iterator<T>::ori_t
    dense_pose_grid_iterator<T>::orientation() const
    {
        return  _ori_offset * _grid->orientation(orientation_index());
    }
    template <class T> inline
    typename dense_pose_grid_iterator<T>::pose_t
    dense_pose_grid_iterator<T>::pose() const
    {
        pose_t r;
        r.linear() = orientation().toRotationMatrix();
        r.translation() = position();
        return r;
    }

    template <class T> inline
    typename dense_pose_grid_iterator<T>::size_t
    dense_pose_grid_iterator<T>::orientation_index() const
    {
        return _grid->pose_to_orientation_index(_idx);
    }
    template <class T> inline
    typename dense_pose_grid_iterator<T>::size_t
    dense_pose_grid_iterator<T>::pose_index() const
    {
        return _idx;
    }
    template <class T> inline
    typename dense_pose_grid_iterator<T>::pos_idx_t
    dense_pose_grid_iterator<T>::position_index() const
    {
        return _grid->pose_to_position_index(_idx);
    }

    template <class T> inline
    void
    dense_pose_grid_iterator<T>::skip_cell()
    {
        _skip_cell = true;
    }

    template <class T> inline
    void
    dense_pose_grid_iterator<T>::increment()
    {
        if (_skip_cell)
        {
            _skip_cell = false;
            _idx = _grid->pose_index_to_cell_end(_idx) - 1;
        }
        if (
            const auto snew = _grid->pose_index_to_seed(_idx + 1);
            _grid->pose_index_to_seed(_idx) != snew
        )
        {
            _gen.seed(snew);
            _pd.reset();
            _po.reset();
        }
        //pos
        {
            _pos(0) = _pd(_gen);
            _pos(1) = _pd(_gen);
            _pos(2) = _pd(_gen);
        }
        //ori
        {
            const auto x0 = _po(_gen);
            const auto x1 = _po(_gen);
            const auto x2 = _po(_gen);
            const auto t1 = 2 * pi * x1;
            const auto t2 = 2 * pi * x2;
            const auto s1 = std::sin(t1);
            const auto s2 = std::sin(t2);
            const auto c1 = std::cos(t1);
            const auto c2 = std::cos(t2);
            const auto r1 = std::sqrt(1 - x0);
            const auto r2 = std::sqrt(x0);

            _ori_offset.x() = s1 * r1;
            _ori_offset.y() = c1 * r1;
            _ori_offset.z() = s2 * r2;
            _ori_offset.w() = c2 * r2;
        }
        ++_idx;
    }

    template <class T> inline
    bool
    dense_pose_grid_iterator<T>::equal(const dense_pose_grid_iterator& other) const
    {
        upp_throw_if_not_equal(std::logic_error, _grid, other._grid)
                << "both iterators must refer to the same grid";
        return _idx == other._idx;
    }

    template <class T> inline
    typename dense_pose_grid_iterator<T>::pose_t
    dense_pose_grid_iterator<T>::dereference() const
    {
        return pose();
    }

    template <class T> inline
    void
    dense_pose_grid_iterator<T>::set(size_t idx)
    {
        _idx = _grid->pose_index_to_cell_begin(idx) - 1;
        while (_idx != idx)
        {
            increment();
        }
    }
}
