#pragma once

#include "iterator.hpp"

#include "grid.hpp"

namespace upp
{
    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    dense_pose_grid<F, T, O>::dense_pose_grid(ori_container_ref_t oris,
            const box_t& aabb,
            float_t cell_size,
            size_t seed,
            size_t cells_per_seed
                                             ) :
        _oris{std::move(oris)},
        _aabb{aabb},
        //_num_cells{pos_t{_aabb.sizes() / cell_size}.eval().cast<size_t>() + pos_idx_t::Ones()},
        //_num_poses{_num_cells(0) * _num_cells(1) * _num_cells(2) * _oris.size()},
        _cell_size{cell_size},
        _seed{seed},
        _num_cells_per_seed{cells_per_seed}
    {
        if constexpr(owning_mode != detail::_dense_pose_grid::ori_owning::owning)
        {
            upp_throw_if_null(std::invalid_argument, _oris);
        }
        _num_cells(0) = _aabb.sizes()(0) / cell_size + 1;
        _num_cells(1) = _aabb.sizes()(1) / cell_size + 1;
        _num_cells(2) = _aabb.sizes()(2) / cell_size + 1;
        _num_poses = _num_cells(0) * _num_cells(1) * _num_cells(2) * orientations().size();
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::iterator_t
    dense_pose_grid<F, T, O>::begin() const
    {
        return {*this, 0};
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::iterator_t
    dense_pose_grid<F, T, O>::end()
    {
        return {*this, _num_poses};
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::size() const
    {
        return _num_poses;
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::number_of_poses() const
    {
        return _num_poses;
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::number_of_orientations() const
    {
        return orientations().size();
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::pos_idx_t
    dense_pose_grid<F, T, O>::number_of_cells() const
    {
        return _num_cells;
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::ori_t const&
    dense_pose_grid<F, T, O>::orientation(size_t i) const
    {
        return orientations().at(i);
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    float
    dense_pose_grid<F, T, O>::cell_size() const
    {
        return _cell_size;
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::box_t const&
    dense_pose_grid<F, T, O>::aabb() const
    {
        return _aabb;
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::pose_to_orientation_index(size_t idx) const
    {
        return idx % number_of_orientations();
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::pose_index_to_cell_begin(size_t idx) const
    {
        return idx - pose_to_orientation_index(idx);
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::pose_index_to_cell_end(size_t idx) const
    {
        return idx - pose_to_orientation_index(idx);
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::pos_idx_t
    dense_pose_grid<F, T, O>::pose_to_position_index(size_t idx) const
    {
        idx /= number_of_orientations();
        const auto ix = idx % _num_cells(0);
        idx /= _num_cells(0);
        const auto iy = idx % _num_cells(1);
        idx /= _num_cells(1);
        const auto iz = idx % _num_cells(2);
        return {ix, iy, iz};
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::pose_to_cell_index(size_t pose_idx) const
    {
        return pose_idx / number_of_orientations();
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    typename dense_pose_grid<F, T, O>::size_t
    dense_pose_grid<F, T, O>::pose_index_to_seed(size_t pose_idx) const
    {
        return _seed + pose_to_cell_index(pose_idx) / _num_cells_per_seed;
    }

    template <class F, template<class...> class T, detail::_dense_pose_grid::ori_owning O>
    inline
    const dense_pose_grid<F, T, O>::ori_container_t&
    dense_pose_grid<F, T, O>::orientations() const
    {
        if constexpr(owning_mode == detail::_dense_pose_grid::ori_owning::owning)
        {
            return _oris;
        }
        else
        {
            return *_oris;
        }
    }
}
