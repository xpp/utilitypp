#pragma once

#include <random>

#include "iterator.hpp"

namespace upp
{
    template <
        class FloatT = float,
        template<class...> class OriContainerTemplate = std::vector,
        detail::_dense_pose_grid::ori_owning OwningMode = detail::_dense_pose_grid::ori_owning::owning
        >
    class dense_pose_grid
    {
    public:
        static const auto owning_mode = OwningMode;

        using params_t = detail::_dense_pose_grid::template_params<FloatT, OriContainerTemplate, OwningMode>;

        using float_t             = typename params_t::float_t            ;
        using ori_container_t     = typename params_t::ori_container_t    ;
        using size_t              = typename params_t::size_t             ;
        using pos_idx_t           = typename params_t::pos_idx_t          ;
        using box_t               = typename params_t::box_t              ;
        using ori_t               = typename params_t::ori_t              ;
        using pos_t               = typename params_t::pos_t              ;
        using pose_t              = typename params_t::pose_t             ;
        using distribution_t      = typename params_t::distribution_t     ;

        using iterator_t          = typename params_t::iterator_t         ;
        using grid_t              = typename params_t::grid_t             ;
        using ori_container_ref_t = typename params_t::ori_container_ref_t;
    private:
        friend class dense_pose_grid_iterator<params_t>;
    public:
        dense_pose_grid(
            ori_container_ref_t oris,
            const box_t& aabb,
            float_t cell_size = 2.5f,
        size_t seed = std::random_device{}(),
        size_t cells_per_seed = 1024);

        iterator_t begin() const;
        iterator_t end();

        size_t size() const;
        size_t number_of_poses() const;
        size_t number_of_orientations() const;

        pos_idx_t number_of_cells() const;
        const ori_t& orientation(size_t i) const;

        float cell_size() const;
        const box_t& aabb() const;

        const ori_container_t& orientations() const;

    private:
        size_t    pose_to_orientation_index(size_t idx) const;
        size_t    pose_index_to_cell_begin(size_t idx) const;
        size_t    pose_index_to_cell_end(size_t idx) const;
        pos_idx_t pose_to_position_index(size_t idx) const;
        size_t    pose_to_cell_index(size_t pose_idx) const;
        size_t    pose_index_to_seed(size_t pose_idx) const;
    private:
        //static data

        ori_container_ref_t _oris;
        box_t               _aabb;
        pos_idx_t           _num_cells;
        size_t              _num_poses;
        float_t             _cell_size;
        size_t              _seed;
        size_t              _num_cells_per_seed;
    };

///TODO
//    template<class Cont, class FloatT>
//    dense_pose_grid(
//        const Cont& oris,
//        const detail::_dense_pose_grid::box_t<FloatT>& aabb,
//        FloatT cell_size,
//        detail::_dense_pose_grid::size_t seed,
//        detail::_dense_pose_grid::size_t cells_per_seed
//    ) -> dense_pose_grid<FloatT, Cont, detail::_dense_pose_grid::ori_owning::owning>;

//    template<class Cont, class FloatT>
//    dense_pose_grid(
//        const Cont* oris,
//        const detail::_dense_pose_grid::box_t<FloatT>& aabb,
//        FloatT cell_size,
//        detail::_dense_pose_grid::size_t seed,
//        detail::_dense_pose_grid::size_t cells_per_seed
//    ) -> dense_pose_grid<FloatT, Cont, detail::_dense_pose_grid::ori_owning::referencing>;

    using dense_pose_grid_f = dense_pose_grid<float>;
    using dense_pose_grid_d = dense_pose_grid<double>;
}

#include "grid.ipp"
