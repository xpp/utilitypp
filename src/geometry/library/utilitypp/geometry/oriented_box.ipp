#pragma once

#include <utilitypp/exception/throw_if.hpp>

#include "oriented_box.hpp"

namespace upp
{

    template<class S, int D, int M, int O> inline
    oriented_box<S, D, M, O>::oriented_box(
        const vector_t& corner,
        const vector_t& extend0,
        const vector_t& extend1,
        const vector_t& extend2
    )
    {
        const float_t len0 = extend0.norm();
        const float_t len1 = extend1.norm();
        const float_t len2 = extend2.norm();
        const vector_t normalized0 = extend0 / len0;
        const vector_t normalized1 = extend1 / len1;
        const vector_t normalized2 = extend2 / len2;

        const float_t dot01 = normalized0.dot(normalized1);
        const float_t dot02 = normalized0.dot(normalized2);
        const float_t dot12 = normalized0.dot(normalized1);

        const float_t angle01 = std::acos(dot01) * 180 / pi;
        const float_t angle02 = std::acos(dot02) * 180 / pi;
        const float_t angle12 = std::acos(dot12) * 180 / pi;

        //checks
        upp_throw_if_greater(std::invalid_argument, std::abs(angle01), eps)
                << "extend0 and extend1 are not perpendicular! (angle = "
                << angle01 << "°)";
        upp_throw_if_greater(std::invalid_argument, std::abs(angle02), eps)
                << "extend0 and extend2 are not perpendicular! (angle = "
                << angle02 << "°)";
        upp_throw_if_greater(std::invalid_argument, std::abs(angle12), eps)
                << "extend1 and extend2 are not perpendicular! (angle = "
                << angle12 << "°)";

        //build transform
        homogeneous_t hom = homogeneous_t::Identity();
        hom.template block<3, 1>(0, 0) = normalized0;
        _d(0) = len0;

        const vector_t cross01 = normalized0.cross(normalized1);
        const float_t direction_match = cross01.dot(normalized2);

        if (direction_match > 0)
        {
            hom.template block<3, 1>(0, 1) = normalized1;
            hom.template block<3, 1>(0, 2) = normalized2;
            _d(1) = len1;
            _d(2) = len2;
        }
        else
        {
            hom.template block<3, 1>(0, 1) = normalized2;
            hom.template block<3, 1>(0, 2) = normalized1;
            _d(1) = len2;
            _d(2) = len1;
        }
        hom.template block<3, 1>(0, 3) = corner;
        _t = transform_t{hom};
    }


    template<class S, int D, int M, int O> inline
    const oriented_box<S, D, M, O>::vector_t&
    oriented_box<S, D, M, O>::dimensions() const
    {
        return _d;
    }
    template<class S, int D, int M, int O> inline
    const oriented_box<S, D, M, O>::transform_t&
    oriented_box<S, D, M, O>::transformation() const
    {
        return _t;
    }
    template<class S, int D, int M, int O> inline
    oriented_box<S, D, M, O>::transform_t
    oriented_box<S, D, M, O>::transformation_centered() const
    {
        transform_t tc = _t;
        tc.translation() = center();
        return tc;
    }

    template<class S, int D, int M, int O> inline
    auto
    oriented_box<S, D, M, O>::translation() const
    {
        return _t.translation();
    }
    template<class S, int D, int M, int O> inline
    auto
    oriented_box<S, D, M, O>::rotation() const
    {
        return _t.linear();
    }

    template<class S, int D, int M, int O> inline
    auto
    oriented_box<S, D, M, O>::axis_x() const
    {
        return _t.template block<3, 1>(0, 0);
    }
    template<class S, int D, int M, int O> inline
    auto
    oriented_box<S, D, M, O>::axis_y() const
    {
        return _t.template block<3, 1>(0, 1);
    }
    template<class S, int D, int M, int O> inline
    auto
    oriented_box<S, D, M, O>::axis_z() const
    {
        return _t.template block<3, 1>(0, 2);
    }

    template<class S, int D, int M, int O> inline
    oriented_box<S, D, M, O>::float_t
    oriented_box<S, D, M, O>::volume() const
    {
        return _d(0) * _d(1) * _d(2);
    }

    template<class S, int D, int M, int O> inline
    void
    oriented_box<S, D, M, O>::scale(float_t factor)
    {
        _d *= factor;
    }
    template<class S, int D, int M, int O> inline
    void
    oriented_box<S, D, M, O>::scale_centered(float_t factor)
    {
        _t.translation() += rotation() * _d * (1 - factor) / 2;
        _d *= factor;
    }

    template<class S, int D, int M, int O> inline
    oriented_box<S, D, M, O>::vector_t
    oriented_box<S, D, M, O>::from_box_frame(const vector_t& p) const
    {
        return _t.translation() + rotation() * p;
    }

    template<class S, int D, int M, int O> inline
    oriented_box<S, D, M, O>::vector_t
    oriented_box<S, D, M, O>::to_box_frame(const vector_t& p) const
    {
        return -rotation().transpose() * _t.translation() + rotation().transpose() * p;
    }

    template<class S, int D, int M, int O> inline
    bool
    oriented_box<S, D, M, O>::contains(const vector_t& p)
    {
        const vector_t b = to_box_frame(p);
        return (_d(0) < 0 ? b(0) >= _d(0) : b(0) <= _d(0)) &&
               (_d(1) < 0 ? b(1) >= _d(1) : b(1) <= _d(1)) &&
               (_d(2) < 0 ? b(2) >= _d(2) : b(2) <= _d(2));
    }

    template<class S, int D, int M, int O> inline
    oriented_box<S, D, M, O>::vector_t
    oriented_box<S, D, M, O>::center() const
    {
        return from_box_frame(_d / 2);
    }
}
