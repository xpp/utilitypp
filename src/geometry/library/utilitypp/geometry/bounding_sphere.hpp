#pragma once

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace upp
{
    enum class bounding_side
    {
        bounded,
        boundary,
        unbounded
    };

    template <
        class ScalarT,
        int Dim     = 3,
        int Options = 0
        >
    class bounding_sphere
    {
    public:
        using float_t = ScalarT;
        static constexpr int dimension = Dim;
        using vector_t = Eigen::Matrix<float_t, dimension, 1, Options>;
    public:
        bounding_sphere(bounding_sphere&&)                 = default;
        bounding_sphere(const bounding_sphere&)            = default;
        bounding_sphere& operator=(bounding_sphere&&)      = default;
        bounding_sphere& operator=(const bounding_sphere&) = default;
        bounding_sphere(const vector_t& center = {0, 0, 0},
                        float_t squared_radius = 0);

        float_t squared_radius() const;
        const vector_t& center() const;
        bounding_side test(vector_t& p) const;
    private:
        vector_t _center;
        float_t _squared_radius;
    };

    using bounding_sphere_3f = bounding_sphere<float, 3>;
    using bounding_sphere_4f = bounding_sphere<float, 4>;

    using bounding_sphere_3d = bounding_sphere<double, 3>;
    using bounding_sphere_4d = bounding_sphere<double, 4>;
}

#include "bounding_sphere.ipp"
