#define BOOST_TEST_MODULE geometry
#include <random>
#include <deque>
#include <iostream>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/geometry/dense_pose_grid.hpp>

template<template<class...> class Temp>
void check(std::mt19937& gen)
{
    const Temp<Eigen::Quaterniond> oris
    {
        Eigen::Quaterniond::UnitRandom(), Eigen::Quaterniond::UnitRandom(),
        Eigen::Quaterniond::UnitRandom(), Eigen::Quaterniond::UnitRandom(),
        Eigen::Quaterniond::UnitRandom(), Eigen::Quaterniond::UnitRandom(),
        Eigen::Quaterniond::UnitRandom(), Eigen::Quaterniond::UnitRandom()
    };
    const Eigen::Vector3d shift = Eigen::Vector3d::Random() * 100;
    const Eigen::AlignedBox3d box {shift, shift + 3 * Eigen::Vector3d::Random().cwiseAbs()};
    const double csz = 0.5;
    const std::size_t seed = gen();
    const size_t cps = 2;
    upp::dense_pose_grid<double, Temp, upp::detail::_dense_pose_grid::ori_owning::owning>
            pg_1{ oris, box, csz, seed, cps};
    ///TODO
    upp::dense_pose_grid<double, Temp, upp::detail::_dense_pose_grid::ori_owning::referencing>
            pg_2{&oris, box, csz, seed, cps};

    static_assert((sizeof(pg_1) - sizeof(pg_2)) == (sizeof(oris) - sizeof(&oris)));

    BOOST_REQUIRE_EQUAL(pg_1.number_of_orientations(), oris.size());
    BOOST_REQUIRE_EQUAL(pg_1.cell_size(), csz);

    BOOST_REQUIRE_EQUAL(pg_1.size(), pg_2.size());
    BOOST_REQUIRE_EQUAL(pg_1.number_of_poses(), pg_2.number_of_poses());
    BOOST_REQUIRE_EQUAL(pg_1.number_of_orientations(), pg_2.number_of_orientations());
    BOOST_REQUIRE_EQUAL(pg_1.number_of_cells(), pg_2.number_of_cells());
    BOOST_REQUIRE_EQUAL(pg_1.cell_size(), pg_2.cell_size());
    BOOST_REQUIRE(pg_1.aabb().min() == pg_2.aabb().min());
    BOOST_REQUIRE(pg_1.aabb().max() == pg_2.aabb().max());

    const std::deque<upp::dense_pose_grid_d::pose_t> poses{pg_1.begin(), pg_1.end()};
    BOOST_REQUIRE_EQUAL(poses.size(), pg_2.size());

    std::cout << "#poses " << poses.size() << std::endl;

    auto it_1 = pg_1.begin();
    auto it_2 = pg_2.begin();
    const auto end_1 = pg_1.end();
    const auto end_2 = pg_2.end();
    while (
        it_1 != end_1 &&
        it_2 != end_2
    )
    {
        BOOST_REQUIRE_EQUAL(it_1.pose_index(), it_2.pose_index());
        const auto idx = it_1.pose_index();
        BOOST_REQUIRE(it_1->matrix() == poses.at(idx).matrix());
        BOOST_REQUIRE(it_2->matrix() == poses.at(idx).matrix());
        if (! gen() % 3)
        {
            it_1.skip_cell();
            it_2.skip_cell();
        }
        else
        {
            ++it_1;
            ++it_2;
        }
    }
    BOOST_REQUIRE(it_1 == end_1);
    BOOST_REQUIRE(it_2 == end_2);
}

BOOST_AUTO_TEST_CASE(dense_pose_grid_test)
{
    std::mt19937 gen{std::random_device{}()};

    for (int i = 0; i < 100; ++i)
    {
        check<std::vector>(gen);
        check<std::deque>(gen);
    }
}
