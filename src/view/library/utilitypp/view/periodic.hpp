#pragma once

#include <utilitypp/concepts/indexed_container.hpp>

namespace upp
{
    template<class T>
    class periodic_container_view
    {
        using ref_t = typename T::reference;
        using cref_t = typename T::const_reference;
    public:
        using container_t = T;

        using value_type  = typename container_t::value_type;
        using value_t     = value_type;

        using const_reference_t = cref_t;
        using reference_t = std::conditional_t<std::is_const_v<T>, cref_t, ref_t>;

        using size_t = typename container_t::size_type;

        periodic_container_view(container_t& cont) :
            _cont{&cont}
        {}

        size_t size() const
        {
            return container().size();
        }

        container_t& container()
        {
            return *_cont;
        }
        const container_t& container() const
        {
            return *_cont;
        }

        reference_t at(size_t i)
        {
            return container().at(i % size());
        }
        const_reference_t at(size_t i) const
        {
            return container().at(i % size());
        }
    private:
        container_t* _cont{nullptr};
    };
//    template<class T> periodic_container_view(T&) -> periodic_container_view<T>;
}
