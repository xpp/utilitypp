#define BOOST_TEST_MODULE view
#include <vector>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/view/periodic.hpp>


BOOST_AUTO_TEST_CASE(periodic_vector_bool)
{
    std::vector v{false, true};
    upp::periodic_container_view pview{v};
    BOOST_CHECK_EQUAL(pview.at(0), false);
    BOOST_CHECK_EQUAL(pview.at(1), true);
    BOOST_CHECK_EQUAL(pview.at(2), false);
    BOOST_CHECK_EQUAL(pview.at(3), true);
    pview.at(2) = true;
    pview.at(3) = false;
    BOOST_CHECK_EQUAL(v.at(0), true);
    BOOST_CHECK_EQUAL(v.at(1), false);
    BOOST_CHECK_EQUAL(v.at(2), true);
    BOOST_CHECK_EQUAL(v.at(3), false);
}

BOOST_AUTO_TEST_CASE(periodic_const_vector_int)
{
    const std::vector v{0, 1};
    upp::periodic_container_view pview{v};
    BOOST_CHECK_EQUAL(pview.at(10), 0);
    BOOST_CHECK_EQUAL(pview.at(11), 1);
}
