#define BOOST_TEST_MODULE distribution
#include <sstream>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/distribution/hyper_cube.hpp>

static constexpr std::size_t test_n = 100;

BOOST_AUTO_TEST_CASE(hyper_cube)
{
    std::mt19937 gen{std::random_device{}()};
    const std::array<float, 2> center {1, 2};
    const float side_length = 2;
    upp::uniform_hyper_cube_distribution<float, 2> d{center, side_length};

    BOOST_CHECK(center == d.center());
    BOOST_CHECK_LE(std::abs(side_length - d.side_length()), 0);

    for (std::size_t i = 0; i < test_n; ++i)
    {
        const auto s = d(gen);
        BOOST_CHECK_GE(s.at(0), 0);
        BOOST_CHECK_GE(2, s.at(0));
        BOOST_CHECK_GE(s.at(1), 1);
        BOOST_CHECK_GE(3, s.at(1));
    }

    decltype(d)::param_type param{center, 4};
    for (std::size_t i = 0; i < test_n; ++i)
    {
        const auto s = d(gen, param);
        BOOST_CHECK_GE(s.at(0), -1);
        BOOST_CHECK_GE(3, s.at(0));
        BOOST_CHECK_GE(s.at(1), 0);
        BOOST_CHECK_GE(4, s.at(1));
    }
}
