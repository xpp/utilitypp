#define BOOST_TEST_MODULE distribution
#include <sstream>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/distribution/hyper_sphere_surface.hpp>

static constexpr std::size_t test_n = 100;

BOOST_AUTO_TEST_CASE(hyper_sphere_surface)
{
    std::mt19937 gen{std::random_device{}()};
    const float radius = 1;
    upp::uniform_hyper_sphere_surface_distribution<float, 3> d{radius};

    BOOST_CHECK_LE(std::abs(radius - d.radius()), 0);

    for (std::size_t i = 0; i < test_n; ++i)
    {
        const auto s = d(gen);
        BOOST_CHECK_LE(radius - std::hypot(s.at(0), s.at(1), s.at(2)), 1e-5);
    }

    decltype(d)::param_type param{2};
    for (std::size_t i = 0; i < test_n; ++i)
    {
        const auto s = d(gen, param);
        BOOST_CHECK_LE(2 - std::hypot(s.at(0), s.at(1), s.at(2)), 1e-5);
    }
}
