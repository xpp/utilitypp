#define BOOST_TEST_MODULE distribution
#include <sstream>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/distribution/hyper_cuboid.hpp>

static constexpr std::size_t test_n = 100;

BOOST_AUTO_TEST_CASE(hyper_cuboid)
{

    std::mt19937 gen{std::random_device{}()};
    const std::array<std::array<float, 2>, 2> bounds = {{{0, 1}, {2, 3}}};
    upp::uniform_hyper_cuboid_distribution<float, 2> d{{{{0, 1}, {2, 3}}}};
    BOOST_CHECK(bounds == d.bounds());

    for (std::size_t i = 0; i < test_n; ++i)
    {
        const auto s = d(gen);
        BOOST_CHECK_GE(s.at(0), 0);
        BOOST_CHECK_GE(1, s.at(0));
        BOOST_CHECK_GE(s.at(1), 2);
        BOOST_CHECK_GE(3, s.at(1));
    }

    decltype(d)::param_type param {{{{10.f, 11.f}, {12.f, 13.f}}}};
    for (std::size_t i = 0; i < test_n; ++i)
    {
        const auto s = d(gen, param);
        BOOST_CHECK_GE(s.at(0), 10);
        BOOST_CHECK_GE(11, s.at(0));
        BOOST_CHECK_GE(s.at(1), 12);
        BOOST_CHECK_GE(13, s.at(1));
    }
}

