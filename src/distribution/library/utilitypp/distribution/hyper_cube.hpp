#pragma once

#include <random>
#include <array>
#include <cmath>

#include <utilitypp/exception/throw_if.hpp>
#include <utilitypp/generate_code/default_ctor_and_assign.hpp>

#include "detail/default_implementation_template.hpp"

namespace upp::detail::uniform_hyper_cube_distribution
{
    namespace uppdetail = upp::detail::uniform_hyper_cube_distribution;
    template<class, std::size_t> struct state_type;

    template<class RealType, std::size_t N>
    using distribution_type = ::upp::detail::distribution_base<state_type<RealType, N>>;

    template<class RealType, std::size_t N>
    struct param_type
    {
        //types
        using distribution_type = uppdetail::distribution_type<RealType, N>;
        //ctor
        upp_default_cexpr_copy_move(param_type);
        param_type(RealType side_length = std::numeric_limits<RealType>::max()) :
            _side_length{side_length}
        {
            _center.fill(0);
            upp_throw_if_negative(std::invalid_argument, _side_length);
        }

        param_type(RealType lo, RealType hi) :
            _side_length{hi - lo}
        {
            upp_throw_if_greater(std::invalid_argument, lo, hi);
            upp_throw_if_negative(std::invalid_argument, _side_length);
            _center.fill((lo + hi) / 2);
        }
        param_type(RealType side_length, const std::array<RealType, N>& c) :
            _side_length{side_length}, _center{c}
        {
            upp_throw_if_negative(std::invalid_argument, _side_length);
        }
        param_type(const std::array<RealType, N>& c, RealType side_length) :
            _side_length{side_length}, _center{c}
        {
            upp_throw_if_negative(std::invalid_argument, _side_length);
        }
        //access
        RealType side_length() const
        {
            return _side_length;
        }
        const std::array<RealType, N>& center() const
        {
            return _center;
        }

        //compare
        upp_operator_in_class_eq_ieq(param_type, _side_length, _center);
        //data
    private:
        RealType _side_length = 1;
        std::array<RealType, N> _center;
    };

    template<class RealType, std::size_t N>
    struct state_type
    {
        using param_type = uppdetail::param_type<RealType, N>;
        using result_type = std::array<RealType, N>;

        using scalar_distribution_type = std::uniform_real_distribution<RealType>;
        using scalar_distribution_param_type = typename scalar_distribution_type::param_type;

        void param(const param_type& p)
        {
            static constexpr auto inf = std::numeric_limits<RealType>::infinity();
            const RealType r = p.side_length() / 2;
            _d.param(scalar_distribution_param_type{-r, std::nextafter(r, inf)});
        }

        void reset()
        {
            _d.reset();
        }

        result_type operator()(auto& gen, const param_type& p)
        {
            result_type result = p.center();
            for (auto& e : result)
            {
                e += _d(gen);
            }
            return result;
        }

    private:
        scalar_distribution_type _d;
    };
}

namespace upp
{
    template<class RealType, std::size_t N = 3>
    using uniform_hyper_cube_distribution = ::upp::detail::uniform_hyper_cube_distribution::distribution_type<RealType, N>;
}
