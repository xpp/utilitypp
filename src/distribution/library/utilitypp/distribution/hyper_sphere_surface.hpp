#pragma once

#include <random>
#include <array>

#include <utilitypp/exception/throw_if.hpp>
#include <utilitypp/generate_code/default_ctor_and_assign.hpp>

#include "detail/default_implementation_template.hpp"

namespace upp::detail::uniform_hyper_sphere_surface_distribution
{
    namespace uppdetail = upp::detail::uniform_hyper_sphere_surface_distribution;
    template<class, class> struct state_type;

    template<class RealType, class ResultT>
    using distribution_type = ::upp::detail::distribution_base<state_type<RealType, ResultT>>;

    template<class RealType, class ResultT>
    struct param_type
    {
        //types
        using distribution_type = uppdetail::distribution_type<RealType, ResultT>;
        //ctor
        upp_default_cexpr_copy_move(param_type);
        param_type(RealType radius = 1) :
            _radius{radius}
        {
            upp_throw_if_negative(std::invalid_argument, _radius);
        }
        //access
        const RealType radius() const
        {
            return _radius;
        }

        //compare
        upp_operator_in_class_eq_ieq(param_type, _radius);
        //data
    private:
        RealType _radius = 1;
    };

    template<class RealType, class ResultT>
    struct state_type
    {
        using param_type = uppdetail::param_type<RealType, ResultT>;
        using result_type = ResultT;

        void param(const param_type&) {}

        void reset()
        {
            _norm_dist.reset();
        }

        result_type operator()(auto& gen, const param_type& p)
        {
            result_type result;
            RealType length = 0;
            for (auto& e : result)
            {
                e = _norm_dist(gen);
                length += std::pow(e, 2.0);
            }

            length = std::sqrt(length) / p.radius();

            for (auto& e : result)
            {
                e /= length;
            }
            return result;
        }
    private:
        std::normal_distribution<RealType> _norm_dist{0.0, 0.1};
    };
}

namespace upp
{
    /**
     * @brief Uniform distribution over the surface of an n dimensional unit sphere.
     *
     * (using the method described in doi>10.1145/377939.377946 (https://dl.acm.org/citation.cfm?doid=377939.377946))
     */
    template<class RealType, std::size_t N = 3, class ResultT = std::array<RealType, N>>
    using uniform_hyper_sphere_surface_distribution = ::upp::detail::uniform_hyper_sphere_surface_distribution::distribution_type<RealType, ResultT>;
}
