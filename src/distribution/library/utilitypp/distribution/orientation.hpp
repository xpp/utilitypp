#pragma once

#include <random>
#include <array>

#include <boost/math/constants/constants.hpp>

#include <utilitypp/exception/throw_if.hpp>
#include <utilitypp/generate_code/default_ctor_and_assign.hpp>

#include "detail/default_implementation_template.hpp"

namespace upp::detail::uniform_orientation_distribution
{
    namespace uppdetail = upp::detail::uniform_orientation_distribution;

    template<class RealType, class ResultT>
    struct state_type;

    template<class RealType, class ResultT>
    using distribution_type = ::upp::detail::distribution_base<state_type<RealType, ResultT>>;

    template<class RealType, class ResultT>
    struct param_type
    {
        using distribution_type = uppdetail::distribution_type<RealType, ResultT>;
        upp_default_cexpr_copy_move(param_type);
    };

    template<class RealType, class ResultT>
    struct state_type
    {
    private:
        static constexpr float inf = std::numeric_limits<float>::infinity();
        static constexpr float pi = boost::math::constants::pi<float>();
    public:
        using param_type = uppdetail::param_type<RealType, ResultT>;
        using result_type = ResultT;

        void param(const param_type&) {}

        void reset()
        {
            _d.reset();
        }

        result_type operator()(auto& gen, const param_type& p)
        {
            const auto x0 = d(gen);
            const auto x1 = d(gen);
            const auto x2 = d(gen);
            const auto t1 = 2 * pi * x1;
            const auto t2 = 2 * pi * x2;
            const auto s1 = std::sin(t1);
            const auto s2 = std::sin(t2);
            const auto c1 = std::cos(t1);
            const auto c2 = std::cos(t2);
            const auto r1 = std::sqrt(1 - x0);
            const auto r2 = std::sqrt(x0);

            const auto qx = s1 * r1;
            const auto qy = c1 * r1;
            const auto qz = s2 * r2;
            const auto qw = c2 * r2;
            return {qw, qx, qy, qz};
        }
    private:
        std::uniform_real_distribution<float> _d{0, std::nextafter(1.f, inf)};
    };
}

namespace upp
{
    /**
     * @brief Uniform distribution over SO(3).
     * The output are quaternions (w, x, y, z).
     *
     * http://planning.cs.uiuc.edu/node198.html
     * x,y,z elem [0,1]
     * quat =
     *  std::sqrt(1-x)*std::sin(2*pi*y)
     *  std::sqrt(1-x)*std::cos(2*pi*y)
     *  std::sqrt(  x)*std::sin(2*pi*z)
     *  std::sqrt(  x)*std::cos(2*pi*z)
     *
     * is adaption from:
     * https://doc.lagout.org/Others/Game%20Development/Programming/Graphics%20Gems%203.pdf 129 ff
     * x0, x1, x2 elem [0,1]
     * t1 = 2 * pi * x1
     * t2 = 2 * pi * x2
     * sin/cos of t1/t2: s1 s2 c1 c2
     * r1 = std::sqrt(1-x0)
     * r2 = std::sqrt(x0)
     *  quat(x, y, z, w) = s1*r1, c1*r1, s2*r2, c2*r2
     */
    template<class RealType, class ResultT>
    using uniform_orientation_distribution = ::upp::detail::uniform_orientation_distribution::distribution_type<RealType, ResultT>;
}
