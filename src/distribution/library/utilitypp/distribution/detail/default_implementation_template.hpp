#pragma once

#include <utilitypp/generate_code/comparison_operators.hpp>

namespace upp::detail
{
    template<class StateType>
    class distribution_base : public StateType::param_type
    {
        // ///////////////////////////////////////////////////////////////////////////// //
        // /////////////////////// implementation specific types /////////////////////// //
        // ///////////////////////////////////////////////////////////////////////////// //
    protected:
        using state_type = StateType;
    public:
        using result_type = typename state_type::result_type;
        using param_type = typename state_type::param_type;
        // ///////////////////////////////////////////////////////////////////////////// //
        // //////////////////////////////// constructors /////////////////////////////// //
        // ///////////////////////////////////////////////////////////////////////////// //
    public:
        using param_type::param_type;
        //copies all ctors from param type (clso default and init via param_type)
        template<class...Args, class = std::enable_if_t<std::is_constructible_v<param_type, Args...>>>
                 distribution_base(Args && ... args) :
                     param_type(std::forward<Args>(args)...)
        {
            _state.param(param());
        }

        //        distribution_base(const param_type& param) :
        //                                                     param_type(param)
        //        {
        //            _state.param(param());
        //        }
        //        distribution_base(param_type&& param) :
        //                                                param_type(std::move(param))
        //        {
        //            _state.param(param());
        //        }

        distribution_base(const distribution_base&)  = default;
        distribution_base(distribution_base&&) = default;
        // ///////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////////// operators ///////////////////////////////// //
        // ///////////////////////////////////////////////////////////////////////////// //
    public:
        //assign
        distribution_base& operator=(const distribution_base&) = default;
        distribution_base& operator=(distribution_base&&) = default;
        //compare
        using param_type::operator==;
        using param_type::operator!=;
        // ///////////////////////////////////////////////////////////////////////////// //
        // ///////////////////////////// DEFAULT FUNCTIONS ///////////////////////////// //
        // ///////////////////////////////////////////////////////////////////////////// //
    public:
        const param_type& param() const
        {
            return *this;
        }
        void param(const param_type& p)
        {
            *static_cast<param_type*>(this) = p;
            reset();
        }
        void param(param_type p)
        {
            *static_cast<param_type*>(this) = std::move(p);
            reset();
        }
        void reset()
        {
            _state.reset(param());
        }
        // ///////////////////////////////////////////////////////////////////////////// //
        // /////////////////////// DEFAULT: generate via operator ////////////////////// //
        // ///////////////////////////////////////////////////////////////////////////// //
        // one (return)
        result_type operator()(auto& gen)
        {
            return _state(gen, param());
        }
        result_type operator()(auto& gen, const param_type& params) const
        {
            state_type state;
            state.param(params);
            return state(gen, params);
        }
        // ///////////////////////////////////////////////////////////////////////////// //
        // one (output)
        result_type& operator()(auto& gen, result_type& out)
        {
            return (out = _state(gen, param()));
        }
        result_type& operator()(auto& gen, const param_type& params, result_type& out) const
        {
            state_type state;
            state.param(params);
            return (out = state(gen, params));
        }
        // ///////////////////////////////////////////////////////////////////////////// //
        // n (first + #)
        template<class Iterator>
        Iterator operator()(auto& gen, Iterator first_out, std::size_t n)
        {
            for (std::size_t i = 0; i < n; ++i, ++first_out)
            {
                *first_out = _state(gen, param());
            }
            return first_out;
        }
        template<class Iterator>
        Iterator operator()(auto& gen, const param_type& params, Iterator first_out, std::size_t n) const
        {
            state_type state;
            state.param(params);
            for (std::size_t i = 0; i < n; ++i, ++first_out)
            {
                *first_out = state(gen, params);
            }
            return first_out;
        }
        // ///////////////////////////////////////////////////////////////////////////// //
        // n (first + last)
        template<class Iterator>
        Iterator operator()(auto& gen, Iterator first_out, Iterator last_out)
        {
            for (; first_out != last_out; ++first_out)
            {
                *first_out = _state(gen, param());
            }
            return first_out;
        }
        template<class Iterator>
        Iterator operator()(auto& gen, const param_type& params, Iterator first_out, Iterator last_out) const
        {
            state_type state;
            state.param(params);
            for (; first_out != last_out; ++first_out)
            {
                *first_out = state(gen, params);
            }
            return first_out;
        }
        // ///////////////////////////////////////////////////////////////////////////// //
        // /////////////////////////////// DEFAULT: data /////////////////////////////// //
        // ///////////////////////////////////////////////////////////////////////////// //
    private:
        state_type _state;
    };
}
