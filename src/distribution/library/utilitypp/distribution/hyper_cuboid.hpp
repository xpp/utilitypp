#pragma once

#include <random>
#include <array>
#include <cmath>

#include <utilitypp/array/array_from_value.hpp>
#include <utilitypp/exception/throw_if.hpp>
#include <utilitypp/generate_code/comparison_operators.hpp>
#include <utilitypp/generate_code/default_ctor_and_assign.hpp>

#include "detail/default_implementation_template.hpp"

namespace upp::detail::uniform_hyper_cuboid_distribution
{
    namespace uppdetail = upp::detail::uniform_hyper_cuboid_distribution;
    template<class, std::size_t, class> struct state_type;

    template<class RealType, std::size_t N, class ResultT>
    using distribution_type = ::upp::detail::distribution_base<state_type<RealType, N, ResultT>>;

    template<class RealType, std::size_t N, class ResultT>
    struct param_type
    {
        //common
        using distribution_type = uppdetail::distribution_type<RealType, N, ResultT>;
        using result_type = ResultT;
        upp_default_cexpr_copy_move(param_type);

        //specific
        param_type(const std::array<std::array<RealType, 2>, N>& bounds) :
            _bounds{bounds}
        {
            for (const auto& b : _bounds)
            {
                upp_throw_if_greater(std::invalid_argument, b.at(0), b.at(1));
            }
        }

        const std::array<std::array<RealType, 2>, N>& bounds() const
        {
            return _bounds;
        }

        //compare
        upp_operator_in_class_eq_ieq(param_type, _bounds);
        //data
    private:
        std::array<std::array<RealType, 2>, N> _bounds;
    };


    template<class RealType, std::size_t N, class ResultT>
    struct state_type
    {
        using param_type = uppdetail::param_type<RealType, N, ResultT>;
        using result_type = param_type::result_type;

        using scalar_distribution_type = std::uniform_real_distribution<RealType>;
        using scalar_distribution_param_type = typename scalar_distribution_type::param_type;


        state_type& param(const param_type& p)
        {
            for (std::size_t i = 0; i < N; ++i)
            {
                _ds.at(i).param(scalar_distribution_param_type
                {
                    p.bounds().at(i).at(0),
                    std::nextafter(p.bounds().at(i).at(1), std::numeric_limits<RealType>::max())
                });
            }
            return *this;
        }

        void reset()
        {
            for (auto& d : _ds)
            {
                d.reset();
            }
        }

        result_type operator()(auto& gen, const param_type&)
        {
            result_type result;
            for (std::size_t i = 0; i < N; ++i)
            {
                auto& d = _ds.at(i);
                result.at(i) = d(gen);
            }
            return result;
        }

    private:
        std::array<scalar_distribution_type, N> _ds;
    };
}

namespace upp
{
    template<class RealType, std::size_t N = 3, class ResultT = std::array<RealType, N>>
    using uniform_hyper_cuboid_distribution = detail::uniform_hyper_cuboid_distribution::distribution_type<RealType, N, ResultT>;
}
