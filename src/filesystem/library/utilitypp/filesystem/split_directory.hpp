#pragma once

#include <deque>
#include <filesystem>

namespace upp::fs
{
    namespace fs = std::filesystem;

    class split_directory
    {
    public:
        split_directory(
            fs::path root,
            std::uintmax_t number_of_levels = 4,
            std::uintmax_t elements_per_level = 256,
            bool reuse = false
        );

        void remove_empty_subdirectories();

        std::deque<fs::path> unmanaged_directories_and_files();
        std::deque<fs::path> managed_directories();

        const fs::path& root_absolute() const;
        const fs::path& root() const;
        std::uintmax_t elements_per_level() const;
        std::uintmax_t number_of_levels() const;
        std::uintmax_t maximal_number_of_elements() const;

        std::uintmax_t index_from_path(const fs::path& p, bool allow_partial_paths = false);
        fs::path path_from_index(std::uintmax_t i);

        bool is_managed_directory(const fs::path& p, bool allow_partial_paths = false);
        bool is_valid_directory_path(const fs::path& p, bool allow_partial_paths = false);
        bool is_valid_directory_name(const std::string& name);
    private:
        std::string to_hex(std::uintmax_t i);
        template<class F>
        void collect_directories(std::uintmax_t lvl, const fs::path& p, std::deque<fs::path>& result, const F& pred);
    private:
        const fs::path _root;
        const fs::path _root_abs;
        const std::uintmax_t _elements_per_level;
        const std::uintmax_t _number_of_levels;
        const std::uintmax_t _maximal_number_of_elements;
        const std::uintmax_t _level_name_length;
        const bool _reuse = false;
        //        std::uintmax_t _hint;
    };
}

#include "split_directory.ipp"
#include "detail/namespace.hpp"
