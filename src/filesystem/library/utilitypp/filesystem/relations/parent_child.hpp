#pragma once

#include <filesystem>

namespace upp::fs
{
    namespace fs = std::filesystem;

    bool is_parent_directory_of(const fs::path& p, const fs::path& c);

    inline bool is_child_directory_of(const fs::path& c, const fs::path& p)
    {
        return is_parent_directory_of(p, c);
    }

    bool is_parent_directory_of_already_cannonical(const fs::path& p, const fs::path& c);

    inline bool is_child_directory_of_already_cannonical(const fs::path& c, const fs::path& p)
    {
        return is_parent_directory_of_already_cannonical(p, c);
    }

}

#include "../detail/namespace.hpp"
