#include "parent_child.hpp"

namespace upp::fs
{
    bool is_parent_directory_of_already_cannonical(const fs::path& p, const fs::path& c)
    {
        return (!fs::exists(p) || fs::is_directory(p)) &&
               p != c &&
               c.string().starts_with(p.string());
    }
    bool is_parent_directory_of(const fs::path& p, const fs::path& c)
    {

        return is_parent_directory_of_already_cannonical(
                   fs::weakly_canonical(fs::absolute(p)),
                   fs::weakly_canonical(fs::absolute(c)));
    }
}

