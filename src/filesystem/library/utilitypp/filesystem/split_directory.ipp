#pragma once

#include "split_directory.hpp"

namespace upp::fs
{
    inline const fs::path& split_directory::root_absolute() const
    {
        return _root_abs;
    }

    inline const fs::path& split_directory::root() const
    {
        return _root;
    }

    inline std::uintmax_t split_directory::elements_per_level() const
    {
        return _elements_per_level;
    }

    inline std::uintmax_t split_directory::number_of_levels() const
    {
        return _number_of_levels;
    }

    inline std::uintmax_t split_directory::maximal_number_of_elements() const
    {
        return _maximal_number_of_elements;
    }
}
