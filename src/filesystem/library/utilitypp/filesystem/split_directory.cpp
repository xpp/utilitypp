#include <numeric>

#include <utilitypp/exception/throw_if.hpp>

#include <utilitypp/string/check_format/is_hex_string.hpp>
#include <utilitypp/string/decode/hex_string_to_uint.hpp>
#include <utilitypp/string/encode/to_hex_string.hpp>

#include "remove_empty_directories.hpp"
#include "iterator/sorting_directory_iterator.hpp"
#include "relations/parent_child.hpp"

#include "split_directory.hpp"

namespace upp::fs
{
    split_directory::split_directory(
        fs::path root,
        std::uintmax_t number_of_levels,
        std::uintmax_t elements_per_level,
        bool reuse
    ) :
        _root{root},
        _root_abs
    {
        [ & ]{
            upp_throw_if(std::invalid_argument, root.empty())
                    << "Root is an empty string";
            if (!fs::exists(root))
            {
                fs::create_directories(root);
                return fs::canonical(root);
            }
            else
            {
                const auto root_abs = fs::canonical(root);
                upp_throw_if_not(std::invalid_argument, fs::is_directory(root_abs))
                        << "root (" << root << " -> " << root_abs << ") is not a directory";
                return root_abs;
            }
        }()
    },
    _elements_per_level{elements_per_level},
                        _number_of_levels{number_of_levels},
                        _maximal_number_of_elements{std::pow(_elements_per_level, _number_of_levels)},
                        _level_name_length{static_cast<std::uintmax_t>(std::ceil(std::log(_elements_per_level) / std::log(16)))},
    _reuse{reuse}
    {
        static constexpr auto max_n = std::numeric_limits<std::uintmax_t>::max();
        const auto log_base_elems_per_level = std::log(max_n) / std::log(_elements_per_level);
        //check if the total number of element is to big to be indexed by
        upp_throw_if_less(
            std::invalid_argument,
            log_base_elems_per_level, _number_of_levels
        ) << "The maximal number of elements ("
          << _number_of_levels << '*' << _elements_per_level
          << ") is too big to be indexed by std::uintmax_t (" << max_n
          << ")! The number of levels has to be reduced to "
          << log_base_elems_per_level << " or the elements per level to "
          << std::pow(max_n, 1.0 / _number_of_levels) << '.';
    }

    void split_directory::remove_empty_subdirectories()
    {
        upp::fs::recursive_remove_empty_sub_directories(_root_abs);
    }

    std::string split_directory::to_hex(uintmax_t i)
    {
        return to_hex_string(i, _level_name_length, '0');
    }

    std::deque<fs::path> split_directory::unmanaged_directories_and_files()
    {
        std::deque<fs::path> result;
        collect_directories(
            0,
            _root_abs,
            result,
            [](const auto&, auto, bool valid)
        {
            return !valid;
        }
        );
        return result;
    }

    std::deque<fs::path> split_directory::managed_directories()
    {
        std::deque<fs::path> result;
        collect_directories(
            0,
            _root_abs,
            result,
            [this](const auto & p, auto lvl, bool valid)
        {
            return is_managed_directory(p);
            //            return valid && lvl == _number_of_levels - 1 && upp::fs::is_directory(p);
        }
        );
        return result;
    }

    std::uintmax_t split_directory::index_from_path(const path& path, bool allow_partial_paths)
    {
        upp_throw_if_not(
            std::invalid_argument,
            is_valid_directory_path(path, allow_partial_paths)
        ) << upp_varout(path) << ", " <<  upp_varout(allow_partial_paths);
        auto rel = fs::relative(fs::weakly_canonical(fs::absolute(path)), root_absolute());
        std::uintmax_t index = 0;
        std::uintmax_t factor = 1;
        for (std::size_t l = 0 ; l < number_of_levels(); ++l, factor *= elements_per_level())
        {
            if (rel.empty())
            {
                index *= elements_per_level();
            }
            index += hex_string_to_uint(rel.filename()) * factor;
            rel = rel.parent_path();
        }
        return index;
    }

    path split_directory::path_from_index(std::uintmax_t index)
    {
        upp_throw_if_not_less(std::invalid_argument, index, maximal_number_of_elements());
        fs::path  p;
        for (std::size_t l = 0 ; l < number_of_levels(); ++l)
        {
            const auto mod = index % elements_per_level();
            p = to_hex(mod) / p;
            index = index / elements_per_level();
        }
        return root_absolute() / p.parent_path(); // remove trailing /
    }

    bool split_directory::is_valid_directory_path(const fs::path& p, bool allow_partial_paths)
    {
        const auto abs = fs::weakly_canonical(fs::absolute(p));
        if (!is_parent_directory_of_already_cannonical(root_absolute(), abs))
        {
            return false;
        }
        auto rel = fs::relative(abs, root_absolute());
        for (std::size_t i = 0;; ++i)
        {
            if (i == number_of_levels())
            {
                return rel.empty();
            }
            if (rel.empty())
            {
                return allow_partial_paths;
            }
            if (!is_valid_directory_name(rel.filename().string()))
            {
                return false;
            }
            rel = rel.parent_path();
        }
    }

    bool split_directory::is_managed_directory(const path& p, bool allow_partial_paths)
    {
        const auto abs = fs::weakly_canonical(fs::absolute(p));
        return fs::exists(abs) && fs::is_directory(abs) && is_valid_directory_path(abs, allow_partial_paths);
    }

    bool split_directory::is_valid_directory_name(const std::string& name)
    {
        return (name.size() == _level_name_length) &&
               upp::is_hex_string(name) &&
               !std::isupper(name.at(0)) &&
               !std::isupper(name.at(1)) &&
               (upp::hex_string_to_uint(name) < _elements_per_level);
    }

    template<class F>
    void split_directory::collect_directories(
        uintmax_t lvl,
        const fs::path& p,
        std::deque<fs::path>& result,
        const F& pred
    )
    {
        if (!fs::is_directory(p))
        {
            return;
        }
        for (auto& e : sorting_directory_iterator(p))
        {
            //check matching
            const bool valid = is_valid_directory_name(e.path().filename().string()) &&
                               fs::is_directory(e.path());

            if (pred(e.path(), lvl, valid))
            {
                result.emplace_back(e.path());
            }
            else if (lvl < _number_of_levels && valid)
            {
                collect_directories(lvl + 1, e.path(), result, pred);
            }
        }
    }
}
