#pragma once

#include "detail/namespace.hpp"

namespace upp::fs
{
    bool recursive_remove_empty_sub_directories(const fs::path& p);
    bool recursive_remove_empty_directories(const fs::path& p);
    bool remove_empty_sub_directories(const fs::path& p);
    bool remove_empty_directories(const fs::path& p);
}
