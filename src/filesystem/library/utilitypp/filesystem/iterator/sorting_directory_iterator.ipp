#pragma once

#include <algorithm>

#include <utilitypp/exception/throw_if.hpp>

#include "sorting_directory_iterator.hpp"

namespace upp::fs
{
    //member ctor exception
    template<class Comp>
    inline sorting_directory_iterator<Comp>::sorting_directory_iterator(const fs::directory_iterator& it, comparator_t cmp) :
        _entries{it, fs::directory_iterator{}},
        _cmp{std::move(cmp)}
    {
        std::sort(_entries.begin(), _entries.end(), _cmp);
    }

    template<class Comp>
    inline sorting_directory_iterator<Comp>::sorting_directory_iterator(const fs::path& p, comparator_t cmp) :
        sorting_directory_iterator(fs::directory_iterator(p), cmp)
    {}

    template<class Comp>
    inline sorting_directory_iterator<Comp>::sorting_directory_iterator(const fs::path& p, fs::directory_options options, comparator_t cmp) :
        sorting_directory_iterator(fs::directory_iterator(p, options), cmp)
    {}

    //member ctor error code

    //member fn
    template<class Comp>
    inline bool sorting_directory_iterator<Comp>::valid() const noexcept
    {
        return _idx < _entries.size();
    }

    //member op deref
    template<class Comp>
    inline const fs::directory_entry& sorting_directory_iterator<Comp>::operator*() const
    {
        if (!(_idx < _entries.size()))
        {
            throw fs::filesystem_error
            {
                "non-dereferenceable sorting directory iterator",
                std::make_error_code(std::errc::no_such_file_or_directory)
            };
        }
        return _entries.at(_idx);
    }

    template<class Comp>
    inline const fs::directory_entry* sorting_directory_iterator<Comp>::operator->() const
    {
        return &(*(*this));
    }

    //member op inc dec
    template<class Comp>
    inline sorting_directory_iterator<Comp>& sorting_directory_iterator<Comp>::operator++() noexcept
    {
        ++_idx;
        return *this;
    }
    template<class Comp>
    inline sorting_directory_iterator<Comp>& sorting_directory_iterator<Comp>::increment(std::error_code&) noexcept
    {
        return ++(this);
    }
    template<class Comp>
    inline sorting_directory_iterator<Comp>& sorting_directory_iterator<Comp>::operator--() noexcept
    {
        --_idx;
        return *this;
    }
    template<class Comp>
    inline sorting_directory_iterator<Comp>& sorting_directory_iterator<Comp>::decrement(std::error_code&) noexcept
    {
        return --(this);
    }

    //member op cmp
    template<class Comp>
    inline bool sorting_directory_iterator<Comp>::operator==(const sorting_directory_iterator& rhs) const
    {
        if (!valid() && !rhs.valid())
        {
            return true;
        }
        if (
            valid() != rhs.valid()  || // one is valid and the other is not
            _idx != rhs._idx        || // they are at a different index
            _entries != rhs._entries   // they iterate over different entries
        )
        {
            return false;
        }
        return true;
    }
    template<class Comp>
    inline bool sorting_directory_iterator<Comp>::operator!=(const sorting_directory_iterator& rhs) const
    {
        return !(*this == rhs);
    }

    //free
    template<class Comp>
    inline sorting_directory_iterator<Comp> begin(sorting_directory_iterator<Comp> iter) noexcept
    {
        return iter;
    }

    template<class Comp>
    inline sorting_directory_iterator<Comp> end(const sorting_directory_iterator<Comp>&) noexcept
    {
        return {};
    }
}

#include "../detail/namespace.hpp"
