#pragma once

#include <vector>
#include <filesystem>

namespace upp::fs
{
    namespace fs = std::filesystem;

    template<class Comparator = std::less<>>
    class sorting_directory_iterator
    {
    public:
        using comparator_t = Comparator;

        //dctor
        sorting_directory_iterator() noexcept = default;

        //ctor exception
        explicit sorting_directory_iterator(const fs::directory_iterator& it, comparator_t cmp = {});
        explicit sorting_directory_iterator(const fs::path& p, comparator_t cmp = {});
        sorting_directory_iterator(const fs::path& p, fs::directory_options options, comparator_t cmp = {});

        //ctor error code
        //        explicit sorting_directory_iterator(
        //            const fs::directory_iterator& it,
        //            std::error_code& ec
        //        )
        //        {
        //            const auto n = std::distance(it, fs::directory_iterator{});
        //            _entries.reserve(n);


        //        }


        //        sorting_directory_iterator(const fs::path& p, std::error_code& ec);
        //        sorting_directory_iterator(const fs::path& p,
        //                                   fs::directory_options options,
        //                                   std::error_code& ec);

        sorting_directory_iterator(const sorting_directory_iterator&) = default;
        sorting_directory_iterator(sorting_directory_iterator&&) = default;
        //dtor
        ~sorting_directory_iterator() = default;

        //fn
        bool valid() const noexcept;

        //op assign
        sorting_directory_iterator& operator=(const sorting_directory_iterator&) = default;
        sorting_directory_iterator& operator=(sorting_directory_iterator&&) = default;

        //op deref
        const fs::directory_entry& operator*() const;
        const fs::directory_entry* operator->() const;

        //op inc dec
        sorting_directory_iterator& operator++() noexcept;
        sorting_directory_iterator& increment(std::error_code&) noexcept;
        sorting_directory_iterator& operator--() noexcept;
        sorting_directory_iterator& decrement(std::error_code&) noexcept;

        //op cmp
        bool operator==(const sorting_directory_iterator& rhs) const;
        bool operator!=(const sorting_directory_iterator& rhs) const;
    private:
        std::vector<fs::directory_entry> _entries;
        std::size_t _idx{0};
        Comparator _cmp;
    };

    template<class Comp>
    sorting_directory_iterator<Comp> begin(sorting_directory_iterator<Comp> iter) noexcept;
    template<class Comp>
    sorting_directory_iterator<Comp> end(const sorting_directory_iterator<Comp>&) noexcept;
}

#include "sorting_directory_iterator.ipp"
