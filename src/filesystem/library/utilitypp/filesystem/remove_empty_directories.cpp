#include "remove_empty_directories.hpp"


namespace upp::fs::detail
{
    inline void recursive_remove_empty_sub_directories(const fs::path& p)
    {
        for (auto& e : fs::directory_iterator(p))
        {
            if (fs::is_directory(e.path()))
            {
                recursive_remove_empty_sub_directories(e.path());
                if (fs::is_empty(e.path()))
                {
                    std::error_code ec;
                    fs::remove(e.path(), ec);
                }
            }
        }
    }

    inline void remove_empty_sub_directories(const fs::path& p)
    {
        for (auto& e : fs::directory_iterator(p))
        {
            if (fs::is_directory(e.path()) && fs::is_empty(e.path()))
            {
                std::error_code ec;
                fs::remove(e.path(), ec);
            }
        }
    }
}
namespace upp::fs
{

    bool recursive_remove_empty_sub_directories(const path& p)
    {
        if (!fs::is_directory(p))
        {
            return false;
        }
        detail::recursive_remove_empty_sub_directories(p);
        return true;
    }

    bool recursive_remove_empty_directories(const path& p)
    {
        if (!fs::is_directory(p))
        {
            return false;
        }
        detail::recursive_remove_empty_sub_directories(p);
        if (fs::is_empty(p))
        {
            std::error_code ec;
            return fs::remove(p, ec);
        }
        return false;
    }

    bool remove_empty_sub_directories(const path& p)
    {
        if (!fs::is_directory(p))
        {
            return false;
        }
        detail::remove_empty_sub_directories(p);
        return true;
    }

    bool remove_empty_directories(const path& p)
    {
        if (!fs::is_directory(p))
        {
            return false;
        }
        detail::remove_empty_sub_directories(p);
        if (fs::is_empty(p))
        {
            std::error_code ec;
            return fs::remove(p, ec);
        }
        return false;
    }
}
