#define BOOST_TEST_MODULE filesystem

#include <random>
#include <filesystem>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/scope_guards/on_scope_exit.hpp>
#include <utilitypp/filesystem/split_directory.hpp>
//#include <utilitypp/filesystem/iterator/sorting_directory_iterator.hpp>

BOOST_AUTO_TEST_CASE(test_split_directory_basic_members)
{
    const upp::fs::path p =  "upp_boost_testcases_" + std::to_string(std::random_device{}());
    upp::fs::split_directory sd{p, 4, 256};
    const auto pa = sd.root_absolute();
    upp_on_scope_exit{upp::fs::remove_all(p);};

    BOOST_CHECK_EQUAL(sd.root(), p);
    BOOST_CHECK_EQUAL(sd.elements_per_level(), 256);
    BOOST_CHECK_EQUAL(sd.number_of_levels(), 4);
    BOOST_CHECK_EQUAL(sd.maximal_number_of_elements(), std::pow(256, 4));

    //valid name
    BOOST_CHECK(sd.is_valid_directory_name("00"));
    BOOST_CHECK(sd.is_valid_directory_name("f0"));
    BOOST_CHECK(sd.is_valid_directory_name("fa"));
    BOOST_CHECK(!sd.is_valid_directory_name("Fa"));
    BOOST_CHECK(!sd.is_valid_directory_name("FA"));
    BOOST_CHECK(!sd.is_valid_directory_name("fa0"));
    BOOST_CHECK(!sd.is_valid_directory_name("G0"));

    //vaild path
    BOOST_CHECK(sd.is_valid_directory_path(p / "00" / "00" / "00" / "00"));
    BOOST_CHECK(sd.is_valid_directory_path(p / "00" / "01" / "02" / "03"));
    BOOST_CHECK(sd.is_valid_directory_path(p / "00" / "01" / "02" / "04"));
    BOOST_CHECK(!sd.is_valid_directory_path(p / "77" / "00" / "00" / "qq"));
    BOOST_CHECK(!sd.is_valid_directory_path(p / "77" / "00" / "00"));

    BOOST_CHECK(sd.is_valid_directory_path(p / "47" / "68" / "24" / "23", true));
    BOOST_CHECK(sd.is_valid_directory_path(p / "77" / "00" / "00", true));
    BOOST_CHECK(!sd.is_valid_directory_path(p / "77" / "00" / "q0", true));

    //idx to path
    BOOST_CHECK_EQUAL(sd.path_from_index(0), pa / "00" / "00" / "00" / "00");
    BOOST_CHECK_EQUAL(sd.path_from_index(1), pa / "00" / "00" / "00" / "01");
    BOOST_CHECK_EQUAL(sd.path_from_index(16), pa / "00" / "00" / "00" / "10");
    BOOST_CHECK_EQUAL(sd.path_from_index(255), pa / "00" / "00" / "00" / "ff");
    BOOST_CHECK_EQUAL(sd.path_from_index(256 * 256 * 256), pa / "01" / "00" / "00" / "00");

    //path to idx
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "00" / "00" / "00" / "00"),   0);
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "00" / "00" / "00" / "01"),   1);
    BOOST_CHECK_EQUAL(sd.index_from_path(pa / "00" / "00" / "00" / "10"),  16);
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "00" / "00" / "00" / "ff"), 255);
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "01" / "00" / "00" / "00"), 256 * 256 * 256);
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "01" / "00" / "00" / "00", true), 256 * 256 * 256);
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "01" / "00" / "00", true), 256 * 256 * 256);
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "01" / "00", true), 256 * 256 * 256);
    BOOST_CHECK_EQUAL(sd.index_from_path(p / "01", true), 256 * 256 * 256);
}

BOOST_AUTO_TEST_CASE(test_split_directory_advanced_members)
{
    const upp::fs::path p =  "upp_boost_testcases_" + std::to_string(std::random_device{}());
    upp::fs::split_directory sd{p, 4, 256};
    upp_on_scope_exit{upp::fs::remove_all(p);};

    BOOST_CHECK(!sd.is_managed_directory(p / "00" / "01" / "02" / "04"));
    BOOST_CHECK(!sd.is_managed_directory(p / "77" / "00" / "00" / "qq"));
    BOOST_CHECK(!sd.is_managed_directory(p / "77" / "00" / "00", true));
    BOOST_CHECK(!sd.is_managed_directory(p / "77" / "00" / "q0", true));

    //create
    {
        upp::fs::create_directories(p / "00" / "00" / "00" / "00");

        upp::fs::create_directories(p / "00" / "01" / "02" / "03");
        std::ofstream(p / "00" / "01" / "02" / "04");

        upp::fs::create_directories(p / "77" / "00" / "00" / "qq");
        upp::fs::create_directories(p / "ww" / "qq" / "00" / "00");
    }

    BOOST_CHECK(!sd.is_managed_directory(p / "00" / "01" / "02" / "04"));
    BOOST_CHECK(!sd.is_managed_directory(p / "77" / "00" / "00" / "qq"));
    BOOST_CHECK(sd.is_managed_directory(p / "77" / "00" / "00", true));
    BOOST_CHECK(!sd.is_managed_directory(p / "77" / "00" / "q0", true));

    std::cout << "files\n";
    for (auto& e : upp::fs::recursive_directory_iterator(p))
    {
        std::cout << "    managed full / partial = "
                  << sd.is_managed_directory(e)
                  << " / " << sd.is_managed_directory(e, true)
                  << " -> " << e << std::endl;
    }

    //separate files
    {
        // *INDENT-OFF*
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww"         , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww"         , true ), false);

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww/qq"      , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww/qq"      , true ), false);

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww/qq/00"   , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww/qq/00"   , true ), false);

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww/qq/00/00", false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "ww/qq/00/00", true ), false);

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77"         , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77"         , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77/00"      , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77/00"      , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77/00/00"   , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77/00/00"   , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77/00/00/qq", false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "77/00/00/qq", true ), false);

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00"         , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00"         , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/00"      , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/00"      , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/00/00"   , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/00/00"   , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/00/00/00", false), true );
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/00/00/00", true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01"      , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01"      , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01/02"   , false), false);
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01/02"   , true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01/02/03", false), true );
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01/02/03", true ), true );

        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01/02/04", false), false); // is a file
        BOOST_CHECK_EQUAL(sd.is_managed_directory(p / "00/01/02/04", true ), false); // is a file
        // *INDENT-ON*
    }


    //managed_directories
    {
        std::array<upp::fs::path, 3> expected
        {
            sd.root_absolute() / "00" / "00" / "00" / "00",
            sd.root_absolute() / "00" / "01" / "02" / "03"
        };
        std::size_t i = 0;
        const auto managed = sd.managed_directories();
        //print
        std::cout << "managed\n";
        for (const auto& e : managed)
        {
            std::cout << "    " << e << std::endl;
        }
        //check
        BOOST_REQUIRE_EQUAL(managed.size(), 2);
        for (const auto& e : managed)
        {
            BOOST_CHECK_EQUAL(e, expected.at(i++));
        }
    }

    //unmanaged_directories_and_files
    {
        std::array<upp::fs::path, 3> expected
        {
            sd.root_absolute() / "00" / "01" / "02" / "04",
            sd.root_absolute() / "77" / "00" / "00" / "qq",
            sd.root_absolute() / "ww"
        };
        std::size_t i = 0;
        const auto umanaged = sd.unmanaged_directories_and_files();
        //print
        std::cout << "umanaged\n";
        for (const auto& e : umanaged)
        {
            std::cout << "    " << e << std::endl;
        }
        //check
        BOOST_REQUIRE_EQUAL(umanaged.size(), 3);
        for (const auto& e : umanaged)
        {
            BOOST_CHECK_EQUAL(e, expected.at(i++));
        }
    }
}

BOOST_AUTO_TEST_CASE(test_split_directory_failure)
{
    const upp::fs::path p =  "upp_boost_testcases_" + std::to_string(std::random_device{}());
    upp_on_scope_exit{upp::fs::remove_all(p);};

    BOOST_CHECK_THROW(
        upp::fs::split_directory(p, 16),
        std::invalid_argument
    );
    //    BOOST_CHECK_THROW(
    //        upp::fs::split_directory(""),
    //        std::invalid_argument
    //    );
}
