#define BOOST_TEST_MODULE filesystem

#include <random>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/filesystem/iterator/sorting_directory_iterator.hpp>


BOOST_AUTO_TEST_CASE(test_sorting_directory_iterator)
{
    const upp::fs::path p =  "upp_boost_testcases_" + std::to_string(std::random_device{}());

    upp::fs::create_directories(p / "0");
    upp::fs::create_directories(p / "b");
    upp::fs::create_directories(p / "a2");
    upp::fs::create_directories(p / "d");
    upp::fs::create_directories(p / "c");
    upp::fs::create_directories(p / "99");
    upp::fs::create_directories(p / "a");
    upp::fs::create_directories(p / "9");

    {
        std::size_t i = 0;
        for (auto& e : upp::fs::sorting_directory_iterator(p))
        {
            std::cout << e << std::endl;

            // *INDENT-OFF*
            switch (i)
            {
            case 0: BOOST_CHECK_EQUAL(e.path(), p / "0" ); break;
            case 1: BOOST_CHECK_EQUAL(e.path(), p / "9" ); break;
            case 2: BOOST_CHECK_EQUAL(e.path(), p / "99"); break;
            case 3: BOOST_CHECK_EQUAL(e.path(), p / "a" ); break;
            case 4: BOOST_CHECK_EQUAL(e.path(), p / "a2"); break;
            case 5: BOOST_CHECK_EQUAL(e.path(), p / "b" ); break;
            case 6: BOOST_CHECK_EQUAL(e.path(), p / "c" ); break;
            case 7: BOOST_CHECK_EQUAL(e.path(), p / "d" ); break;
            default: BOOST_CHECK(false);
            }
            // *INDENT-ON*
            ++i;
        }
    }
    {
        std::size_t i = 0;
        for (auto& e : upp::fs::sorting_directory_iterator<std::greater<>>(p))
        {
            std::cout << e << std::endl;

            // *INDENT-OFF*
            switch (i)
            {
            case 7: BOOST_CHECK_EQUAL(e.path(), p / "0" ); break;
            case 6: BOOST_CHECK_EQUAL(e.path(), p / "9" ); break;
            case 5: BOOST_CHECK_EQUAL(e.path(), p / "99"); break;
            case 4: BOOST_CHECK_EQUAL(e.path(), p / "a" ); break;
            case 3: BOOST_CHECK_EQUAL(e.path(), p / "a2"); break;
            case 2: BOOST_CHECK_EQUAL(e.path(), p / "b" ); break;
            case 1: BOOST_CHECK_EQUAL(e.path(), p / "c" ); break;
            case 0: BOOST_CHECK_EQUAL(e.path(), p / "d" ); break;
            default: BOOST_CHECK(false);
            }
            // *INDENT-ON*
            ++i;
        }
    }
    upp::fs::remove_all(p);
}
