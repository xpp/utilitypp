#define BOOST_TEST_MODULE filesystem

#include <random>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/scope_guards/on_scope_exit.hpp>
#include <utilitypp/filesystem/relations.hpp>

BOOST_AUTO_TEST_CASE(test_is_parent_directory_of)
{
    //not existing files
    {
        BOOST_CHECK(upp::fs::is_parent_directory_of("0", "0/1"));
        BOOST_CHECK(upp::fs::is_parent_directory_of("0/1/2/3/4", "0/1/2/3/4/5"));

        BOOST_CHECK(upp::fs::is_parent_directory_of(".", "0"));
        BOOST_CHECK(upp::fs::is_parent_directory_of(".", "0"));
        BOOST_CHECK(upp::fs::is_parent_directory_of("./0", "0/1"));
        BOOST_CHECK(upp::fs::is_parent_directory_of("..", "."));
        BOOST_CHECK(upp::fs::is_parent_directory_of("0", "./1/../0/1"));

        BOOST_CHECK(!upp::fs::is_parent_directory_of("0", "0"));
        BOOST_CHECK(!upp::fs::is_parent_directory_of("0/1", "0"));
        BOOST_CHECK(!upp::fs::is_parent_directory_of(".", ".."));
        BOOST_CHECK(!upp::fs::is_parent_directory_of("./1/../0/1", "0"));
    }
    //existing files
    {
        const upp::fs::path p =  "upp_boost_testcases_" + std::to_string(std::random_device{}());
        upp_on_scope_exit{upp::fs::remove_all(p);};
        upp::fs::create_directories(p / "0" / "1" / "2" / "3" / "4" / "5");
        upp::fs::create_directories(p / "1" / "1" / "2" / "3" / "4" / "5");
        std::ofstream(p / "0" / "2");
        upp::fs::current_path(p);

        BOOST_CHECK(upp::fs::is_parent_directory_of("0", "0/1"));
        BOOST_CHECK(upp::fs::is_parent_directory_of("0/1/2/3/4", "0/1/2/3/4/5"));

        BOOST_CHECK(upp::fs::is_parent_directory_of(".", "0"));
        BOOST_CHECK(upp::fs::is_parent_directory_of(".", "0"));
        BOOST_CHECK(upp::fs::is_parent_directory_of("./0", "0/1"));
        BOOST_CHECK(upp::fs::is_parent_directory_of("..", "."));
        BOOST_CHECK(upp::fs::is_parent_directory_of("0", "./1/../0/1"));

        BOOST_CHECK(!upp::fs::is_parent_directory_of("0", "0"));
        BOOST_CHECK(!upp::fs::is_parent_directory_of("0/1", "0"));
        BOOST_CHECK(!upp::fs::is_parent_directory_of(".", ".."));
        BOOST_CHECK(!upp::fs::is_parent_directory_of("./1/../0/1", "0"));

        BOOST_CHECK(upp::fs::exists("./0/2"));
        BOOST_CHECK(!upp::fs::is_parent_directory_of("./0/2", "0/2/3"));
        BOOST_CHECK(!upp::fs::is_parent_directory_of("./1/../0/2", "0/2/3"));
    }
}
