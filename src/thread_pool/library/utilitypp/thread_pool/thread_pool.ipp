#pragma once

#include "thread_pool.hpp"

namespace upp
{
    template<class ThreadDataT>
    template<class...Ts> inline
    thread_pool<ThreadDataT>::thread_pool(Ts&& ...ts)
        : base(std::forward<Ts>(ts)...)
    {
        const std::size_t worker_count = this->worker_count();
        upp_throw_if_equal_0(std::invalid_argument, worker_count)
                << "The thread pool has to have at least one worker";

        _workers.reserve(worker_count);
        _idle_worker_count = 0;
        auto to_init = worker_count;
        while (to_init--)
        {
            _workers.emplace_back([n = to_init, this] {executor(n);});
        }
        //wit for all threads to be started (this allows moving the pool after the ctor returns)
        while (_idle_worker_count != worker_count)
        {
            std::this_thread::sleep_for(busy_wait_dt);
        }
    }

    template<class ThreadDataT> inline
    thread_pool<ThreadDataT>::~thread_pool()
    {
        _enque_allowed = false;
        _shutdown = true;
        for (auto& t : _workers)
        {
            t.join();
        }
    }

    template<class ThreadDataT> inline
    void
    thread_pool<ThreadDataT>::executor(std::size_t index)
    {
        job_handle_t handle;
        handle._index = index;
        if constexpr(!void_thread_data())
        {
            handle._data = &thread_pool::thread_data(index);
        }
        ++_idle_worker_count;

        while (!_shutdown)
        {
            job_function_t job;
            {
                std::unique_lock lock{_job_mutex};
                if (_job_queue.empty())
                {
                    lock.unlock();
                    std::this_thread::sleep_for(busy_wait_dt);
                    continue;
                }
                job = std::move(_job_queue.front());
                _job_queue.pop_front();
                --_idle_worker_count;
                --_job_queue_size;
            }
            //we have a job -> execute it
            handle._reenqueue = false;
            job(handle);
            job = job_function_t{};
            ++_idle_worker_count;
        }
    }

    template<class ThreadDataT>
    template<class FunctionT, class FutureT> inline
    FutureT
    thread_pool<ThreadDataT>::enqueue(FunctionT function)
    {
        upp_throw_if_not(std::logic_error, _enque_allowed.load())
                << "enqueuing of new jobs is blocked!";
        using enqueue_helper_t = detail::thread_pool::enqueue_helper<ThreadDataT, FunctionT>;
        return enqueue_helper_t::enqueue(*this, std::move(function));
    }

    template<class ThreadDataT> inline
    std::size_t
    thread_pool<ThreadDataT>::worker_idle_count() const
    {
        return _idle_worker_count;
    }

    template<class ThreadDataT> inline
    std::size_t
    thread_pool<ThreadDataT>::worker_running_count() const
    {
        return this->worker_count() - _idle_worker_count;
    }

    template<class ThreadDataT> inline
    std::size_t
    thread_pool<ThreadDataT>::job_queue_size() const
    {
        return _job_queue_size + worker_running_count();
    }

    template<class ThreadDataT> inline
    bool
    thread_pool<ThreadDataT>::job_queue_empty() const
    {
        return 0 == job_queue_size();
    }

    template<class ThreadDataT> inline
    void
    thread_pool<ThreadDataT>::wait_for_all_jobs() const
    {
        while (!job_queue_empty())
        {
            std::this_thread::sleep_for(busy_wait_dt);
        }
    }

    template<class ThreadDataT>
    template< class Clock, class Duration > inline
    bool
    thread_pool<ThreadDataT>::wait_for_all_jobs_until(
        const std::chrono::time_point<Clock, Duration>& timeout
    ) const
    {
        while (!job_queue_empty())
        {
            if (Clock::now() >= timeout)
            {
                return false;
            }
            std::this_thread::sleep_for(std::chrono::microseconds{1000});
        }
        return true;
    }

    template<class ThreadDataT>
    template< class Rep, class Period > inline
    bool
    thread_pool<ThreadDataT>::wait_for_all_jobs_for(const std::chrono::duration<Rep, Period>& rel) const
    {
        return wait_for_all_jobs_until(std::chrono::steady_clock::now() + rel);
    }

    template<class ThreadDataT>
    template<class FunctionT, class ReturnT> inline
    void
    thread_pool<ThreadDataT>::internal_enqueue(std::shared_ptr<std::promise<ReturnT>>&& promise, FunctionT function)
    {
        if (!_enque_allowed)
        {
            return; // break promise;
        }

        auto job =
            [
                this,
                promise = std::move(promise),
                function = std::forward<FunctionT>(function)
            ](job_handle_t& handle) mutable
        {
            try
            {
                if constexpr(std::is_same_v<ReturnT, void>)
                {
                    function(handle);
                    if (handle._reenqueue)
                    {
                        //ignore the return value!
                        internal_enqueue(std::move(promise), std::move(function));
                    }
                    else
                    {
                        promise->set_value();
                    }
                }
                else
                {
                    auto&& value = function(handle);
                    if (handle._reenqueue)
                    {
                        //ignore the return value!
                        internal_enqueue(std::move(promise), std::move(function));
                    }
                    else
                    {
                        promise->set_value(std::move(value));
                    }
                }
            }
            catch (...)
            {
                try
                {
                    // store anything thrown in the promise
                    promise->set_exception(std::current_exception());
                }
                catch (...)
                {
                    upp_throw(std::logic_error)
                            << "unreachable code reached!";
                    std::terminate(); //this code is impossible to reach!
                }
            }
        };
        std::lock_guard lock{_job_mutex};
        _job_queue.emplace_back(std::move(job));
        ++_job_queue_size;
    }
}

