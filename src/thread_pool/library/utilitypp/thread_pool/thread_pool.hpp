#pragma once

#include <type_traits>

#include <deque>
#include <vector>
#include <span>

#include <thread>
#include <atomic>
#include <mutex>
#include <future>
#include <condition_variable>

#include <utilitypp/exception/throw_if.hpp>

#include "forward_declares.hpp"

//thread_pool
namespace upp
{
    template<class ThreadDataT>
    class thread_pool : public ::upp::detail::thread_pool::thread_pool_base<thread_pool<ThreadDataT>>
    {
        using base = ::upp::detail::thread_pool::thread_pool_base<thread_pool<ThreadDataT>>;

        template<class, class, class> friend struct upp::detail::thread_pool::enqueue_helper;
    public:
        using thread_data_t = base::thread_data_t;
        using job_handle_t = detail::thread_pool::job_handle<thread_data_t>;
        using job_function_t = detail::thread_pool::job_function<thread_data_t>;

        static constexpr bool void_thread_data()
        {
            return std::is_same_v<thread_data_t, void>;
        }

        //ctor / dtor
    public:
        template<class...Ts>
        thread_pool(Ts&& ...ts);

        ~thread_pool();

        //execute
    private:
        void executor(std::size_t index);

        //jobs
    public:
        template<
                class FunctionT,
                class FutureT = detail::thread_pool::enqueue_helper<ThreadDataT, FunctionT>::future_t>
        FutureT
        enqueue(FunctionT function);

        std::size_t worker_idle_count() const;
        std::size_t worker_running_count() const;

        std::size_t job_queue_size() const;
        bool job_queue_empty() const;


        void wait_for_all_jobs() const;
        template< class Clock, class Duration >
        bool wait_for_all_jobs_until(const std::chrono::time_point<Clock, Duration>& timeout) const;
        template< class Rep, class Period >
        bool wait_for_all_jobs_for(const std::chrono::duration<Rep, Period>& rel) const;
    private:
        template<class FunctionT, class ReturnT>
        void
        internal_enqueue(std::shared_ptr<std::promise<ReturnT>>&& promise, FunctionT function);

        //data
    private:
        std::atomic_bool _enque_allowed{true};
        std::atomic_bool _shutdown{false};
        std::vector<std::thread> _workers;
        std::atomic_size_t _idle_worker_count{0};

        std::mutex _job_mutex;
        std::atomic_size_t _job_queue_size{0};
        std::deque<job_function_t> _job_queue;

        static constexpr std::chrono::microseconds busy_wait_dt{1000};
    };
}



#include "thread_pool.ipp"
