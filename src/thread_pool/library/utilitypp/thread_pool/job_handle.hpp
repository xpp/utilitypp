#pragma once

#include "forward_declares.hpp"

namespace upp::detail::thread_pool
{
    template<class ThreadDataT> struct job_handle_impl;

    template<>
    struct job_handle_impl<void>
    {
        std::size_t thread_index() const
        {
            return _index;
        }

        void reenqueue(bool reenq = true)
        {
            _reenqueue = reenq;
        }
    protected:
        friend class upp::thread_pool<void>;
        bool _reenqueue;
        std::size_t _index;
    };

    template<class ThreadDataT>
    struct job_handle_impl : job_handle_impl<void>
    {
        ThreadDataT& data()
        {
            return *_data;
        }
    protected:
        friend class upp::thread_pool<ThreadDataT>;
        friend class upp::thread_pool<ThreadDataT&>;
        ThreadDataT* _data;
    };
}
