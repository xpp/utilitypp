#pragma once

#include <span>

#include "forward_declares.hpp"

//thread_pool_base<tp_t<ThreadDataT>>
namespace upp::detail::thread_pool
{
    template<class ThreadDataT>
    class thread_pool_base<tp_t<ThreadDataT>>
    {
    public:
        using thread_data_t = ThreadDataT;

    public:
        thread_pool_base(const thread_pool_base&) = delete;
        thread_pool_base(thread_pool_base&&) = default;

        thread_pool_base(std::vector<thread_data_t>&& vec);
        thread_pool_base(const std::vector<thread_data_t>& vec);
        thread_pool_base(std::size_t n = std::thread::hardware_concurrency());

        thread_data_t& thread_data(std::size_t i);
        const thread_data_t& thread_data(std::size_t i) const;

        std::span<thread_data_t> thread_data();
        std::span<const thread_data_t> thread_data() const;

        std::size_t worker_count() const;
    private:
        std::vector<thread_data_t> _data;
    };
}

//thread_pool_base<tp_t<ThreadDataT&>>
namespace upp::detail::thread_pool
{
    template<class ThreadDataT>
    class thread_pool_base<tp_t<ThreadDataT&>>
    {
    public:
        using thread_data_t = ThreadDataT;
        //ctor
    public:
        thread_pool_base() = delete;
        thread_pool_base(const thread_pool_base&) = delete;
        thread_pool_base(thread_pool_base&&) = default;
        thread_pool_base(std::vector<thread_data_t>& vec);

        thread_data_t& thread_data(std::size_t i);
        const thread_data_t& thread_data(std::size_t i) const;

        std::span<thread_data_t> thread_data();
        std::span<const thread_data_t> thread_data() const;

        std::size_t worker_count() const;
    private:
        std::span<thread_data_t> _data;
    };
}

//thread_pool_base<tp_t<void>>
namespace upp::detail::thread_pool
{
    template<>
    class thread_pool_base<tp_t<void>>
    {
    public:
        thread_pool_base(const thread_pool_base&) = default;
        thread_pool_base(thread_pool_base&&) = default;
        thread_pool_base(std::size_t n = std::thread::hardware_concurrency());

        std::size_t worker_count() const;
    private:
        std::size_t _worker_count;
    };
}

//thread_pool_base<tp_t<ThreadDataT>>
namespace upp::detail::thread_pool
{
    template<class ThreadDataT> inline
    tpb_t<ThreadDataT>::thread_pool_base(std::size_t n): _data(n) {}

    template<class ThreadDataT> inline
    tpb_t<ThreadDataT>::thread_pool_base(std::vector<thread_data_t>&& vec) : _data{std::move(vec)} {}

    template<class ThreadDataT> inline
    tpb_t<ThreadDataT>::thread_pool_base(const std::vector<thread_data_t>& vec) : _data{vec} {}

    template<class ThreadDataT> inline
    tpb_t<ThreadDataT>::thread_data_t&
    tpb_t<ThreadDataT>::thread_data(std::size_t i)
    {
        return _data.at(i);
    }
    template<class ThreadDataT> inline
    const tpb_t<ThreadDataT>::thread_data_t&
    tpb_t<ThreadDataT>::thread_data(std::size_t i) const
    {
        return _data.at(i);
    }

    template<class ThreadDataT> inline
    std::span<typename tpb_t<ThreadDataT>::thread_data_t>
    tpb_t<ThreadDataT>::thread_data()
    {
        return {_data};
    }
    template<class ThreadDataT> inline
    std::span<const typename tpb_t<ThreadDataT>::thread_data_t>
    tpb_t<ThreadDataT>::thread_data() const
    {
        return {_data};
    }

    template<class ThreadDataT> inline
    std::size_t
    tpb_t<ThreadDataT>::worker_count() const
    {
        return _data.size();
    }
}

//thread_pool_base<tp_t<ThreadDataT&>>
namespace upp::detail::thread_pool
{
    template<class ThreadDataT> inline
    tpb_t<ThreadDataT&>::thread_pool_base(std::vector<thread_data_t>& vec)
        : _data{vec}
    {}

    template<class ThreadDataT> inline
    tpb_t<ThreadDataT&>::thread_data_t&
    tpb_t<ThreadDataT&>::thread_data(std::size_t i)
    {
        upp_throw_if_not_less(std::range_error, i, _data.size());
        return _data[i];
    }

    template<class ThreadDataT> inline
    const tpb_t<ThreadDataT&>::thread_data_t&
    tpb_t<ThreadDataT&>::thread_data(std::size_t i) const
    {
        upp_throw_if_not_less(std::range_error, i, _data.size());
        return _data[i];
    }

    template<class ThreadDataT> inline
    std::span<typename tpb_t<ThreadDataT&>::thread_data_t>
    tpb_t<ThreadDataT&>::thread_data()
    {
        return {_data};
    }

    template<class ThreadDataT> inline
    std::span<const typename tpb_t<ThreadDataT&>::thread_data_t>
    tpb_t<ThreadDataT&>::thread_data() const
    {
        return {_data};
    }

    template<class ThreadDataT> inline
    std::size_t
    tpb_t<ThreadDataT&>::worker_count() const
    {
        return _data.size();
    }
}

//thread_pool_base<tp_t<void>>
namespace upp::detail::thread_pool
{
    inline
    tpb_t<void>::thread_pool_base(std::size_t n)
        : _worker_count{n}
    {}

    inline
    std::size_t
    tpb_t<void>::worker_count() const
    {
        return _worker_count;
    }
}
