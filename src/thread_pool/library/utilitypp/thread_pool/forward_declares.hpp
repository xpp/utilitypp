#pragma once

#include <functional>
#include <type_traits>

namespace upp
{
    template<class ThreadDataT>
    class thread_pool;
}

namespace upp::detail::thread_pool
{

    template<class ThreadDataT>
    struct job_handle_impl;

    template<class ThreadDataT>
    using job_handle = job_handle_impl<std::remove_reference_t<ThreadDataT>>;

    template<class ThreadDataT>
    using job_function = std::function<void(job_handle<ThreadDataT>&)>;

    template<class ThreadDataT, class FunctionT, class = void>
    struct enqueue_helper;

    template<class>
    class thread_pool_base;

    template<class ThreadDataT>
    using tp_t = ::upp::thread_pool<ThreadDataT>;

    template<class ThreadDataT>
    using tpb_t = thread_pool_base<tp_t<ThreadDataT>>;
}

