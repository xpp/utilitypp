#pragma once

#include <type_traits>
#include <future>

#include "forward_declares.hpp"

namespace upp::detail::thread_pool
{
    template<class TData, class Fnc>
    concept handle_functor = std::is_invocable_v<Fnc, job_handle<TData>&>;

    template<class TData, class Fnc>
    concept void_functor = !handle_functor<TData, Fnc>&&
                           std::is_invocable_v<Fnc>;

    template<class TData, class Fnc>
    concept data_functor = !handle_functor<TData, Fnc>&&
                           !void_functor<TData, Fnc>&&
                           std::is_invocable_v<Fnc, TData&>;

    template<class TData, class Fnc>
    concept valid_functor = handle_functor<TData, Fnc> ||
                            data_functor<TData, Fnc> ||
                            void_functor<Fnc, Fnc>;

    template<class TData, class Fnc, class>
    struct enqueue_helper
    {
        static_assert(
            valid_functor<TData, Fnc>,
            "The function has to take one of the following parameters: "
            "TData&, a job_handle& for TData or void"
        );
    };

    template<class TData, class Fnc> requires handle_functor<TData, Fnc>
    struct enqueue_helper <TData, Fnc>
    {
        using return_t = std::invoke_result_t<Fnc, job_handle<TData>&>;
        using future_t = std::future<return_t>;
        static std::future<return_t> enqueue(auto& pool, Fnc function)
        {
            auto promise = std::make_shared<std::promise<return_t>>();
            auto future = promise->get_future();
            pool.internal_enqueue(std::move(promise), std::move(function));
            return future;
        }
    };

    template<class TData, class Fnc> requires void_functor<TData, Fnc>
    struct enqueue_helper<TData, Fnc>
    {
        using return_t = std::invoke_result_t<Fnc>;
        using future_t = std::future<return_t>;
        static std::future<return_t> enqueue(auto& pool, Fnc function)
        {
            auto promise = std::make_shared<std::promise<return_t>>();
            auto future = promise->get_future();
            auto wrapped_function = [function = std::move(function)](job_handle<TData>&)
            {
                function();
            };
            pool.internal_enqueue(std::move(promise), std::move(wrapped_function));
            return future;
        }
    };

    template<class TData, class Fnc> requires data_functor<TData, Fnc>
    struct enqueue_helper<TData, Fnc>
    {
        static_assert(!std::is_same_v<TData, void>); // make sure the case is caught above
        using return_t = std::invoke_result_t<Fnc, TData&>;
        using future_t = std::future<return_t>;
        static std::future<return_t> enqueue(auto& pool, Fnc function)
        {
            auto promise = std::make_shared<std::promise<return_t>>();
            auto future = promise->get_future();
            auto wrapped_function = [function = std::move(function)](job_handle<TData>& h)
            {
                function(h.data());
            };
            pool.internal_enqueue(std::move(promise), std::move(wrapped_function));
            return future;
        }
    };
}


