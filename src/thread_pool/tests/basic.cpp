#define BOOST_TEST_MODULE thread_pool

#include <iostream>
#include <random>

#include <boost/test/included/unit_test.hpp>

#include <utilitypp/thread_pool.hpp>

static constexpr auto n_iterations = 50;

struct Data
{
    std::vector<std::size_t> vals;
};

void add_size_as_value(Data& d)
{
    const auto n = d.vals.size();
    std::cout << "add_size_as_value " << n << '\n';
    d.vals.emplace_back(n);
}

std::size_t add_sizex1k_as_value_if_not_empty(upp::thread_pool<Data>::job_handle_t& h)
{
    if (!h.data().vals.empty())
    {
        const auto n = h.data().vals.size() * 1000;
        std::cout << "add_sizex1k_as_value_if_not_empty " << n << '\n';
        h.data().vals.emplace_back(n);
        return n;
    }
    std::cout << "add_sizex1k_as_value_if_not_empty reenqueue\n";
    h.reenqueue();
    return ~0ul;
}

//adds several types of jobs and checks the end result
//calls make_tp_and_get_data(n_threads) -> pair<pool, data or ref to data>
void do_base_test(auto make_tp_and_get_data)
{
    std::mt19937 gen{std::random_device{}()};
    std::uniform_int_distribution<std::size_t> d_n_threads{1, 16};
    std::uniform_int_distribution<std::size_t> d_n_sz_factor{4, 32};
    std::uniform_int_distribution<std::size_t> d_n_szx10_factor{1, 16};

    for (std::size_t i = 0; i < n_iterations; ++i)
    {
        std::cout << "####################################################\n";
        std::cout << i << '\n';
        std::cout << "####################################################\n";
        std::atomic_size_t lambda_cnt = 0;
        const auto lambda = [&lambda_cnt]
        {
            ++lambda_cnt;
        };

        const std::size_t n_threads = d_n_threads(gen);
        const std::size_t n_sz = n_threads * d_n_sz_factor(gen);
        const std::size_t n_szx1k = n_threads * d_n_szx10_factor(gen);

        auto pair = make_tp_and_get_data(n_threads);
        auto& [tp, data] = pair;

        std::vector<std::future<std::size_t>> futures;
        for (std::size_t i = 0; i < n_szx1k; i++)
        {
            tp.enqueue(lambda);
            futures.emplace_back(tp.enqueue(add_sizex1k_as_value_if_not_empty));
            std::cout << "#j " << tp.job_queue_size() << '\n';
        }
        for (std::size_t i = 0; i < n_sz; i++)
        {
            tp.enqueue(lambda);
            tp.enqueue(add_size_as_value);
            std::cout << "#j " << tp.job_queue_size() << '\n';
        }

        tp.wait_for_all_jobs();

        BOOST_REQUIRE_EQUAL(data.size(), tp.thread_data().size());
        for(std::size_t i = 0; i < data.size(); ++i)
        {
            BOOST_REQUIRE_EQUAL(&(data[i]), &(tp.thread_data(i)));
        }

        BOOST_CHECK_EQUAL(lambda_cnt.load(), n_szx1k + n_sz);

        std::map<std::size_t, std::size_t> freturns;
        {
            std::cout << "futures\t//";
            for (auto& f : futures)
            {
                const auto v = f.get();
                if (!freturns.count(v))
                {
                    freturns[v] = 0;
                }
                ++freturns[v];
                std::cout << '\t' << v;
            }
            std::cout << '\n';
        }

        BOOST_CHECK_EQUAL(tp.worker_count(), data.size());
        for (std::size_t tidx = 0; tidx < tp.worker_count(); ++tidx)
        {
            std::cout << tidx << "\t//";
            const auto& dat = data[tidx];
            for (std::size_t i = 0; i < dat.vals.size(); ++i)
            {
                const auto v = dat.vals.at(i);
                if (v > i)
                {
                    BOOST_CHECK_EQUAL(v, i * 1000);
                    BOOST_CHECK(freturns.count(i * 1000));
                    BOOST_CHECK(freturns.at(i * 1000));
                    --freturns.at(i * 1000);
                }
                else
                {
                    BOOST_CHECK_EQUAL(v, i);
                }
                std::cout << '\t' << v;
            }
            std::cout << '\n';
        }
        for (auto [key, value] : freturns)
        {
            BOOST_CHECK_EQUAL(value, 0);
        }
    }
}


BOOST_AUTO_TEST_CASE(owning_thread_data)
{
    do_base_test([](auto n_threads)
    {
        upp::thread_pool<Data> tp{n_threads};
        auto data = tp.thread_data();
        return std::pair{std::move(tp), data};
    });
}

BOOST_AUTO_TEST_CASE(referencing_thread_data)
{
    do_base_test([](auto n_threads)
    {
        std::vector<Data> data(n_threads);
        upp::thread_pool<Data&> tp{data};
        return std::pair{std::move(tp), std::move(data)};
    });
}
