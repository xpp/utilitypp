#pragma once

#include <array>

namespace upp
{
    template<class T, std::size_t N>
    inline std::array<T, N> array_from_value(const T& val)
    {
        std::array<T, N> array;
        array.fill(val);
        return array;
    }
}
