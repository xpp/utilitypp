#pragma once

#include <tuple>
#include <type_traits>

namespace upp
{
    template<class>
    struct is_tuple : std::false_type {};

    template<class...Ts>
    struct is_tuple<std::tuple<Ts...>> : std::true_type {};

    template<class T>
    static constexpr auto is_tuple_v = is_tuple<T>::value;
}
