#pragma once

#include <tuple>
#include <type_traits>

#include <utilitypp/type_traits/remove_cvr.hpp>

namespace upp
{
    template<class T, std::size_t I>
    using tuple_element_t = remove_cvr_t<decltype(std::get<I>(std::declval<T>()))>;
}
