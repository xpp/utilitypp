#pragma once

#include <limits>
#include <cmath>

namespace upp
{
    inline bool approx_equals(double a, double b, double max_diff = std::numeric_limits<double>::epsilon())
    {
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wfloat-equal"
        return std::abs(a - b) <= max_diff;
    #pragma GCC diagnostic pop
    }
}

