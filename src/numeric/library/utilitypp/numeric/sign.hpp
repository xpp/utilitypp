#pragma once

#include <cstddef>

namespace upp
{
    inline constexpr std::int8_t sign(auto v)
    {
        return v < 0 ? -1 : 1;
    }
}

