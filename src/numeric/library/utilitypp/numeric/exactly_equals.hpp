#pragma once

namespace upp
{
    inline bool exactly_equals(double a, double b)
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wfloat-equal"
        return a == b;
#pragma GCC diagnostic pop
    }

    template<int I, class...Ts>
    inline bool exactly_equals(Ts...ts)
    {
        return (exactly_equals(ts, I) && ...);
    }

    template<class...Ts>
    inline bool exactly_equals_1(Ts...ts)
    {
        return exactly_equals<1>(ts...);
    }

    template<class...Ts>
    inline bool exactly_equals_0(Ts...ts)
    {
        return exactly_equals<0>(ts...);
    }
}

